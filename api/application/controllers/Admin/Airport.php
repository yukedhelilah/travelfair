<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Airport extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/m_airport', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function get_data_post(){
        $data   = $this->mainmodul->get_data();

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $data,
        ], 200);                
    }
    
    public function save_data_post(){
        if ($this->input->post('act') == 'add') {
            $data   = array(
                'regionID'      => $this->input->post('selectRegion', true),
                'airportCode'   => strtoupper($this->input->post('airportCode', true)),
                'airportName'   => $this->input->post('airportName', true),
                'airportCountry'=> $this->input->post('airportCountry', true),
            );
            $sql    = $this->mainmodul->add_data($data);
        } else {
            $data = array(
                'regionID'      => $this->input->post('selectRegion', true),
                'airportCode'   => strtoupper($this->input->post('airportCode', true)),
                'airportName'   => $this->input->post('airportName', true),
                'airportCountry'=> $this->input->post('airportCountry', true),
            );
            $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));
        }

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
    
    public function delete_data_post(){
        $data = array(
            'flag'      => 1,
        );
        $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
}

