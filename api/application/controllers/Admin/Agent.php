<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/m_agent', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function get_data_post(){
        $data   = $this->mainmodul->get_data();

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $data,
        ], 200);                
    }
    
    public function save_data_post(){
        if ($this->input->post('act') == 'add') {
            $data   = array(
                'agentName'     => strtoupper($this->input->post('agentName', true)),
                'agentID'       => $this->input->post('agentID', true),
            );
            $sql    = $this->mainmodul->add_data($data);

            $staff  = array(
                'agentID'       => $sql,
                'staffName'     => 'SUPER ID',
                'staffUsername' => 'superid',
                'staffPassword' => md5('1234'),
            ); 
            $sql2   = $this->mainmodul->add_staff($staff);
        } else {
            if(!empty($this->input->post('resetPassword'))){
                $data = array(
                    'agentName'     => strtoupper($this->input->post('agentName', true)),
                    'agentID'       => $this->input->post('agentID', true),
                );

                $staff = array(
                    'staffPassword'      => md5('1234'),
                );
                $sql2   = $this->mainmodul->edit_staff($staff,$this->input->post('id'));

            } else {
                $data = array(
                    'agentName'     => strtoupper($this->input->post('agentName', true)),
                    'agentID'       => $this->input->post('agentID', true),
                );
            }
            $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));
        }

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
    
    public function delete_data_post(){
        $data = array(
            'flag'      => 1,
        );
        $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
}

