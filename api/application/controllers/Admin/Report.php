<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/m_report', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_cdv_post(){
        $data = $this->mainmodul->get_cdv();

        $this->response([
            'code'      => 200,
            'massage'   => 'Success',
            'data'      => $data,
        ], 200);
    }

    public function get_cdv_extra_post(){
        $data = $this->mainmodul->get_cdv_extra();

        $this->response([
            'code'      => 200,
            'massage'   => 'Success',
            'data'      => $data,
        ], 200);
    }

    public function get_map_per_post(){
        $data = $this->mainmodul->get_map_per();

        $this->response([
            'code'      => 200,
            'massage'   => 'Success',
            'data'      => $data,
        ], 200);
    }

    public function get_map_us_post(){
        $data = $this->mainmodul->get_map_us();

        $this->response([
            'code'      => 200,
            'massage'   => 'Success',
            'data'      => $data,
        ], 200);
    }

    public function get_flazz_post(){
        $data = $this->mainmodul->get_flazz();

        $this->response([
            'code'      => 200,
            'massage'   => 'Success',
            'data'      => $data,
        ], 200);
    }

    public function get_ezlink_post(){
        $data = $this->mainmodul->get_ezlink();

        $this->response([
            'code'      => 200,
            'massage'   => 'Success',
            'data'      => $data,
        ], 200);
    }

    public function get_daily_post(){
        $data = $this->mainmodul->get_daily();

        $this->response([
            'code'      => 200,
            'massage'   => 'Success',
            'data'      => $data,
        ], 200);
    }
}

