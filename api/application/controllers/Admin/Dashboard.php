<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Admin/m_dashboard', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function dashboard_post(){
        $data['allClaimed']     = $this->mainmodul->all_claimed();
        $data['todayClaimed']   = $this->mainmodul->today_claimed();
        $data['onQueue']        = $this->mainmodul->on_queue();
        $data['unclaimed']      = $this->mainmodul->unclaimed();

        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
        ], 200);
    }

    public function gift_post(){
        $data['gift1']  = $this->mainmodul->gift1();
        $data['gift2']  = $this->mainmodul->gift2();
        $data['gift3']  = $this->mainmodul->gift3();
        $data['gift4']  = $this->mainmodul->gift4();
        $data['gift5']  = $this->mainmodul->gift5();
        $data['gift6']  = $this->mainmodul->gift6();
        $data['gift7']  = $this->mainmodul->gift7();
        $data['gift8']  = $this->mainmodul->gift8();

        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
        ], 200);
    }
}

