<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/m_stock', 'mainmodul');
        $this->load->model('Agent/m_claimed', 'secondmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function get_item_post(){
        $data   = $this->mainmodul->get_item();

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $data,
        ], 200);                
    }
    
    public function get_data_post(){
        $data   = $this->mainmodul->get_data();

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $data,
        ], 200);                
    }
    
    public function save_data_post(){
        if ($this->input->post('act') == 'add') {
            $data   = array(
                'itemID'        => $this->input->post('selectItem', true),
                'stockDate'     => $this->input->post('stockDate', true),
                'stockTotal'    => $this->input->post('stockTotal', true),
            );
            $sql    = $this->mainmodul->add_data($data);
        } else {
            $data = array(
                'itemID'        => $this->input->post('selectItem', true),
                'stockDate'     => $this->input->post('stockDate', true),
                'stockTotal'    => $this->input->post('stockTotal', true),
            );
            $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));
        }

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
    
    public function delete_data_post(){
        $data = array(
            'flag'      => 1,
        );
        $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
    
    public function reset_queue_post(){
        $queue   = $this->mainmodul->get_queue();

        if ($queue != null) {
            foreach ($queue as $key => $value) {
                $sql1   = $this->mainmodul->delete_checker($value->checkerID);
                $sql2   = $this->mainmodul->delete_queue($value->ticketID);
                $details    = array(
                    'isStatus'   => 0,
                );
                $sql    = $this->secondmodul->edit_details($details, $value->ticketID);
            }

            $result;
            if ($sql == true && $sql1 == true && $sql2 == true){
                $result = true;
            } else {
                $result[0] = $sql;
                $result[2] = $sql1;
                $result[3] = $sql2;
            }
        } else {
            $result = 'NO QUEUE';
        }

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $result,
        ], 200);                
    }
}

