<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class claim extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Agent/m_claim', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function check_pnr_post(){
        $data = $this->mainmodul->check_pnr(strtoupper($this->input->post('pnr')));
        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'status'    => 'Internal Server Error',
                'info'      => $data['error'],
            ], 200);
        }
        
        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
            'total'     => count($data) > 0 ? count($data) : 0,
        ], 200);
    }

    public function check_ticket_post(){
        $data = $this->mainmodul->check_ticket($this->input->post('ticketNo'));
        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'status'    => 'Internal Server Error',
                'info'      => $data['error'],
            ], 200);
        }
        
        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
            'total'     => count($data) > 0 ? count($data) : 0,
        ], 200);
    }

    public function check_route_post(){
        $data = $this->mainmodul->check_route(strtoupper($this->input->post('airportCode')));
        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'status'    => 'Internal Server Error',
                'info'      => $data['error'],
            ], 200);
        }
        
        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
            'total'     => count($data) > 0 ? count($data) : 0,
        ], 200);
    }

    public function save_post(){
        $error = [];
        if(isset($_POST['isPnr']) && $_POST['isPnr'] == '0'){
            $error[] = 'PNR is already in our system and/or your PNR is not valid.';
        }
        if(isset($_POST['isFrom']) && $_POST['isFrom'] == '0'){
            $error[] = 'We can not find this city ('.strtoupper($_POST['tripFrom_3']).') in our system.';
        }
        if(isset($_POST['isTo']) && $_POST['isTo'] == '0'){
            $error[] = 'We can not find this city ('.strtoupper($_POST['tripTo_1']).') in our system.';
        }
        if($_POST['classDep'] == 1 || $_POST['classArr'] == 1){
            $error[] = 'Choose your passanger Class.';
        }

        $dateDep = date('Y-m-d', strtotime($_POST['depdate_yy'].'-'.$_POST['depdate_mm'].'-'.$_POST['depdate_dd']));
        $dateArr = date('Y-m-d', strtotime($_POST['arrdate_yy'].'-'.$_POST['arrdate_mm'].'-'.$_POST['arrdate_dd']));

        if ($dateArr < $dateDep) {
            $error[] = 'Your arrival date can not be earlier than departure date.';
        }

        $isTicket = $_POST['isTicket'];
        $ticketNo = $_POST['ticketNo'];
        foreach ($isTicket as $key => $value) {
            if($value == '0'){
                $error[] = 'Ticket number (618-'.$ticketNo[$key].') is already in our system and/or your ticket number is not valid.';
            }
        }

        $tempTicket= array();

        foreach($ticketNo as $key => $valTicket){
            $tempTicket[] = $valTicket;
        }

        if(count($tempTicket) != count(array_unique($tempTicket))){
            $error[] = 'Input different ticket number.';
        }

        $kf     = $_POST['kfNo'];
        foreach ($kf as $key => $value) {
            if($value == ''){
                $error[] = 'Krisflyer '.$value.' is not valid.';
            }
        }

        $tempKf = array();

        foreach($kf as $key => $valKf){
            $tempKf[] = $valKf;
        }

        if(count($tempKf) != count(array_unique($tempKf))){
            $error[] = 'Input different krisflyer number.';
        }

        if(count($error)>0){
            $this->response([
                'code' => 500,
                'status' => 'Internal Server Error',
                'info' => $error,
            ], 200);
        }else{
            $gift   = count($ticketNo) >= 8 ? 2 : (count($ticketNo) >= 4 ? 1 : 0);

            $data_claimed = array(
                'agentID'       => $_POST['agentID'],
                'pnr'           => strtoupper($_POST['pnr']),
                'tripType'      => $_POST['tripType'],
                'issuanceDate'  => date('Y-m-d'),
                'tripFrom_1'    => strtoupper($_POST['tripFrom_1']),
                'tripFrom_2'    => strtoupper($_POST['tripFrom_2']),
                'tripFrom_3'    => strtoupper($_POST['tripFrom_3']),
                'classDep'      => $_POST['classDep'],
                'depDate'       => $_POST['depdate_yy'].'-'.$_POST['depdate_mm'].'-'.$_POST['depdate_dd'],
                'arrDate'       => $_POST['arrdate_yy'].'-'.$_POST['arrdate_mm'].'-'.$_POST['arrdate_dd'],
                'tripTo_1'      => strtoupper($_POST['tripTo_1']),
                'tripTo_2'      => strtoupper($_POST['tripTo_2']),
                'tripTo_3'      => strtoupper($_POST['tripTo_3']),
                'classArr'      => $_POST['classArr'],
                'noofticket'    => count($ticketNo),
                'isStatus'      => 0,
                'createdDate'   => date('Y-m-d H:i:s'),
                'createdBy'     => $_POST['staffID'],
            );

            $sql_claimed = $this->mainmodul->add_claimed($data_claimed);
            if (isset($sql_claimed['error'])) {
                $error[] = $sql_claimed['error'];
                $this->response([
                    'code'      => 500,
                    'status'    => 'Internal Server Error',
                    'info'      => $error,
                ], 200);
            }

            foreach ($isTicket as $key => $value) {
                if($value == '1'){
                    $data_claimed_details = array(
                        'claimID'       => $sql_claimed['last_id'],
                        'ticketNo'      => $ticketNo[$key],
                        'kfNo'          => strtoupper($kf[$key]),
                        'isStatus'      => 0, //0 Waiting List 1 Queue 2 Done
                        'createdDate'   => date('Y-m-d'),
                        'createdTime'   => date('H:i:s'),
                        'createdBy'     => $_POST['staffID'],
                    );

                    $sql_claimed_details = $this->mainmodul->add_claimed_details($data_claimed_details);
                }
            }

            $data = $this->mainmodul->detil($sql_claimed['last_id']);

            $this->response([
                'code'      => 200,
                'status'    => 'Success',
                'data'      => $data,
            ], 200);
        }
    }
}

