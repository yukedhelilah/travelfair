<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Agent/m_staff', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function get_data_post(){
        $data   = $this->mainmodul->get_data($this->input->post('agentID'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $data,
        ], 200);                
    }
    
    public function save_data_post(){
        if ($this->input->post('act') == 'add') {
            $data   = array(
                'agentID'       => $this->input->post('agentID',true),
                'staffName'     => strtoupper($this->input->post('staffName', true)),
                'staffUsername' => $this->input->post('staffUsername', true),
                'staffPassword' => md5('1234'),
            );
            $sql    = $this->mainmodul->add_data($data);
        } else {
            if(!empty($this->input->post('resetPassword'))){
                $data = array(
                    'staffName'     => strtoupper($this->input->post('staffName', true)),
                    'staffUsername' => $this->input->post('staffUsername', true),
                    'staffPassword' => md5('1234'),
                );
            } else {
                $data = array(
                    'staffName'     => strtoupper($this->input->post('staffName', true)),
                    'staffUsername' => $this->input->post('staffUsername', true),
                );
            }
            $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));
        }

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
    
    public function delete_data_post(){
        $data = array(
            'flag'      => 1,
        );
        $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
}

