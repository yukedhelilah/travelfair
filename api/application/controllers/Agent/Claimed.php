<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Claimed extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Agent/m_claimed', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function get_data_post(){
        $data   = $this->mainmodul->get_data($this->input->post('agentID'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $data,
        ], 200);                
    }
    
    public function get_details_post(){
        $data   = $this->mainmodul->get_details($this->input->post('claimedID'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $data,
        ], 200);                
    }
    
    public function save_data_post(){
        $data = array(
            'pnr'           => strtoupper($_POST['pnr']),
            'depDate'       => $_POST['depdate_yy'].'-'.$_POST['depdate_mm'].'-'.$_POST['depdate_dd'],
            'arrDate'       => $_POST['arrdate_yy'].'-'.$_POST['arrdate_mm'].'-'.$_POST['arrdate_dd'],
            'tripType'      => $_POST['tripType'],
            'tripFrom_3'    => strtoupper($_POST['tripFrom_3']),
            'tripTo_1'      => strtoupper($_POST['tripTo_1']),
            'classDep'      => $_POST['classDep'],
            'classArr'      => $_POST['classArr'],
            'remarks'       => $_POST['remarks'],
            'lastDate'      => date('Y-m-d H:i:s'),
            'lastBy'        => $_POST['staffID'],
        );
        $sql    = $this->mainmodul->edit_data($data,$this->input->post('pnrID'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
    
    public function save_details_post(){
        if ($this->input->post('act') == 'add') {
            $details    = array(
                'claimID'       => $this->input->post('claimedID', true),
                'ticketNo'      => $this->input->post('ticketNo', true),
                'kfNo'          => $this->input->post('kfNo', true),
                'createdBy'     => $this->input->post('staffID',true),
                'createdDate'   => date('Y-m-d'),
                'createdTime'   => date('H:i:s'),
            );
            $sql    = $this->mainmodul->add_details($details);

            $data   = $this->mainmodul->get_details($this->input->post('claimedID'));

            $ticket     = $data[0]['noofticket'];
            $wl = 0; $queue = 0; $done = 0;
            foreach ($data[0]['ticket'] as $key => $value) {
                if ($value->isStatus == 0) {
                    $wl     += 1;
                } else if ($value->isStatus == 1) {
                    $queue  += 1;
                } else {
                    $done   += 1;
                }
            }

            $status;
            if ($done > 0 && $done < $ticket) {
                $status = 1;
            } else if ($ticket == $done) {
                $status = 2;
            } else {
                $status = 0;
            }

            $data    = array(
                'noofticket'    => $ticket+1,
                'isStatus'      => $status,
                'lastDate'      => date('Y-m-d H:i:s'),
                'lastBy'        => $this->input->post('staffID'),
            );
            $sql    = $this->mainmodul->edit_data($data, $this->input->post('claimedID'));
        } else {
            $details    = array(
                'ticketNo'  => $this->input->post('ticketNo', true),
                'kfNo'      => $this->input->post('kfNo', true),
            );
            $sql   = $this->mainmodul->edit_details($details,$this->input->post('id'));

            $data    = array(
                'lastDate'      => date('Y-m-d H:i:s'),
                'lastBy'        => $this->input->post('staffID'),
            );
            $sql    = $this->mainmodul->edit_data($data, $this->input->post('claimedID'));
        }

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
    
    public function delete_details_post(){
        $sql    = $this->mainmodul->delete_details($this->input->post('ticketID'));

        $data   = $this->mainmodul->get_details($this->input->post('claimedID'));

        $ticket     = $data[0]['noofticket'];
        $wl = 0; $queue = 0; $done = 0;
        foreach ($data[0]['ticket'] as $key => $value) {
            if ($value->isStatus == 0) {
                $wl     += 1;
            } else if ($value->isStatus == 1) {
                $queue  += 1;
            } else {
                $done   += 1;
            }
        }

        $status;
        if ($done > 0 && $done < $ticket) {
            $status = 1;
        } else if ($ticket == $done) {
            $status = 2;
        } else {
            $status = 0;
        }

        $data   = array(
            'noofticket'    => $ticket-1,
            'isStatus'      => $status,
            'lastDate'      => date('Y-m-d H:i:s'),
            'lastBy'        => $this->input->post('staffID'),
        );
        $sql2   = $this->mainmodul->edit_data($data, $this->input->post('claimedID'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
}

