<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Queue extends App_Public {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('checker/queue_open_model', 'datamodul');
        $this->load->model('checker/queue_done_model', 'datamodul2');
    }

    public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function list_open_post()
    {
        $session = [
            'staffID' => base64_decode(str_rot13($_POST['staff'])),
        ];

        $search         = $_POST['search']['value'];
        $limit          = $_POST['length'];
        $start          = $_POST['start'];
        $order_index    = $_POST['order'][0]['column'];
        $order_field    = $_POST['columns'][$order_index]['data'];
        $order_ascdesc  = $_POST['order'][0]['dir'];
        $sql_total      = $this->datamodul->count_all($session);
        $sql_data       = $this->datamodul->filter($search, $limit, $start, $order_field, $order_ascdesc, $session);
        $sql_filter     = $this->datamodul->count_filter($search, $session);

        $records = array(); $no = 1;
        if(!empty($sql_data)){
        foreach ($sql_data as $row) {

            $records[] = array(
                'no'            => $no,
                'id'            => md5($row['id']),
                'queueDate'     => date('d M', strtotime($row['queueDate'])),
                'queueCode'     => strtoupper($row['queueCode']),
                'noofpnr'       => $row['noofpnr'],
                'noofticket'    => $row['noofticket'],
                'staff'         => strtoupper($row['staff']),
            );

            $no++;
        }
        }

        $data   = array(
            'draw'              => $_POST['draw'],
            'recordsTotal'      => $sql_total,
            'recordsFiltered'   => $sql_filter,
            'data'              => $records
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function list_done_post()
    {
        $session = [
            'staffID' => base64_decode(str_rot13($_POST['staff'])),
        ];

        $search         = $_POST['search']['value'];
        $limit          = $_POST['length'];
        $start          = $_POST['start'];
        $order_index    = $_POST['order'][0]['column'];
        $order_field    = $_POST['columns'][$order_index]['data'];
        $order_ascdesc  = $_POST['order'][0]['dir'];
        $sql_total      = $this->datamodul2->count_all($session);
        $sql_data       = $this->datamodul2->filter($search, $limit, $start, $order_field, $order_ascdesc, $session);
        $sql_filter     = $this->datamodul2->count_filter($search, $session);

        $records = array(); $no = 1;
        if(!empty($sql_data)){
        foreach ($sql_data as $row) {

            $records[] = array(
                'no'            => $no,
                'id'            => md5($row['id']),
                'queueDate'     => date('d M', strtotime($row['queueDate'])),
                'queueCode'     => strtoupper($row['queueCode']),
                'noofpnr'       => $row['noofpnr'],
                'noofticket'    => $row['noofticket'],
                'staff'         => strtoupper($row['staff']),
            );

            $no++;
        }
        }

        $data   = array(
            'draw'              => $_POST['draw'],
            'recordsTotal'      => $sql_total,
            'recordsFiltered'   => $sql_filter,
            'data'              => $records
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}