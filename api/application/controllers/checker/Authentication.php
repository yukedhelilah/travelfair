<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends App_Public {

    public function __construct()
    {
        parent::__construct();
        /*header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');*/
        
        $this->load->model('checker/authentication_model', 'mainmodul');
    }

    public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function signin_post()
    {
           
        $required = [];
        if(empty($this->input->post('staffUsername'))){
            $required[] = 'Username';
        }

        if(empty($this->input->post('staffPassword'))){
            $required[] = 'Password';
        }

        if(count($required)>0){
            $error = [
                'reason'    => 'required',
                'message'   => join(", ",$required).' is required',
            ];
            $this->response([
                'code'      => 401,
                'message'   => 'Unauthorized',
                'errors'    => $error,
            ], 200);
        }

        $_data = array(
            'staffUsername' => strtolower($this->input->post('staffUsername')),
            'staffPassword' => md5($this->input->post('staffPassword')),
        );
        $data = $this->mainmodul->login_staff($_data);
        if (isset($data['error'])) {
            $error = [
               'message'   => $data['error']
            ];
            $this->response([
                'code'      => 500,
                'message'   => 'Internal Server Error',
                'errors'    => $error,
            ], 200);
        }else{
            if($data['total']==1){
                //Action -> Login
                $this->response([
                    'code' => 200,
                    'status' => 'Success',
                    'data' => $data,
                ], 200); 
            }else{
                $error = [
                   'message'   => 'Invalid combination Username and Password.'
                ];
                $this->response([
                    'code'      => 500,
                    'message'   => 'Internal Server Error',
                    'errors'    => $error,
                ], 200);
            }
        }
    }
}