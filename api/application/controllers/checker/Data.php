<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends App_Public {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('checker/data_model', 'datamodul');
    }

    public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    function search_pnr_ticket_post(){
        $get = $this->datamodul->search_pnr_ticket(strtoupper($this->input->post('pnr')),$this->input->post('ticket'));
        $data = [
            'status'    => (!empty($get) ? true : false),
            'items'     => $get,
        ];
        echo json_encode($data);
    }

    function det_pnr_ticket_post(){
        $get        = $this->datamodul->det_by_pnr($this->input->post('id'));
        $data_edit  = $this->datamodul->det_by_pnr_all($this->input->post('id'));
        $ticket     = $this->datamodul->get_ticket_by_pnr($this->input->post('id'));
        $data = [
            'status'    => (!empty($get) ? true : false),
            'data'      => $get,
            'data_edit' => $data_edit,
            'ticket'    => $ticket,
        ];
        echo json_encode($data);
    }

    function summary_post(){
        $color = ['1' => 'indigo', '2' => 'orange', '3' => 'teal', '4' => 'purple', '5' => 'pink'];

        $arr = []; $a = 0; $b = 0; $c = 0;
        foreach ($this->input->post('id') as $key => $value) {
            $get = $this->datamodul->get_ticket_by_id($value);

            if(isset($get['error'])){
                print_r($get['error']);
            }
            
            if($b != $get['claimID']){
                $a++;
                $b = $get['claimID'];
                $c = 1;
            }else{
                $c++;
            }

            $det = $this->datamodul->det_by_pnr(md5($get['claimID']));
            $gift_1 = $this->gift_1($get['classDep'], $get['classArr'], $det['no_ticket'], $c);
            $gift_2 = $this->gift_2($get['classDep'], $get['classArr'], $det['no_ticket'], $c);
            $gift_3 = $this->gift_3($get['classDep'], $get['classArr'], $det['no_ticket'], $c);
            $gift_4 = $this->gift_4($get['tripFrom_3'], $get['tripTo_1']);
            $gift_5 = $this->gift_5($get['month_depdate'], $get['year_depdate']);
            $gift_6 = $this->gift_6($get['tripFrom_3'], $get['tripTo_1'], $det['no_ticket'], $get['depDate'], $get['arrDate']);
            $gift_7 = $this->gift_7($get['tripFrom_3'], $get['tripTo_1']);
            $gift_8 = $this->gift_8($get['tripFrom_3'], $get['tripTo_1']);
            $gift_9 = 0;//$this->gift_9($get['tripFrom_3'], $get['tripTo_1']);
            $arr[] = array_merge($get, [
                'color' => $color[$a], 
                'gift_1' => $gift_1, 
                'gift_2' => $gift_2, 
                'gift_3' => $gift_3, 
                'gift_4' => $gift_4, 
                'gift_5' => $gift_5, 
                'gift_6' => $gift_6,
                'gift_7' => $gift_7,
                'gift_8' => $gift_8,
            ]);
        }

        $data = [
            'status'    => (!empty($arr) ? true : false),
            'data'      => $arr,
        ];
        echo json_encode($data);
    }

    function edit_queue_post(){
        $color = ['1' => 'indigo', '2' => 'orange', '3' => 'teal', '4' => 'purple', '5' => 'pink'];

        $arr = []; $a = 0; $b = 0; $c = 0;
        $dt = $this->datamodul->get_checker_details($this->input->post('search'));
        foreach ($dt as $key => $value) {
            $get = $this->datamodul->get_ticket_by_id($value['id']);

            if(isset($get['error'])){
                print_r($get['error']);
            }

            if($b != $get['claimID']){
                $a++;
                $b = $get['claimID'];
                $c = 1;
            }else{
                $c++;
            }

            $det = $this->datamodul->det_by_pnr(md5($get['claimID']));
            $gift_1 = $get['gift_1'];
            $gift_2 = $get['gift_2'];
            $gift_3 = $get['gift_3'];
            $gift_4 = $get['gift_4'];
            $gift_5 = $get['gift_5'];
            $gift_6 = $get['gift_6'];
            $gift_7 = $get['gift_7'];
            $gift_8 = $get['gift_8'];
            $arr[] = array_merge($get, [
                'color' => $color[$a], 
                'gift_1' => $gift_1, 
                'gift_2' => $gift_2, 
                'gift_3' => $gift_3, 
                'gift_4' => $gift_4, 
                'gift_5' => $gift_5, 
                'gift_6' => $gift_6,
                'gift_7' => $gift_7,
                'gift_8' => $gift_8,
            ]);
        }

        $data = [
            'status'    => (!empty($arr) ? true : false),
            'data'      => $arr,
            'dt'    => $dt,
        ];
        echo json_encode($data);
    }

    function gift_1($classDep, $classArr, $jumlah, $urutan){
        //FLAZZ CARDS ==> Setiap pembelian 2 tiket PEY* ke atas (Kalau beli 2 tiket dapat 1)
        $classDep > $classArr ? $terbesar = $classDep : $terbesar = $classArr;
        if($terbesar > 2){
            $urutan%2 == 0 ? $total = 1 : $total = 0;
        }else{
            $total = 0;
        }
        
        return $total;
    }

    function gift_2($classDep, $classArr, $jumlah, $urutan){
        //FOLDABLE BAGS ==> Setiap pembelian tiket PEY* ke atas
        $classDep > $classArr ? $terbesar = $classDep : $terbesar = $classArr;
        if($terbesar > 2){
            $total = 1;
        }else{
            $total = 0;
        }
        
        return $total;
    }

    function gift_3($classDep, $classArr, $jumlah, $urutan){
        //KF BAGS / SHOE BAGS ==> Setiap pembelian tiket all class       
        $total = 1;
        
        return $total;
    }

    function gift_4($depfrom, $depto){
        //CDV ==> NON = 0 / SEA, NA, SWP, WAA = 1 / EUR, AME = 2
        $sql    = $this->datamodul->check_region($depfrom);
        $sql2   = $this->datamodul->check_region($depto);
        $total = $sql['noofcdv'] >= $sql2['noofcdv'] ? $sql['noofcdv'] : $sql2['noofcdv'];

        return $total;
    }

    function gift_5($month, $year){
        //CDV EXTRA ==> Hanya untuk keberangkatan periode Februari - April 2020
        $total = 0;
        if($month == '2' || $month == '3' || $month == '4'){
            if($year == '2020'){
                $total = 1;
            }
        }

        return $total;
    }

    function gift_6($depfrom, $depto, $jumlah, $depdate, $arrdate){
        //EZ LINK CARD ==> Berempat (GV4) dan Minim 3 nights stay -> dihitung berdasarkan tanggal berangkat -> memenuhi salah satu dapat
        $datetime1 = new DateTime($depdate);
        $datetime2 = new DateTime($arrdate);
        $difference = $datetime1->diff($datetime2);

        $total = 0;
        if($depfrom == 'NON' && $depto == 'NON'){
            if($jumlah >= 2){
                $total = 1;
            }else if($difference->days >= 2){
                $total = 1;
            }
        }

        return $total;
    }

    function gift_7($depfrom, $depto){
        //MAP VOUCHER TOTAL IDR 200K ==> Hanya untuk tiket PERTH baik return atau open jaw
        $total = 0;
        if($depfrom == 'PER' || $depto == 'PER'){
            $total = 1;
        }

        return $total;
    }

    function gift_8($depfrom, $depto){
        //MAP VOUCHER TOTAL IDR 100K ==> Hanya untuk region EUR (Eropa) dan AME (Amerika)
        $sql    = $this->datamodul->check_region($depfrom);
        $sql2   = $this->datamodul->check_region($depto);
        $total = $sql['regionCode'] == 'AME' || $sql2['regionCode'] == 'AME' || $sql['regionCode'] == 'EUR' || $sql2['regionCode'] == 'EUR' ? 1 : 0;

        return $total;
    }

    function gift_9($depfrom, $depto){
        //Ngo Ping Entrance Tickets ==> Hanya untuk rute hongkong
        $total = 0;
        if($depfrom == 'HKG' || $depto == 'HKG'){
            //$total = 1;
            $total = 0;
        }

        return $total;
    }

    function save_post(){
        $status = true;
        $error = [];

        $this->db->query('START TRANSACTION');

        $queueCode = $this->generateRandomString();
        $data = [
            'queueDate' => date('Y-m-d'),
            'queueCode' => $queueCode,
            'noofpnr'   => $this->input->post('no_pnr'),
            'noofticket'=> $this->input->post('no_ticket'),
            'isStatus'  => 1,
            'createdDate'   => date('Y-m-d H:i:s'),
            'createdBy' => base64_decode(str_rot13($this->input->post('staff'))),
        ];
        $sql = $this->datamodul->insert_checker($data);
        if(isset($sql['error'])){
            array_push($error, $sql['query']);
        }

        if(!empty($this->input->post('gift_1'))){
            foreach ($this->input->post('gift_1') as $key => $value) {
                $get = $this->datamodul->get_ticket_by_id($key);
                $subdata = [
                    'checkerID' => $sql['id'],
                    'claimID'   => $get['claimID'],
                    'detailID'  => $key,
                    'total_1'   => $value,
                    'total_2'   => $this->input->post('gift_2')[$key],
                    'total_3'   => $this->input->post('gift_3')[$key],
                    'total_4'   => $this->input->post('gift_4')[$key],
                    'total_5'   => $this->input->post('gift_5')[$key],
                    'total_6'   => $this->input->post('gift_6')[$key],
                    'total_7'   => $this->input->post('gift_7')[$key],
                    'total_8'   => $this->input->post('gift_8')[$key],
                    'total_9'   => $this->input->post('gift_9')[$key],
                ];

                $update = $this->datamodul->update_status_claimed_details($key,1);
                if(isset($update['error'])){
                    array_push($error, $update['error']);
                }

                $sql2 = $this->datamodul->insert_checker_details($subdata);
                if(isset($sql2['error'])){
                    array_push($error, $sql2['error']);
                }
            }
        }

        if (count($error) > 0) {
            /* Rollback Transaction if Failure */
            $this->db->query('ROLLBACK');
            $status = false;
        } else {
            /* Commit Transaction if Success */
            $this->db->query('COMMIT');
        }

        $this->response([
            'status' => $status,
            'error'  => $error,
            'url'    => $status == true ? $queueCode : '',
        ]); 
    }

    function generateRandomString($length = 4) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return date('d').$randomString;
    }

    function print_preview_get(){
        $get = $this->datamodul->det_checker($this->input->get('code'));
        $data = [
            'status'    => (!empty($get) ? true : false),
            'data'      => $get,
        ];
        echo json_encode($data);
    }

    public function update_data_claimed_post(){
        $data = array(
            'pnr'           => strtoupper($_POST['pnr']),
            'depDate'       => $_POST['depdate_yy'].'-'.$_POST['depdate_mm'].'-'.$_POST['depdate_dd'],
            'arrDate'       => $_POST['arrdate_yy'].'-'.$_POST['arrdate_mm'].'-'.$_POST['arrdate_dd'],
            'tripType'      => $_POST['tripType'],
            'tripFrom_3'    => strtoupper($_POST['tripFrom_3']),
            'tripTo_1'      => strtoupper($_POST['tripTo_1']),
            'classDep'      => $_POST['classDep'],
            'classArr'      => $_POST['classArr'],
        );
        $sql    = $this->datamodul->edit_data($data,$this->input->post('pnrID'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            /*'data'      => $this->db->last_query(),*/
        ], 200);                
    }

    function delete_ticket_post()
    {
        $get = $this->datamodul->delete_claimed_details($this->input->post('id'));
        $data = [
            'status'    => true,
        ];
        echo json_encode($data);
    }

    function update_data_ticket_post(){
        $status = true;
        $error = [];

        $this->db->query('START TRANSACTION');

        if(!empty($this->input->post('ticketNo'))){
            foreach ($this->input->post('ticketNo') as $key => $value) {
                $data = [
                    'id'        => $key,
                    'ticketNo'  => $value,
                    'kfNo'      => $this->input->post('kfNo')[$key],
                ];

                $sql = $this->datamodul->update_claimed_details($data);
                if(isset($sql['error'])){
                    array_push($error, $sql['query']);
                }
            }
        }  

        if (count($error) > 0) {
            /* Rollback Transaction if Failure */
            $this->db->query('ROLLBACK');
            $status = false;
        } else {
            /* Commit Transaction if Success */
            $this->db->query('COMMIT');
        }

        $this->response([
            'status' => $status,
            'error'  => $error,
        ]);       
    }

    function update_data_queue_post(){
        $status = true;
        $error = [];

        $this->db->query('START TRANSACTION');

        if(!empty($this->input->post('ticketNo'))){
            foreach ($this->input->post('ticketNo') as $key => $value) {
                $subdata = [
                    'detailID'  => $key,
                    'total_1'   => $this->input->post('gift_1')[$key],
                    'total_2'   => $this->input->post('gift_2')[$key],
                    'total_3'   => $this->input->post('gift_3')[$key],
                    'total_4'   => $this->input->post('gift_4')[$key],
                    'total_5'   => $this->input->post('gift_5')[$key],
                    'total_6'   => $this->input->post('gift_6')[$key],
                    'total_7'   => $this->input->post('gift_7')[$key],
                    'total_8'   => $this->input->post('gift_8')[$key],
                ];

                $sql = $this->datamodul->update_checker_details_bydetailID($subdata);
                if(isset($sql['error'])){
                    array_push($error, $sql['error']);
                }

                $subdata2 = [
                    'id'        => $key,
                    'ticketNo'  => $value,
                    'kfNo'      => $this->input->post('ticketNo')[$key],
                    'total_1'   => $this->input->post('gift_1')[$key],
                    'total_2'   => $this->input->post('gift_2')[$key],
                    'total_3'   => $this->input->post('gift_3')[$key],
                    'total_4'   => $this->input->post('gift_4')[$key],
                    'total_5'   => $this->input->post('gift_5')[$key],
                    'total_6'   => $this->input->post('gift_6')[$key],
                    'total_7'   => $this->input->post('gift_7')[$key],
                    'total_8'   => $this->input->post('gift_8')[$key],
                ];

                $sql2 = $this->datamodul->update_claimed_details($subdata2);
                if(isset($sql2['error'])){
                    array_push($error, $sql2['error']);
                }
                
            }
        }

        if (count($error) > 0) {
            /* Rollback Transaction if Failure */
            $this->db->query('ROLLBACK');
            $status = false;
        } else {
            /* Commit Transaction if Success */
            $this->db->query('COMMIT');
        }

        $this->response([
            'status' => $status,
            'error'  => $error,
        ]); 
    }
}
