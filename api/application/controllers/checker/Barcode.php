<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Barcode extends App_Public {

	public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

	public function set_barcode_get()
	{
		$code = $this->input->get('code');
		//load library
		$this->load->library('zend');
		//load in folder Zend
		$this->zend->load('Zend/Barcode');
		
		//generate barcode
		$barcodeOptions = array(
		    'text' => $code, 
		    'barHeight'=> 32, 
		    'factor'=>1,
		    'drawText' => false
		);

		$rendererOptions = array();
		$renderer = Zend_Barcode::factory(
		    'code128', 'image', $barcodeOptions, $rendererOptions
		)->render();
	}
}