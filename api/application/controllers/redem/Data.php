<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends App_Public {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('redem/data_model', 'datamodul');
    }

    public function index_get()
    {
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    function search_queue_code_post(){
        $get = $this->datamodul->search_queue_code(strtoupper($this->input->post('code')));
        $data = [
            'status'    => (!empty($get) ? true : false),
            'items'     => $get,
        ];
        echo json_encode($data);
    }

    function det_queue_code_post(){
        $get = $this->datamodul->det_queue_code($this->input->post('id'));
        $data = [
            'status'    => (!empty($get) ? true : false),
            'data'     => $get,
        ];
        echo json_encode($data);  
    }

    function save_post(){
        //print_r($_POST); exit();
        $status = true;
        $error = [];

        $this->db->query('START TRANSACTION');

        if(!empty($this->input->post('gift_1'))){
            foreach ($this->input->post('gift_1') as $key => $value) {
                for ($i=0; $i <= 9; $i++) { 
                    if($this->input->post('gift_'.$i)[$key] > $this->input->post('old_gift_'.$i)[$key]){
                        // Ada yang ke tambahan
                        $add = $this->input->post('gift_'.$i)[$key] - $this->input->post('old_gift_'.$i)[$key];
                        $update = $this->datamodul->update_checker_details($key,$add,$i,'plus');
                        if(isset($update['error'])){
                            array_push($error, $update['error']);
                        }
                    }else if($this->input->post('gift_'.$i)[$key] < $this->input->post('old_gift_'.$i)[$key]){
                        // Ada yang di kurangi
                        $min = $this->input->post('old_gift_'.$i)[$key] - $this->input->post('gift_'.$i)[$key];
                        $update = $this->datamodul->update_checker_details($key,$min,$i,'min');
                        if(isset($update['error'])){
                            array_push($error, $update['error']);
                        }
                    }
                }

                //Copy data claimed_details
                $get_checker_details = $this->datamodul->get_checker_details($key);
                if(!empty($get_checker_details)){
                    foreach ($get_checker_details as $key => $value) {
                        $dt = [
                            'id'      => $value['detailID'],
                            'total_1' => $value['total_1'],
                            'total_2' => $value['total_2'],
                            'total_3' => $value['total_3'],
                            'total_4' => $value['total_4'],
                            'total_5' => $value['total_5'],
                            'total_6' => $value['total_6'],
                            'total_7' => $value['total_7'],
                            'total_8' => $value['total_8'],
                            'total_9' => $value['total_9'],
                            'isStatus'=> '2',
                        ];

                        $update_claimed_details = $this->datamodul->update_claimed_details($dt);
                        if(isset($update_claimed_details['error'])){
                            array_push($error, $update_claimed_details['error']);
                        }
                    }
                }

                $compare_a = $this->datamodul->det_sum_claimed_details($key);  
                $compare_b = $this->datamodul->det_sum_checker_details($key);

                if($compare_a['total'] > $compare_b['total']){
                    $isStatus = 1; //Di klaim sebagian
                }else if($compare_a['total'] == $compare_b['total']){
                    $isStatus = 2; //Di klaim semua = Done
                }

                $update_status_claimed = $this->datamodul->update_status_claimed($key,$isStatus);
                if(isset($update_status_claimed['error'])){
                    array_push($error, $update_status_claimed['error']);
                }
                $update_status_checker = $this->datamodul->update_status_checker($key,2);
                if(isset($update_status_checker['error'])){
                    array_push($error, $update_status_checker['error']);
                }
            }
        }

        if (count($error) > 0) {
            /* Rollback Transaction if Failure */
            $this->db->query('ROLLBACK');
            $status = false;
        } else {
            /* Commit Transaction if Success */
            $this->db->query('COMMIT');
        }

        $this->response([
            'status' => $status,
            'error'  => $error,
        ]); 
    }

    function update_post()
    {
        $input  = $this->input->post();
        $params = json_decode($input['params'], true);
        $data   = json_decode($input['data'], true);
        $dataPNR = [];

        foreach ($data as $key => $value) {
            if ($value['pnrKey'] == $params['currentPnrKey'] && $value['pnrName'] == $params['pnrName'] && $value['giftKey'] == $params['giftKey']) {
                array_push($dataPNR, $value);
            }
        }

        //print_r($dataPNR);

        // foreach ($dataPNR as $key => $value) {
        //     $sql = $this->datamodul->checkCheckerDetails($value['claimID'],$value['giftKey']);
        //     echo $this->db->last_query();//print_r($sql);
        // }

        $result = $this->datamodul->checkCheckerDetails($dataPNR, $params['claimID'], $params['giftKey']);

        /*if(!empty($this->input->post('data'))){
            $arr = [];
            foreach ($this->input->post('data') as $key => $value) {
                $arr[$value['claimID']][$value['giftKey']][] = $value['barcodeValue'];
            }

            foreach ($arr as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $sql = $this->datamodul->checkCheckerDetails($key,$key2);

                    print_r($sql);
//                    echo count($value);
                }
            }

//            print_r($arr);
        }*/
    }

    function checkBarcode_get()
    {
        $result = $this->datamodul->checkBarcode($this->input->get());
        $this->response($result, 200);
    }
}
