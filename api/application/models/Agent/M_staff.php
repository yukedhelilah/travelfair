<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_staff extends CI_Model
{   
    function get_data($agentID){
        $select = array(
            'id',
            'staffName',
            'staffUsername',
            'lastLogin',
        );

        $this->db->select($select);
        $this->db->from('ms_agent_staff');
        $this->db->where('agentID', $agentID);
        $this->db->where('flag', 0);
        $this->db->order_by('staffName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function add_data($data){
        $this->db->insert('ms_agent_staff', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('ms_agent_staff', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}