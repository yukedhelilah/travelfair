<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_claim extends CI_Model
{     
    function check_pnr($pnr){
        $this->db->select('a.*');
        $this->db->from('claimed a');
        $this->db->where('a.pnr', $pnr);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()) {
            $row = $query->row_array();
            return [
                'id'       => $row['id'],
                'pnr'      => $row['pnr'],
            ];
        }
        
        return [];
    }

    function check_ticket($ticketNo){
        $this->db->select('a.*');
        $this->db->from('claimed_details a');
        $this->db->where('a.ticketNo', $ticketNo);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()) {
            $row = $query->row_array();
            return [
                'id'            => $row['id'],
                'ticketNo'      => $row['ticketNo'],
            ];
        }
        
        return [];
    }

    function check_route($airport){
        $this->db->select('a.*');
        $this->db->from('ms_route a');
        $this->db->where('a.airportCode', $airport);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()) {
            $row = $query->row_array();
            return [
                'id'                => $row['id'],
                'regionID'          => $row['regionID'],
                'airportCode'       => $row['airportCode'],
                'airportName'       => strtoupper($row['airportName']),
                'airportCountry'    => strtoupper($row['airportCountry']),
            ];
        }
        
        return [];
    }

    function add_claimed($data){
        $this->db->insert('claimed',$data);
        
        $query = $this->db->affected_rows();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }else{
            return ['last_id' => $this->db->insert_id()];
        }
                
        return [];
    }

    function add_claimed_details($data){
        $this->db->insert('claimed_details',$data);
        
        $query = $this->db->affected_rows();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }else{
            return ['last_id' => $this->db->insert_id()];
        }
                
        return [];
    }

    function detil($id){
        $this->db->select('a.*, IF(a.tripType=0,"Return","Open Jaw") trip, b.className as depClass, c.className as arrClass');
        $this->db->from('claimed a');
        $this->db->join('ms_class b','a.classDep=b.id');
        $this->db->join('ms_class c','a.classArr=c.id');
        $this->db->where('a.id', $id);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        $arr = array();
        if ($query->num_rows()>0) {
            $row = $query->result();

            $this->db->select('a.*');
            $this->db->from('claimed_details a');
            $this->db->where('a.claimID', $id);
        
            $query_ticket = $this->db->get();

            foreach ($row as $key => $value) {

                $arr[$key] = [
                    '_'                 => $value->id,
                    'pnr'               => $value->pnr,
                    'trip'              => $value->trip,
                    'dep'               => $value->tripFrom_1.' '.$value->tripFrom_2.' '.$value->tripFrom_3,
                    'arr'               => $value->tripTo_1.' '.$value->tripTo_2.' '.$value->tripTo_3,
                    'depClass'          => $value->depClass,
                    'arrClass'          => $value->arrClass,
                    'tripFrom_1'        => $value->tripFrom_1,
                    'tripFrom_2'        => $value->tripFrom_2,
                    'tripFrom_3'        => $value->tripFrom_3,
                    'classDep'          => $value->classDep,
                    'tripTo_1'          => $value->tripTo_1,
                    'tripTo_2'          => $value->tripTo_2,
                    'tripTo_3'          => $value->tripTo_3,
                    'classArr'          => $value->classArr,
                    'depDate'           => $value->depDate,
                    'arrDate'           => $value->arrDate,
                    'ticket'            => $query_ticket->result(),
                    'noofticket'        => count($query_ticket->result()),
                    'createdBy'         => $value->createdBy,
                ];
            }
        }
        
        return $arr;//[];
    }
}