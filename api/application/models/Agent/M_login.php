<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_login extends CI_Model
{   

    function login($agentID, $username, $password){
        $select = array(
            'b.id as agentID',
            'a.id as staffID',
            'b.agentName',
            'a.staffName',
            'a.staffPassword',
        );

        $this->db->select($select);
        $this->db->from('ms_agent_staff a');
        $this->db->join('ms_agent b','a.agentID=b.id');
        $this->db->where('b.agentID', $agentID);
        $this->db->where('a.staffUsername', $username);
        $this->db->where('a.staffPassword', $password);
        $this->db->where('a.flag', '0');
        $this->db->where('b.flag', '0');

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $password = $row['staffPassword'] == md5('1234') ? '0' : '1';

            $tokenKey = $row['staffName'] . '|' . date('Y-m-d H:i:s');
            return [
                'agentID'       => $row['agentID'],
                'staffID'       => $row['staffID'],
                'agentName'     => $row['agentName'],
                'staffName'     => $row['staffName'],
                'password'      => $password,
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  

    function log($id,$data){
        $this->db->where('id',$id);
        $this->db->update('ms_agent_staff', $data);
        if($this->db->affected_rows()){
            return true;
        }else{
            return false;
        }
    }

    function change_password($id,$data){
        $this->db->where('id',$id);
        $this->db->update('ms_agent_staff', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}