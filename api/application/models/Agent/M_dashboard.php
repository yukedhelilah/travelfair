<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_dashboard extends CI_Model
{     
    function all_claimed($agentID){
        $this->db->select('COUNT(a.id) total', 0);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b', 'a.claimID=b.id');
        $this->db->where('b.agentID', $agentID);
        $this->db->where('a.isStatus', '2');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row();
            return $row->total;
        }
    }

    function today_claimed($agentID){
        $this->db->select('COUNT(a.id) total', 0);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b', 'a.claimID=b.id');
        $this->db->where('b.agentID', $agentID);
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row();
            return $row->total;
        }
    }

    function on_queue($agentID){
        $this->db->select('COUNT(a.id) total', 0);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b', 'a.claimID=b.id');
        $this->db->where('b.agentID', $agentID);
        $this->db->where('a.isStatus', '1');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row();
            return $row->total;
        }
    }

    function unclaimed($agentID){
        $this->db->select('COUNT(a.id) total', 0);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b', 'a.claimID=b.id');
        $this->db->where('b.agentID', $agentID);
        $this->db->where('a.isStatus', '0');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row();
            return $row->total;
        }
    }

    function get_data($agentID){
        $this->db->select('a.*, IF(a.tripType=0,"Return","Open Jaw") trip, b.classCode as depClass, c.classCode as arrClass, d.staffName');
        $this->db->from('claimed a');
        $this->db->from('claimed_details aa');
        $this->db->join('ms_class b','b.id=a.classDep');
        $this->db->join('ms_class c','c.id=a.classArr');
        $this->db->join('ms_agent_staff d','d.id=a.createdBy');
        $this->db->where('a.agentID', $agentID);
        $this->db->where('aa.claimedDate', date('Y-m-d'));
        $this->db->order_by('aa.claimedDate', 'desc');
        $this->db->limit(10);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }
}