<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Data_model extends CI_Model
{   
    function search_queue_code($code){
        $this->db->select('md5(a.id) as id', true);
        $this->db->from('checker a');
        if(!empty($code)){
            $this->db->like('a.queueCode', $code);
        }
        $this->db->order_by('a.id','desc');
        $query = $this->db->get();
        return $query->row_array();
    }

    function det_queue_code($md5_id){
        $this->db->select('c.pnr, a.claimID, SUM(a.total_1) gift_1, SUM(a.total_2) gift_2, SUM(a.total_3) gift_3, SUM(a.total_4) gift_4, SUM(a.total_5) gift_5, SUM(a.total_6) gift_6, SUM(a.total_7) gift_7, SUM(a.total_8) gift_8, SUM(a.total_9) gift_9, 8 AS jumlah_gift');
        $this->db->from('checker_details a');
        $this->db->join('checker b','a.checkerID=b.id');
        $this->db->join('claimed c','a.claimID=c.id');
        $this->db->join('claimed_details d','a.detailID=d.id');
        $this->db->where('md5(b.id)', $md5_id, true);
        $this->db->group_by('a.claimID');
        $this->db->order_by('a.id','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_checker_details($claimID, $qty, $no, $type){
        $this->db->select('a.id');
        $this->db->from('checker_details a');
        $this->db->where('a.claimID', $claimID);
        $this->db->order_by('a.id','asc');
        $query = $this->db->get();
        $row = $query->row_array();

        $total = $type == 'min' ? 'total_'.$no-$qty : 'total_'.$no+$qty;
        $this->db->where('id', $row['id']);
        $this->db->update('checker_details', array('total_'.$no => $total));
        if ($this->db->affected_rows() == '1') {
            return ['id' => $row['id']];
        } else {
            if ($this->db->trans_status() === FALSE) {
                return ['id' => 'update claimed_details', 'error' => $this->db->error(), 'query' => $this->db->last_query()];
            }
            return ['id' => $row['id']];
        }
    }

    function update_claimed_details($data){
        $this->db->where('id', $data['id']);
        $this->db->update('claimed_details', $data);
        if ($this->db->affected_rows() == '1') {
            return ['id' => $data['id']];
        } else {
            if ($this->db->trans_status() === FALSE) {
                return ['id' => 'update claimed_details', 'error' => $this->db->error(), 'query' => $this->db->last_query()];
            }
            return ['id' => $data['id']];
        }
    }

    function det_sum_claimed_details($claimID){
        $this->db->select('COUNT(*) total', true);
        $this->db->from('claimed_details');
        $this->db->where('claimID',$claimID);
        $query = $this->db->get();
        return $query->row_array();
    }

    function det_sum_checker_details($claimID){
        $this->db->select('COUNT(*) total', true);
        $this->db->from('checker_details');
        $this->db->where('claimID',$claimID);
        $query = $this->db->get();
        return $query->row_array();
    }

    function update_status_claimed($id, $isStatus){
        $this->db->where('id', $id);
        $this->db->update('claimed', array('isStatus' => $isStatus));
        if ($this->db->affected_rows() == '1') {
            return ['id' => $id];
        } else {
            if ($this->db->trans_status() === FALSE) {
                return ['id' => 'update status claimed', 'error' => $this->db->error(), 'query' => $this->db->last_query()];
            }
            return ['id' => $id];
        }
    }

    function update_status_checker($claimID, $isStatus){
        $this->db->select('a.checkerID');
        $this->db->from('checker_details a');
        $this->db->where('a.claimID', $claimID);
        $query = $this->db->get();
        $row = $query->row_array();

        $this->db->where('id', $row['checkerID']);
        $this->db->update('checker', array('isStatus' => $isStatus));
        if ($this->db->affected_rows() == '1') {
            return ['id' => $row['checkerID']];
        } else {
            if ($this->db->trans_status() === FALSE) {
                return ['id' => 'update status checker', 'error' => $this->db->error(), 'query' => $this->db->last_query()];
            }
            return ['id' => $row['checkerID']];
        }
    }

    function get_checker_details($claimID){
        $this->db->select('a.*');
        $this->db->from('checker_details a');
        $this->db->where('a.claimID', $claimID);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_checker_details_gift($claimID){
        $this->db->select('a.*');
        $this->db->from('checker_details a');
        $this->db->where('a.claimID', $claimID);
        $query = $this->db->get();
        return $query->result_array();
    }

    function checkBarcode($params) {
        $claimID = $params['claimID'];
        $giftKey = $params['giftKey'];
        $barcodeValue = $params['barcodeValue'];

        $sql = "SELECT COLUMN_NAME 
                FROM INFORMATION_SCHEMA.COLUMNS 
                WHERE TABLE_SCHEMA='sqtfsura_0220' 
                    AND TABLE_NAME='claimed_details'
                    AND COLUMN_NAME LIKE 'barcode_{$giftKey}%'";
                    
        $query = $this->db->query($sql);
        if ($query && $query->num_rows() > 0) {
            $columns = [];
            foreach ($query->result_array() as $key => $value) {
                array_push($columns, $value['COLUMN_NAME']);
            }

            $selectColumns = implode(', ', $columns);

            $sql2 = "SELECT {$selectColumns} FROM claimed_details 
                    WHERE claimID = '{$claimID}'";
            $query2 = $this->db->query($sql2);
            if ($query2 && $query2->num_rows() > 0) {
                foreach ($query2->result_array() as $key => $value) {
                    $selectColumn = ($query->num_rows > 1 ? ('barcode_' . $giftKey . '_' . ($key+1)) : ('barcode_' . $giftKey));
                    if ($value[$selectColumn] == $barcodeValue) {
                        return [
                            'status' => 200,
                            'data'  => false,
                        ]; 
                    }
                }
            }
        }

        return [
            'status' => 200,
            'data' => true,
            'dump' => true
        ];
    }

    function checkCheckerDetails($dataPNR, $claimID, $giftKey){
        $columns = 'total_'.$giftKey.' as total';

        $this->db->select('id, detailID');
        $this->db->select($columns, true);
        $this->db->from('checker_details');
        $this->db->where('claimID',$claimID);
        $query = $this->db->get();

        $push = [];
        if ($query && $query->num_rows() > 0) {
            foreach ($query->result_array() as $key => $value) {
                if($value['total'] > 1){
                    $params = [
                        'id'    => $value['detailID'],
                        'barcode_' . $giftKey . '_' . ($key+1) => $dataPNR[$key]['barcodeValue']
                    ];
                } else {
                    $params = [
                        'id'    => $value['detailID'],
                        'barcode_' . $giftKey => $dataPNR[$key]['barcodeValue']
                    ];
                }

                //print_r($params);

                $this->db->where('id', $params['id']);
                $this->db->update('claimed_details', $params);
                if ($this->db->affected_rows() == '1') {
                    //return true;
                    $push[] = $params;
                } else {
                    if ($this->db->trans_status() === FALSE) {
                        //return false;
                    }
                    $push[] = $params;
                    //return true;
                }
            }
        }

        return $push;
    }

    function get_ticket_by_id($id){
        $this->db->select('a.*, b.*');
        $this->db->from('claimed_details a');
        $this->db->join('claimed b','a.claimID=b.id');
        $this->db->where('a.id',$id);
        $query = $this->db->get();
        return $query->row_array();
    }
}