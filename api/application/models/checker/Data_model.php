<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Data_model extends CI_Model
{   
    function search_pnr_ticket($pnr,$ticket){
        $this->db->select('md5(b.id) as id', true);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b','b.id=a.claimID');
        if(!empty($pnr)){
            $this->db->like('b.pnr', $pnr);
        }
        if(!empty($ticket)){
            $this->db->like('CONCAT("618",a.ticketNo)', $ticket, false);
        }
        $this->db->order_by('b.id','desc');
        $query = $this->db->get();
        return $query->row_array();
    }

    function det_by_pnr($md5_id){
        $this->db->select('b.pnr, c.agentName, d.staffName, e.classCode as depClass, f.classCode as arrClass');
        $this->db->select('DATE_FORMAT(b.depDate,"%d %b %Y") depdate', true);
        $this->db->select('DATE_FORMAT(b.arrDate,"%d %b %Y") arrdate', true);
        $this->db->select('IF(b.tripType=0,"RETURN","OPEN JAW") tripType', false);
        $this->db->select('CONCAT(b.tripFrom_1," ",b.tripFrom_2," ",b.tripFrom_3) route_departure', false);
        $this->db->select('CONCAT(b.tripTo_1," ",b.tripTo_2," ",b.tripTo_3) route_return', false);
        $this->db->select('(SELECT COUNT(*) FROM claimed_details a WHERE a.claimID=b.id) no_ticket', false);
        $this->db->select('(SELECT COUNT(*) FROM claimed_details a WHERE a.isStatus<2 AND a.claimID=b.id) no_waitinglist', false);
        $this->db->select('(SELECT COUNT(*) FROM claimed_details a WHERE a.isStatus=2 AND a.claimID=b.id) no_claimed', false);
        $this->db->from('claimed b');
        $this->db->join('ms_agent c','b.agentID=c.id');
        $this->db->join('ms_agent_staff d','b.createdBy=d.id');
        $this->db->join('ms_class e','b.classDep=e.id');
        $this->db->join('ms_class f','b.classArr=f.id');
        $this->db->where('md5(b.id)', $md5_id, true);
        $this->db->order_by('b.id','desc');
        $query = $this->db->get();
        return $query->row_array();
    }

    function det_by_pnr_all($md5_id){
        $this->db->select('b.*');
        $this->db->from('claimed b');
        $this->db->where('md5(b.id)', $md5_id, true);
        $this->db->order_by('b.id','desc');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_ticket_by_pnr($md5_id){
        $this->db->select('b.id, b.ticketNo, b.kfNo, b.isStatus');
        $this->db->select('IFNULL((SELECT bb.queueCode FROM checker_details aa JOIN checker bb ON aa.checkerID=bb.id WHERE aa.detailID=b.id),"") queueCode', true);
        $this->db->from('claimed_details b');
        $this->db->where('md5(b.claimID)', $md5_id, true);
        $this->db->order_by('b.ticketNo','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_ticket_by_id($id){
        $this->db->select('a.id, a.claimID, a.ticketNo, a.kfNo, e.total_1 as gift_1, e.total_2 as gift_2, e.total_3 as gift_3, e.total_4 as gift_4, e.total_5 as gift_5, e.total_6 as gift_6, e.total_7 as gift_7, e.total_8 as gift_8, e.total_9 as gift_9, b.pnr, b.classDep, c.classCode as classDepName, b.classArr, d.classCode as classArrName, b.tripFrom_3, b.tripTo_1, MONTH(b.depDate) as month_depdate, YEAR(b.depDate) as year_depdate, b.depDate, b.arrDate', true);
        $this->db->select('DATE_FORMAT(b.depDate,"%d %b %Y") depdate', true);
        $this->db->select('DATE_FORMAT(b.arrDate,"%d %b %Y") arrdate', true);
        $this->db->select('CONCAT(b.tripFrom_1," ",b.tripFrom_2," ",b.tripFrom_3) route_departure', false);
        $this->db->select('CONCAT(b.tripTo_1," ",b.tripTo_2," ",b.tripTo_3) route_return', false);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b','a.claimID=b.id');
        $this->db->join('ms_class c','b.classDep=c.id');
        $this->db->join('ms_class d','b.classArr=d.id');
        $this->db->join('checker_details e','e.detailID=a.id','left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        return $query->row_array();
    }

    function get_checker_details($md5_id){
        $this->db->select('b.detailID as id');
        $this->db->from('checker_details b');
        $this->db->where('md5(b.checkerID)', $md5_id, true);
        $query = $this->db->get();
        return $query->result_array();
    }

    function check_region($airportcode){
        $this->db->select('a.airportCode, b.regionCode, b.noofcdv');
        $this->db->from('ms_route a');
        $this->db->join('ms_region b','a.regionID=b.id');
        $this->db->where('a.airportCode', $airportcode);
        $query = $this->db->get();
        return $query->row_array();
    }

    function update_status_claimed_details($id, $status){
        $this->db->where('id', $id);
        $this->db->update('claimed_details', array('isStatus' => $status));
        if ($this->db->affected_rows() == '1') {
            return ['id' => $id];
        } else {
            if ($this->db->trans_status() === FALSE) {
                return ['id' => 'update claimed_details', 'error' => $this->db->error(), 'query' => $this->db->last_query()];
            }
            return ['id' => $id];
        }
    }

    function insert_checker($data){
        if ($this->db->insert('checker', $data)) {
            return ['id' => $this->db->insert_id()];
        } else {
            return ['id' => '', 'error' => $this->db->error(), 'query' => $this->db->last_query()];
        }
    }

    function insert_checker_details($data){
        if ($this->db->insert('checker_details', $data)) {
            return ['id' => $this->db->insert_id()];
        } else {
            return ['id' => '', 'error' => $this->db->error(), 'query' => $this->db->last_query()];
        }
    }

    function det_checker($pnr){
        $this->db->select('a.*')
            ->from('checker a')
            ->like('a.queueCode', $pnr);
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        $arr = array();
        if ($query->num_rows()>0) {
            $row = $query->row_array();

            $this->db->select('b.pnr, COUNT(*) noofticket, SUM(a.total_1) gift_1, SUM(a.total_2) gift_2, SUM(a.total_3) gift_3, SUM(a.total_4) gift_4, SUM(a.total_5) gift_5, SUM(a.total_6) gift_6, SUM(a.total_7) gift_7, SUM(a.total_8) gift_8, SUM(a.total_9) gift_9')
                ->from('checker_details a')
                ->join('claimed b','b.id=a.claimID')
                ->where('a.checkerID', $row['id'])
                ->group_by('a.claimID');
            $query_group = $this->db->get();

            $row_group = $query_group->result_array();
            $arr_group = array();
            foreach ($row_group as $key_group => $value_group) {
                array_push($arr_group, [
                    'pnr'       => $value_group['pnr'],
                    'noofticket'=> $value_group['noofticket'],
                    'gift_1'    => $value_group['gift_1'],
                    'gift_2'    => $value_group['gift_2'],
                    'gift_3'    => $value_group['gift_3'],
                    'gift_4'    => $value_group['gift_4'],
                    'gift_5'    => $value_group['gift_5'],
                    'gift_6'    => $value_group['gift_6'],
                    'gift_7'    => $value_group['gift_7'],
                    'gift_8'    => $value_group['gift_8'],
                    'gift_9'    => $value_group['gift_9'],
                ]);
            }

            $arr = [
                'queueDate'  => date('d M Y', strtotime($row['queueDate'])),
                'queueCode'  => $row['queueCode'],                
                'details'     => $arr_group,
            ];
        }
        
        return $arr;
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('claimed', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_claimed_details($id){
        $this->db->select('b.id, b.noofticket');
        $this->db->from('claimed_details a');
        $this->db->join('claimed b', 'a.claimID=b.id');
        $this->db->where('a.id',$id);
        $query = $this->db->get();
        $row = $query->row_array();


        $this->db->set('noofticket',$row['noofticket']-1);
        $this->db->where('id',$row['id']);
        $this->db->update('claimed');
        if ($this->db->affected_rows() == '1') {
            $this->delete_claimed_details_action($id);
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }            
            return $this->db->last_query();
            $this->delete_claimed_details_action($id);
        }
    }

    function delete_claimed_details_action($id){
        $this->db->where('id', $id);
        $this->db->delete('claimed_details');
        if ($this->db->affected_rows() == '1') {
            return true;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function update_checker_details_bydetailID($data){
        $this->db->where('detailID', $data['detailID']);
        $this->db->update('checker_details', $data);
        if ($this->db->affected_rows() == '1') {
            return ['id' => $data['detailID']];
        } else {
            if ($this->db->trans_status() === FALSE) {
                return ['id' => 'update checker_details', 'error' => $this->db->error(), 'query' => $this->db->last_query()];
            }
            return ['id' => $data['detailID']];
        }
    }

    function update_claimed_details($data){
        $this->db->where('id', $data['id']);
        $this->db->update('claimed_details', $data);
        if ($this->db->affected_rows() == '1') {
            return ['id' => $data['id']];
        } else {
            if ($this->db->trans_status() === FALSE) {
                return ['id' => 'update claimed_details', 'error' => $this->db->error(), 'query' => $this->db->last_query()];
            }
            return ['id' => $data['id']];
        }
    }

}