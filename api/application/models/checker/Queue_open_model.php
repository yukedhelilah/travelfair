<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Queue_open_model extends CI_Model
{   
    public function filter($search, $limit, $start, $order_field, $order_ascdesc, $session){
        $this->db->select('a.*, b.adminName as staff', true)
            ->from('checker a')
            ->join('ms_admin b','a.createdBy = b.id')
            ->where('a.isStatus', 1)
            ->group_start()
            ->like('b.adminName', $search)
            ->or_like('a.queueCode', $search)
            ->or_like('a.noofpnr', $search)
            ->or_like('a.noofticket', $search)
            ->group_end()
            ->order_by($order_field, $order_ascdesc)
            ->limit($limit, $start);
        $query = $this->db->get();
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        return $query->result_array();
    }

    public function count_all($session){
        $this->db->select('a.*, b.adminName as staff')
            ->from('checker a')
            ->join('ms_admin b','a.createdBy = b.id')
            ->where('a.isStatus', 1);
        $query = $this->db->get();
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        return $query->num_rows();
    }

    public function count_filter($search, $session){
        $this->db->select('a.*, b.adminName as staff')
            ->from('checker a')
            ->join('ms_admin b','a.createdBy = b.id')
            ->where('a.isStatus', 1)
            ->group_start()
            ->like('b.adminName', $search)
            ->or_like('a.queueCode', $search)
            ->or_like('a.noofpnr', $search)
            ->or_like('a.noofticket', $search)
            ->group_end();
        $query = $this->db->get();
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        return $query->num_rows();
    }

}