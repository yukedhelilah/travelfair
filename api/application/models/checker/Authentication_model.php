<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Authentication_model extends CI_Model
{   
    function login_staff($data){
        $this->db->select('a.adminName, a.id')
            ->from('ms_admin a')
            ->where('a.username', $data['staffUsername'])
            ->where('a.password', $data['staffPassword'])
            ->where('a.flag', 0);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }

        $arr = array();
        if ($query->num_rows()) {
            $row = $query->row_array();
            $tokenKey = $row['id'] . '|' . $row['adminName'] . '|' . date('Y-m-d H:i:s');
            $arr = [
                'total'         => $query->num_rows(),
                'staff'         => str_rot13(base64_encode($row['id'])),
                'staffName'     => strtoupper($row['adminName']),
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d", strtotime("+1 day")),
            ];
        }else{
            $arr = [
                'total'     => $query->num_rows(),
                'staff'   => ''
            ];
        }

        return $arr;
    }
}