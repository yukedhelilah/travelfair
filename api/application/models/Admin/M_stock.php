<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_stock extends CI_Model
{   
    function get_item(){
        $select = array(
            'id',
            'items'
        );

        $this->db->select($select);
        $this->db->from('ms_item');
        $this->db->order_by('id', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_data(){
        $select = array(
            'a.id',
            'a.itemID',
            'a.stockDate',
            'a.stockTotal',
            'b.items',
        );

        $this->db->select($select);
        $this->db->from('ms_item_stock a');
        $this->db->join('ms_item b','a.itemID=b.id');
        $this->db->where('a.flag', 0);
        $this->db->where('a.stockDate', date('Y-m-d'));
        $this->db->order_by('b.id', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function add_data($data){
        $this->db->insert('ms_item_stock', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('ms_item_stock', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function get_queue(){
        $this->db->select('a.id as ticketID, checkerID');
        $this->db->from('claimed_details a');
        $this->db->join('checker_details b','a.id=b.detailID');
        $this->db->where('a.isStatus', '1');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function delete_checker($checkerID){
        $this->db->from('checker a');
        $this->db->where('id', $checkerID);
        $this->db->delete('checker');

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return ['error' => $this->db->error()];
        }
    }

    function delete_queue($ticketID){
        $this->db->from('checker_details a');
        $this->db->where('detailID', $ticketID);
        $this->db->delete('checker_details');

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return ['error' => $this->db->error()];
        }
    }
}