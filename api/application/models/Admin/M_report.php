<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_report extends CI_Model
{   
    function get_cdv(){
        $select = array(
            'c.agentName',
            'a.createdDate',
            'b.pnr',
            'a.ticketNo',
            'a.kfNo',
            'b.tripFrom_1',
            'b.tripFrom_2',
            'b.tripFrom_3',
            'b.tripTo_1',
            'b.tripTo_2',
            'b.tripTo_3',
            'e.classCode as depClass',
            'f.classCode as arrClass',
            'a.barcode_4_1 as cdv1',
            'a.barcode_4_2 as cdv2',
            'd.adminName',
            'a.claimedDate',
            'a.claimedTime',
        );

        $this->db->select($select);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b','b.id=a.claimID','left');
        $this->db->join('ms_agent c','c.id=b.agentID','left');
        $this->db->join('ms_admin d','d.id=a.claimedBy','left');
        $this->db->join('ms_class e','e.id=b.classDep','left');
        $this->db->join('ms_class f','f.id=b.classArr','left');
        $this->db->where('a.total_4 >', 0);
        $this->db->where('a.isStatus', 2);
        $this->db->order_by('a.claimedDate','desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_cdv_extra(){
        $select = array(
            'c.agentName',
            'a.createdDate',
            'b.pnr',
            'a.ticketNo',
            'a.kfNo',
            'b.tripFrom_1',
            'b.tripFrom_2',
            'b.tripFrom_3',
            'b.tripTo_1',
            'b.tripTo_2',
            'b.tripTo_3',
            'e.classCode as depClass',
            'f.classCode as arrClass',
            'a.barcode_5 as cdv',
            'd.adminName',
            'a.claimedDate',
            'a.claimedTime',
        );

        $this->db->select($select);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b','b.id=a.claimID','left');
        $this->db->join('ms_agent c','c.id=b.agentID','left');
        $this->db->join('ms_admin d','d.id=a.claimedBy','left');
        $this->db->join('ms_class e','e.id=b.classDep','left');
        $this->db->join('ms_class f','f.id=b.classArr','left');
        $this->db->where('a.total_5 >', 0);
        $this->db->where('a.isStatus', 2);
        $this->db->order_by('a.claimedDate','desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_map_per(){
        $select = array(
            'c.agentName',
            'a.createdDate',
            'b.pnr',
            'a.ticketNo',
            'a.kfNo',
            'b.tripFrom_1',
            'b.tripFrom_2',
            'b.tripFrom_3',
            'b.tripTo_1',
            'b.tripTo_2',
            'b.tripTo_3',
            'e.classCode as depClass',
            'f.classCode as arrClass',
            'a.barcode_7_1 as map1',
            'a.barcode_7_2 as map2',
            'd.adminName',
            'a.claimedDate',
            'a.claimedTime',
        );

        $this->db->select($select);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b','b.id=a.claimID','left');
        $this->db->join('ms_agent c','c.id=b.agentID','left');
        $this->db->join('ms_admin d','d.id=a.claimedBy','left');
        $this->db->join('ms_class e','e.id=b.classDep','left');
        $this->db->join('ms_class f','f.id=b.classArr','left');
        $this->db->where('a.total_7 >', 0);
        $this->db->where('a.isStatus', 2);
        $this->db->order_by('a.claimedDate','desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_map_us(){
        $select = array(
            'c.agentName',
            'a.createdDate',
            'b.pnr',
            'a.ticketNo',
            'a.kfNo',
            'b.tripFrom_1',
            'b.tripFrom_2',
            'b.tripFrom_3',
            'b.tripTo_1',
            'b.tripTo_2',
            'b.tripTo_3',
            'e.classCode as depClass',
            'f.classCode as arrClass',
            'a.barcode_8 as map',
            'd.adminName',
            'a.claimedDate',
            'a.claimedTime',
        );

        $this->db->select($select);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b','b.id=a.claimID','left');
        $this->db->join('ms_agent c','c.id=b.agentID','left');
        $this->db->join('ms_admin d','d.id=a.claimedBy','left');
        $this->db->join('ms_class e','e.id=b.classDep','left');
        $this->db->join('ms_class f','f.id=b.classArr','left');
        $this->db->where('a.total_8 >', 0);
        $this->db->where('a.isStatus', 2);
        $this->db->order_by('a.claimedDate','desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_flazz(){
        $select = array(
            'c.agentName',
            'a.createdDate',
            'b.pnr',
            'a.ticketNo',
            'a.kfNo',
            'b.tripFrom_1',
            'b.tripFrom_2',
            'b.tripFrom_3',
            'b.tripTo_1',
            'b.tripTo_2',
            'b.tripTo_3',
            'e.classCode as depClass',
            'f.classCode as arrClass',
            'a.barcode_1 as flazz',
            'd.adminName',
            'a.claimedDate',
            'a.claimedTime',
        );

        $this->db->select($select);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b','b.id=a.claimID','left');
        $this->db->join('ms_agent c','c.id=b.agentID','left');
        $this->db->join('ms_admin d','d.id=a.claimedBy','left');
        $this->db->join('ms_class e','e.id=b.classDep','left');
        $this->db->join('ms_class f','f.id=b.classArr','left');
        $this->db->where('a.total_1 >', 0);
        $this->db->where('a.isStatus', 2);
        $this->db->order_by('a.claimedDate','desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_ezlink(){
        $select = array(
            'c.agentName',
            'a.createdDate',
            'b.pnr',
            'a.ticketNo',
            'a.kfNo',
            'b.tripFrom_1',
            'b.tripFrom_2',
            'b.tripFrom_3',
            'b.tripTo_1',
            'b.tripTo_2',
            'b.tripTo_3',
            'e.classCode as depClass',
            'f.classCode as arrClass',
            'a.barcode_6 as ezlink',
            'd.adminName',
            'a.claimedDate',
            'a.claimedTime',
        );

        $this->db->select($select);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b','b.id=a.claimID','left');
        $this->db->join('ms_agent c','c.id=b.agentID','left');
        $this->db->join('ms_admin d','d.id=a.claimedBy','left');
        $this->db->join('ms_class e','e.id=b.classDep','left');
        $this->db->join('ms_class f','f.id=b.classArr','left');
        $this->db->where('a.total_6 >', 0);
        $this->db->where('a.isStatus', 2);
        $this->db->order_by('a.claimedDate','desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_daily(){
        // $sql = "SELECT c.agentName, b.pnr, a.ticketNo, a.kfNo, b.depDate, 
        // IF(b.tripType=0,'Return','Open Jaw') trip,
        // CONCAT(b.tripFrom_1,' ', b.tripFrom_2,' ',b.tripFrom_3,' (',e.classCode,') - ', b.tripTo_1,' ', b.tripTo_2,' ',b.tripTo_3,' (',f.classCode,')') itin,
        // IF(a.barcode_1='','0',a.`barcode_1`) Flazz,IF(a.`total_2`=0,'0',a.`total_2`) Foldable, IF(a.`total_3`=0,'0',a.`total_3`) Shopping,
        // CASE 
        //     WHEN a.total_4 = 0 THEN '0'
        //     WHEN a.barcode_4_1!='' AND a.barcode_4_2='' THEN a.barcode_4_1
        //     WHEN a.barcode_4_1!='' AND a.barcode_4_2!='' THEN CONCAT(a.barcode_4_1,',',a.barcode_4_2) 
        // END AS CDV, 
        // IF(a.barcode_5='','0',a.barcode_5) CDV_Extra, IF(a.barcode_6='','0',a.barcode_6) EZ_Link,
        // CASE 
        //     WHEN a.total_7 = 0 THEN '0'
        //     WHEN a.total_7=1 THEN CONCAT(a.barcode_7_1,',',a.barcode_7_2) 
        // END AS MAP_PER, IF(a.barcode_8='','0',a.barcode_8) MAP_US, d.adminName, a.claimedDate, a.claimedTime
        // FROM claimed_details a
        // JOIN claimed b ON a.claimID=b.id
        // LEFT JOIN ms_agent c ON b.agentID=c.id
        // LEFT JOIN ms_admin d ON a.claimedBy=d.id
        // LEFT JOIN ms_class e ON e.id=b.classDep
        // LEFT JOIN ms_class f ON f.id=b.classArr
        // WHERE a.isStatus = 2 AND a.claimedDate = '".date('Y-m-d')."'
        // ORDER BY claimedTime ASC";

        $sql = "SELECT d.pnr, d.tripFrom_3, e.classCode as depClass, d.tripTo_1, f.classCode as arrClass, d.depDate, d.arrDate, c.ticketNo, c.kfNo, 
            c.total_1 as flazz, c.barcode_1 as barcodeFlazz, c.total_2 as foldable, c.total_3 shopping, c.total_4 as cdv, c.barcode_4_1 as barcodeCDV1, c.barcode_4_2 as barcodeCDV2, c.total_5 as cdvExtra, c.barcode_5 as barcodeCDVExtra, c.total_6 as ezLink, c.barcode_6 as barcodeEZLink, 
            c.total_7 as mapPER, c.barcode_7_1 as barcodeMAPPER1, c.barcode_7_2 as barcodeMAPPER2, c.total_8 as mapUS, c.barcode_8 as barcodeMAPUS, b.queueCode, g.agentName, c.claimedDate, c.claimedTime, h.adminName
            FROM checker_details a
            JOIN checker b ON a.checkerID=b.id
            JOIN claimed_details c ON a.detailID=c.id
            JOIN claimed d ON c.claimID=d.id
            JOIN ms_class e ON d.classDep=e.id
            JOIN ms_class f ON d.classArr=f.id
            JOIN ms_agent g ON d.agentID=g.id
            JOIN ms_admin h ON c.claimedBy=h.id
            WHERE c.isStatus = 2 AND c.claimedDate = '2020-02-13'
            ORDER BY c.claimedTime, d.pnr";

        $result = $this->db->query($sql);
        $result = $result->result();
        return $result;
    }
}