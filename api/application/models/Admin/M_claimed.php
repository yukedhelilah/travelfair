<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_claimed extends CI_Model
{   
    function get_data(){
        $this->db->select('a.*, IF(a.tripType=0,"Return","Open Jaw") trip, b.classCode as depClass, c.classCode as arrClass, d.agentName');
        $this->db->from('claimed a');
        $this->db->join('ms_class b','b.id=a.classDep');
        $this->db->join('ms_class c','c.id=a.classArr');
        $this->db->join('ms_agent d','d.id=a.agentID');
        $this->db->order_by('a.id', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $value) {
                $this->db->select('a.ticketNo, a.isStatus');
                $this->db->from('claimed_details a');
                $this->db->join('claimed b', 'b.id=a.claimID');
                $this->db->where('claimID', $value->id);

                $query_ticket = $this->db->get();

                if (!$query_ticket) {
                    return ['error' => $this->db->error()];
                }
                
                if ($query_ticket->num_rows()>0) {
                    $ticket        = $query_ticket->result();

                }
                    $arr[$key] = [
                        'id'            => $value->id,
                        'pnr'           => $value->pnr,
                        'issuanceDate'  => $value->issuanceDate,
                        'trip'          => $value->trip,
                        'tripFrom_1'    => $value->tripFrom_1,
                        'tripFrom_2'    => $value->tripFrom_2,
                        'tripFrom_3'    => $value->tripFrom_3,
                        'tripTo_1'      => $value->tripTo_1,
                        'tripTo_2'      => $value->tripTo_2,
                        'tripTo_3'      => $value->tripTo_3,
                        'depClass'      => $value->depClass,
                        'arrClass'      => $value->arrClass,
                        'depDate'       => $value->depDate,
                        'noofticket'    => $value->noofticket,
                        'isStatus'      => $value->isStatus,
                        'agentName'     => $value->agentName,
                        'ticket'        => $ticket,
                    ];
            }

            return $arr;
        }
    }

    function get_details($claimedID){
        $this->db->select('a.*, IF(a.tripType=0,"Return","Open Jaw") trip, b.className as depClass, c.className as arrClass, d.staffName as staffCreated, e.staffName as staffEdited');
        $this->db->from('claimed a');
        $this->db->join('ms_class b','a.classDep=b.id');
        $this->db->join('ms_class c','a.classArr=c.id');
        $this->db->join('ms_agent_staff d','a.createdBy=d.id');
        $this->db->join('ms_agent_staff e','a.lastBy=e.id','left');
        $this->db->where('a.id', $claimedID);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        $arr = array();
        if ($query->num_rows()>0) {
            $row = $query->row();

            $this->db->select('a.*,b.adminName as claimBy');
            $this->db->select('(SELECT aa.total_1 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_1', true);
            $this->db->select('(SELECT aa.total_2 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_2', true);
            $this->db->select('(SELECT aa.total_3 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_3', true);
            $this->db->select('(SELECT aa.total_4 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_4', true);
            $this->db->select('(SELECT aa.total_5 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_5', true);
            $this->db->select('(SELECT aa.total_6 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_6', true);
            $this->db->select('(SELECT aa.total_7 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_7', true);
            $this->db->select('(SELECT aa.total_8 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_8', true);
            $this->db->select('(SELECT aa.total_9 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_9', true);
            $this->db->from('claimed_details a');
            $this->db->join('ms_admin b','a.claimedBy=b.id','left');
            $this->db->where('a.claimID', $claimedID);
        
            $query_ticket = $this->db->get();

            $arr = [
                '_'                 => $row->id,
                'agentID'           => $row->agentID,
                'pnr'               => $row->pnr,
                'issuanceDate'      => $row->issuanceDate,
                'trip'              => $row->trip,
                'tripType'          => $row->tripType,
                'dep'               => $row->tripFrom_1.' '.$row->tripFrom_2.' '.$row->tripFrom_3,
                'arr'               => $row->tripTo_1.' '.$row->tripTo_2.' '.$row->tripTo_3,
                'depClass'          => $row->depClass,
                'arrClass'          => $row->arrClass,
                'classDep'          => $row->classDep,
                'classArr'          => $row->classArr,
                'tripFrom_1'        => $row->tripFrom_1,
                'tripFrom_2'        => $row->tripFrom_2,
                'tripFrom_3'        => $row->tripFrom_3,
                'classDep'          => $row->classDep,
                'tripTo_1'          => $row->tripTo_1,
                'tripTo_2'          => $row->tripTo_2,
                'tripTo_3'          => $row->tripTo_3,
                'classArr'          => $row->classArr,
                'depDate'           => $row->depDate,
                'arrDate'           => $row->arrDate,
                'isStatus'          => $row->isStatus,
                'ticket'            => $query_ticket->result(),
                'noofticket'        => count($query_ticket->result()),
                'createdBy'         => $row->staffCreated,
                'createdDate'       => $row->createdDate,
                'lastBy'            => $row->staffEdited,
                'lastDate'          => $row->lastDate,
                'remarks'           => $row->remarks,
            ];
        }
        
        return $arr;
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('claimed', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function add_details($data){
        $this->db->insert('claimed_details', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_details($data,$id){
        $this->db->where('id',$id);
        $this->db->update('claimed_details', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_details($ticketID){
        $this->db->where('id', $ticketID);  
        return $this->db->delete('claimed_details');
    }
}