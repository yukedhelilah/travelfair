<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_agent extends CI_Model
{   
    function get_data(){
        $select = array(
            'id',
            'agentID',
            'agentName',
        );

        $this->db->select($select);
        $this->db->from('ms_agent');
        $this->db->where('flag', 0);
        $this->db->order_by('agentName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function add_data($data){
        $this->db->insert('ms_agent', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('ms_agent', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function add_staff($data){
        $this->db->insert('ms_agent_staff', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_staff($data,$id){
        $this->db->where('agentID',$id);
        $this->db->where('staffUsername','superid');
        $this->db->update('ms_agent_staff', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}