<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_queue extends CI_Model
{   
    function get_data(){
        $this->db->select('a.*, b.adminName as admin');
        $this->db->from('checker a');
        $this->db->join('ms_admin b','b.id=a.createdBy');
        $this->db->order_by('a.createdDate', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $value) {
                $this->db->select('c.id, c.pnr');
                $this->db->from('checker_details a');
                $this->db->join('checker b','a.checkerID=b.id');
                $this->db->join('claimed c','a.claimID=c.id');
                $this->db->where('a.checkerID', $value->id);
                $this->db->group_by('a.claimID');

                $query_pnr  = $this->db->get();
                $pnr        = $query_pnr->result();

                foreach ($pnr as $keys => $val) {
                    $this->db->select('b.id, b.ticketNo, a.claimID');
                    $this->db->from('checker_details a');
                    $this->db->join('claimed_details b','b.id=a.detailID');
                    $this->db->where('a.claimID', $val->id);
                    $this->db->where('a.checkerID', $value->id);
        
                    $query_ticket  = $this->db->get();

                    if (!$query_ticket) {
                        return ['error' => $this->db->error()];
                    }
                    
                    if ($query_ticket->num_rows()>0) {
                        $ticket        = $query_ticket->result();

                        $pnr[$keys] = [
                            'id'        => $val->id,
                            'pnr'       => $val->pnr,
                            'ticket'    => $ticket,
                        ];
                    } else {
                        $pnr[$keys] = [
                            'id'        => $val->id,
                            'pnr'       => $val->pnr,
                            'ticket'    => 0,
                        ];
                    }
                }

                $arr[$key] = [
                    'id'                => $value->id,
                    'code'              => md5($value->id),
                    'createdBy'         => $value->admin,
                    'createdDate'       => date('Y-m-d', strtotime($value->createdDate)),
                    'createdTime'       => date('H:i', strtotime($value->createdDate)),
                    'queueCode'         => $value->queueCode,
                    'noofticket'        => $value->noofticket,
                    'noofpnr'           => $value->noofpnr,
                    'isStatus'          => $value->isStatus,
                    'detail'            => $pnr,
                    'guestName'         => $value->guestName,
                    'guestPhone'        => $value->guestPhone,
                ];
            }

            return $arr;
        }
    }

    function get_details($claimedID){
        $this->db->select('a.*, IF(a.tripType=0,"Return","Open Jaw") trip, b.className as depClass, c.className as arrClass, d.staffName as staffCreated, e.staffName as staffEdited');
        $this->db->from('claimed a');
        $this->db->join('ms_class b','a.classDep=b.id');
        $this->db->join('ms_class c','a.classArr=c.id');
        $this->db->join('ms_agent_staff d','a.createdBy=d.id');
        $this->db->join('ms_agent_staff e','a.lastBy=e.id','left');
        $this->db->where('a.id', $claimedID);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        $arr = array();
        if ($query->num_rows()>0) {
            $row = $query->row();

            $this->db->select('a.*,b.adminName as claimBy');
            $this->db->select('(SELECT aa.total_1 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_1', true);
            $this->db->select('(SELECT aa.total_2 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_2', true);
            $this->db->select('(SELECT aa.total_3 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_3', true);
            $this->db->select('(SELECT aa.total_4 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_4', true);
            $this->db->select('(SELECT aa.total_5 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_5', true);
            $this->db->select('(SELECT aa.total_6 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_6', true);
            $this->db->select('(SELECT aa.total_7 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_7', true);
            $this->db->select('(SELECT aa.total_8 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_8', true);
            $this->db->select('(SELECT aa.total_9 FROM checker_details aa WHERE aa.detailID=a.id) queue_gift_9', true);
            $this->db->from('claimed_details a');
            $this->db->join('ms_admin b','a.claimedBy=b.id','left');
            $this->db->where('a.claimID', $claimedID);
        
            $query_ticket = $this->db->get();

            $arr = [
                '_'                 => $row->id,
                'agentID'           => $row->agentID,
                'pnr'               => $row->pnr,
                'issuanceDate'      => $row->issuanceDate,
                'trip'              => $row->trip,
                'tripType'          => $row->tripType,
                'dep'               => $row->tripFrom_1.' '.$row->tripFrom_2.' '.$row->tripFrom_3,
                'arr'               => $row->tripTo_1.' '.$row->tripTo_2.' '.$row->tripTo_3,
                'depClass'          => $row->depClass,
                'arrClass'          => $row->arrClass,
                'classDep'          => $row->classDep,
                'classArr'          => $row->classArr,
                'tripFrom_1'        => $row->tripFrom_1,
                'tripFrom_2'        => $row->tripFrom_2,
                'tripFrom_3'        => $row->tripFrom_3,
                'classDep'          => $row->classDep,
                'tripTo_1'          => $row->tripTo_1,
                'tripTo_2'          => $row->tripTo_2,
                'tripTo_3'          => $row->tripTo_3,
                'classArr'          => $row->classArr,
                'depDate'           => $row->depDate,
                'arrDate'           => $row->arrDate,
                'isStatus'          => $row->isStatus,
                'ticket'            => $query_ticket->result(),
                'noofticket'        => count($query_ticket->result()),
                'createdBy'         => $row->staffCreated,
                'createdDate'       => $row->createdDate,
                'lastBy'            => $row->staffEdited,
                'lastDate'          => $row->lastDate,
            ];
        }
        
        return $arr;
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('claimed', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function add_details($data){
        $this->db->insert('claimed_details', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_details($data,$id){
        $this->db->where('id',$id);
        $this->db->update('claimed_details', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_details($ticketID){
        $this->db->where('id', $ticketID);  
        return $this->db->delete('claimed_details');
    }
}