<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_dashboard extends CI_Model
{     
    function all_claimed(){
        $this->db->select('COUNT(a.id) total', 0);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b', 'a.claimID=b.id');
        $this->db->where('a.isStatus', '2');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row();
            return $row->total;
        }
    }

    function today_claimed(){
        $this->db->select('COUNT(a.id) total', 0);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b', 'a.claimID=b.id');
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row();
            return $row->total;
        }
    }

    function on_queue(){
        $this->db->select('COUNT(a.id) total', 0);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b', 'a.claimID=b.id');
        $this->db->where('a.isStatus', '1');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row();
            return $row->total;
        }
    }

    function unclaimed(){
        $this->db->select('COUNT(a.id) total', 0);
        $this->db->from('claimed_details a');
        $this->db->join('claimed b', 'a.claimID=b.id');
        $this->db->where('a.isStatus', '0');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row();
            return $row->total;
        }
    }

    function gift1(){
        $this->db->select('IFNULL(stockTotal, 0) as stock');
        $this->db->from('ms_item_stock a');
        $this->db->where('a.itemID', '1');
        $this->db->where('a.stockDate', date('Y-m-d'));
        $query = $this->db->get();

        $this->db->select('IFNULL(SUM(a.total_1),0) AS claimed');
        $this->db->from('claimed_details a');
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));
        $query1 = $this->db->get();

        $this->db->select('IFNULL(SUM(b.total_1),0) AS queue');
        $this->db->from('claimed_details a');
        $this->db->join('checker_details b','a.id=b.detailID');
        $this->db->where('a.isStatus', '1');
        $query2 = $this->db->get();
        
        if (!$query || !$query1 || !$query2) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0 && $query1->num_rows()>0 && $query2->num_rows()>0) {
            $row        = $query->row();
            $row1       = $query1->row();
            $row2       = $query2->row();
            $balance    = $row->stock - ($row1->claimed + $row2->queue);

            return [
                'stock'     => $row->stock,
                'claimed'   => $row1->claimed,
                'queue'     => $row2->queue,
                'balance'   => "$balance"
            ];
        }
    }

    function gift2(){
        $this->db->select('IFNULL(stockTotal, 0) as stock');
        $this->db->from('ms_item_stock a');
        $this->db->where('a.itemID', '2');
        $this->db->where('a.stockDate', date('Y-m-d'));
        $query = $this->db->get();

        $this->db->select('IFNULL(SUM(a.total_2),0) AS claimed');
        $this->db->from('claimed_details a');
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));
        $query1 = $this->db->get();

        $this->db->select('IFNULL(SUM(b.total_2),0) AS queue');
        $this->db->from('claimed_details a');
        $this->db->join('checker_details b','a.id=b.detailID');
        $this->db->where('a.isStatus', '1');
        $query2 = $this->db->get();
        
        if (!$query || !$query1 || !$query2) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0 && $query1->num_rows()>0 && $query2->num_rows()>0) {
            $row        = $query->row();
            $row1       = $query1->row();
            $row2       = $query2->row();
            $balance    = $row->stock - ($row1->claimed + $row2->queue);

            return [
                'stock'     => $row->stock,
                'claimed'   => $row1->claimed,
                'queue'     => $row2->queue,
                'balance'   => "$balance"
            ];
        }
    }

    function gift3(){
        $this->db->select('IFNULL(stockTotal, 0) as stock');
        $this->db->from('ms_item_stock a');
        $this->db->where('a.itemID', '3');
        $this->db->where('a.stockDate', date('Y-m-d'));
        $query = $this->db->get();

        $this->db->select('IFNULL(SUM(a.total_3),0) AS claimed');
        $this->db->from('claimed_details a');
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));
        $query1 = $this->db->get();

        $this->db->select('IFNULL(SUM(b.total_3),0) AS queue');
        $this->db->from('claimed_details a');
        $this->db->join('checker_details b','a.id=b.detailID');
        $this->db->where('a.isStatus', '1');
        $query2 = $this->db->get();
        
        if (!$query || !$query1 || !$query2) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0 && $query1->num_rows()>0 && $query2->num_rows()>0) {
            $row        = $query->row();
            $row1       = $query1->row();
            $row2       = $query2->row();
            $balance    = $row->stock - ($row1->claimed + $row2->queue);

            return [
                'stock'     => $row->stock,
                'claimed'   => $row1->claimed,
                'queue'     => $row2->queue,
                'balance'   => "$balance"
            ];
        }
    }

    function gift4(){
        $this->db->select('IFNULL(stockTotal, 0) as stock');
        $this->db->from('ms_item_stock a');
        $this->db->where('a.itemID', '4');
        $this->db->where('a.stockDate', date('Y-m-d'));
        $query = $this->db->get();

        $this->db->select('IFNULL(SUM(a.total_4),0) AS claimed');
        $this->db->from('claimed_details a');
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));
        $query1 = $this->db->get();

        $this->db->select('IFNULL(SUM(b.total_4),0) AS queue');
        $this->db->from('claimed_details a');
        $this->db->join('checker_details b','a.id=b.detailID');
        $this->db->where('a.isStatus', '1');
        $query2 = $this->db->get();
        
        if (!$query || !$query1 || !$query2) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0 && $query1->num_rows()>0 && $query2->num_rows()>0) {
            $row        = $query->row();
            $row1       = $query1->row();
            $row2       = $query2->row();
            $balance    = $row->stock - ($row1->claimed + $row2->queue);

            return [
                'stock'     => $row->stock,
                'claimed'   => $row1->claimed,
                'queue'     => $row2->queue,
                'balance'   => "$balance"
            ];
        }
    }

    function gift5(){
        $this->db->select('IFNULL(stockTotal, 0) as stock');
        $this->db->from('ms_item_stock a');
        $this->db->where('a.itemID', '5');
        $this->db->where('a.stockDate', date('Y-m-d'));
        $query = $this->db->get();

        $this->db->select('IFNULL(SUM(a.total_5),0) AS claimed');
        $this->db->from('claimed_details a');
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));
        $query1 = $this->db->get();

        $this->db->select('IFNULL(SUM(b.total_5),0) AS queue');
        $this->db->from('claimed_details a');
        $this->db->join('checker_details b','a.id=b.detailID');
        $this->db->where('a.isStatus', '1');
        $query2 = $this->db->get();
        
        if (!$query || !$query1 || !$query2) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0 && $query1->num_rows()>0 && $query2->num_rows()>0) {
            $row        = $query->row();
            $row1       = $query1->row();
            $row2       = $query2->row();
            $balance    = $row->stock - ($row1->claimed + $row2->queue);

            return [
                'stock'     => $row->stock,
                'claimed'   => $row1->claimed,
                'queue'     => $row2->queue,
                'balance'   => "$balance"
            ];
        }
    }

    function gift6(){
        $this->db->select('IFNULL(stockTotal, 0) as stock');
        $this->db->from('ms_item_stock a');
        $this->db->where('a.itemID', '6');
        $this->db->where('a.stockDate', date('Y-m-d'));
        $query = $this->db->get();

        $this->db->select('IFNULL(SUM(a.total_6),0) AS claimed');
        $this->db->from('claimed_details a');
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));
        $query1 = $this->db->get();

        $this->db->select('IFNULL(SUM(b.total_6),0) AS queue');
        $this->db->from('claimed_details a');
        $this->db->join('checker_details b','a.id=b.detailID');
        $this->db->where('a.isStatus', '1');
        $query2 = $this->db->get();
        
        if (!$query || !$query1 || !$query2) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0 && $query1->num_rows()>0 && $query2->num_rows()>0) {
            $row        = $query->row();
            $row1       = $query1->row();
            $row2       = $query2->row();
            $balance    = $row->stock - ($row1->claimed + $row2->queue);

            return [
                'stock'     => $row->stock,
                'claimed'   => $row1->claimed,
                'queue'     => $row2->queue,
                'balance'   => "$balance"
            ];
        }
    }

    function gift7(){
        $this->db->select('IFNULL(stockTotal, 0) as stock');
        $this->db->from('ms_item_stock a');
        $this->db->where('a.itemID', '7');
        $this->db->where('a.stockDate', date('Y-m-d'));
        $query = $this->db->get();

        $this->db->select('IFNULL(SUM(a.total_7),0) AS claimed');
        $this->db->from('claimed_details a');
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));
        $query1 = $this->db->get();

        $this->db->select('IFNULL(SUM(b.total_7),0) AS queue');
        $this->db->from('claimed_details a');
        $this->db->join('checker_details b','a.id=b.detailID');
        $this->db->where('a.isStatus', '1');
        $query2 = $this->db->get();
        
        if (!$query || !$query1 || !$query2) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0 && $query1->num_rows()>0 && $query2->num_rows()>0) {
            $row        = $query->row();
            $row1       = $query1->row();
            $row2       = $query2->row();
            $balance    = $row->stock - ($row1->claimed + $row2->queue);

            return [
                'stock'     => $row->stock,
                'claimed'   => $row1->claimed,
                'queue'     => $row2->queue,
                'balance'   => "$balance"
            ];
        }
    }

    function gift8(){
        $this->db->select('IFNULL(stockTotal, 0) as stock');
        $this->db->from('ms_item_stock a');
        $this->db->where('a.itemID', '8');
        $this->db->where('a.stockDate', date('Y-m-d'));
        $query = $this->db->get();

        $this->db->select('IFNULL(SUM(a.total_8),0) AS claimed');
        $this->db->from('claimed_details a');
        $this->db->where('a.isStatus', '2');
        $this->db->where('a.claimedDate', date('Y-m-d'));
        $query1 = $this->db->get();

        $this->db->select('IFNULL(SUM(b.total_8),0) AS queue');
        $this->db->from('claimed_details a');
        $this->db->join('checker_details b','a.id=b.detailID');
        $this->db->where('a.isStatus', '1');
        $query2 = $this->db->get();
        
        if (!$query || !$query1 || !$query2) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0 && $query1->num_rows()>0 && $query2->num_rows()>0) {
            $row        = $query->row();
            $row1       = $query1->row();
            $row2       = $query2->row();
            $balance    = $row->stock - ($row1->claimed + $row2->queue);

            return [
                'stock'     => $row->stock,
                'claimed'   => $row1->claimed,
                'queue'     => $row2->queue,
                'balance'   => "$balance"
            ];
        }
    }
}