<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_airport extends CI_Model
{   
    function get_data(){
        $select = array(
            'a.id',
            'a.regionID',
            'a.airportCode',
            'a.airportName',
            'a.airportCountry',
            'b.region'
        );

        $this->db->select($select);
        $this->db->from('ms_route a');
        $this->db->join('ms_region b','a.regionID=b.id');
        $this->db->where('a.flag', 0);
        $this->db->order_by('airportCode', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function add_data($data){
        $this->db->insert('ms_route', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('ms_route', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}