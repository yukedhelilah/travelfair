<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_class extends CI_Model
{   
    function get_data(){
        $select = array(
            'id',
            'className',
            'classCode',
        );

        $this->db->select($select);
        $this->db->from('ms_class');
        $this->db->where('flag', 0);
        $this->db->order_by('id', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function add_data($data){
        $this->db->insert('ms_class', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('ms_class', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}