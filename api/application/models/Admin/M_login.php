<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_login extends CI_Model
{   

    function login($username, $password){
        $this->db->select('username,adminName,id,password');
        $this->db->from('ms_admin');
        $this->db->where('level', '0');
        $this->db->where('flag', '0');
        $this->db->where('username', $username);
        $this->db->where('password', $password);

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $password = $row['password'] == md5('1234') ? '0' : '1';

            $tokenKey = $row['username'] . '|' . date('Y-m-d H:i:s');
            return [
                'id'            => $row['id'],
                'name'          => $row['adminName'],
                'username'      => $row['username'],
                'password'      => $password,
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  

    function log($id,$data){
        $this->db->where('id',$id);
        $this->db->update('ms_admin', $data);
        if($this->db->affected_rows()){
            return true;
        }else{
            return false;
        }
    }

    function change_password($id,$data){
        $this->db->where('id',$id);
        $this->db->update('ms_admin', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}