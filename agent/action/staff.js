localStorage.removeItem(btoa('agent_claimedID'));

$(document).ready(function() {
	get_data();
});

function get_data(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Agent/staff/get_data/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/agent/staff/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            agentID 	: agentID
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var button		= "";

                if (value.staffUsername == 'superid') {
                	button 	= `<div style="text-align:center">
		                    	<div data-toggle="tooltip" title="Edit" onclick="edit_data('`+value.id+`','`+value.staffName+`','`+value.staffUsername+`')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
		                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
		                		</div>
		            		</div>`;
                } else {
                	button 	= `<div style="text-align:center">
		                    	<div data-toggle="tooltip" title="Edit" onclick="edit_data('`+value.id+`','`+value.staffName+`','`+value.staffUsername+`')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
		                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
		                		</div>
		                    	<div data-toggle="tooltip" title="Delete" onclick="delete_data(`+value.id+`)" class="btn btn-xs btn-danger" style="padding: 1px 5px;">
		                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-trash"></i>
		                		</div>
		            		</div>`;
                }

                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    `<span style="text-align:center">` + value.staffName + `</span>`, 
                    value.staffUsername, 
                    value.lastLogin,
                    button,
                ]);
            });
            $('#tb_agent').DataTable().destroy();
        	$('#tb_agent').DataTable({
				'responsive': true,
				'language'	: {
					'searchPlaceholder'		: 'Search...',
					'sSearch'				: '',
					'lengthMenu'			: '_MENU_ items/page',
			  	},
				'aaData'	: aaData,
				"bJQueryUI"	: true,
				"aoColumns"	: [
					{ "sWidth"	: "5%" },  
					{ "sWidth"	: "28%" }, 
					{ "sWidth"	: "28%" }, 
					{ "sWidth"	: "28%" }, 
					{ "sWidth"	: "7%" },
				],
				'aLengthMenu'	: [
					[10, 20, 50, -1],
					[10, 20, 50, "All"]
				],
			});
        },
    });
}

function add_data(){
	$('#modal_add').modal('show');
	$('#modal_tittle').html('Add New Data');
	/*$('#add_password').css('display','block');*/
	$('#edit_password').css('display','none');
	$('#default_password').css('display','block');
	$('#act').val('add');
	$('#id').val('');
	$('#staffName').val('');
	$('#staffUsername').val('');
	$('#staffUsername').prop('readonly',false);
	$('#staffPassword').val('');
}

function edit_data(id,name,username){
	$('#modal_add').modal('show');
	$('#modal_tittle').html('Edit Data');
	$('#default_password').css('display','none');
	$/*('#add_password').css('display','none');*/
	$('#edit_password').css('display','block');
	$('#act').val('edit');
	$('#id').val(id);
	$('#staffName').val(name);
	$('#staffUsername').prop('readonly',true);
	$('#staffUsername').val(username);
	$('#staffPassword').val('');
	$('#resetPassword').prop('checked',false);
}

function delete_data(id){
	$('#modal_delete').modal('show');
    $('#delete_data').click(function() {
	    $.ajax({
	        type: 'POST',
	        dataType: 'JSON',
       	 	url: 'http://sqtfsurabaya.com/api/Agent/staff/delete_data/_',
	        // url: 'http://localhost/travelfair/sqtf_0220/api/agent/staff/delete_data/_',
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded'
	        },
	        data: {
	            token	: '1b787b70ed2e0de697d731f14b5da57b',
	            id		: id
	        },
	        success: function(response) {
        		if (response.code == 200) {
        			alert('success');
					$('#modal_delete').modal('hide');
		        	get_data();
		        } else {
        			alert('failed, try again later');
					$('#modal_delete').modal('hide');
		        	get_data();

		        }
	        },
	    });
    });
}

function save(){
	if ($('#staffName').val() == '' || $('#staffUsername').val() == '') {
		alert('Data must be required!');
	} else {
	    $.ajax({
	        type: 'POST',
	        dataType: 'JSON',
        	url: 'http://sqtfsurabaya.com/api/Agent/staff/save_data/_',
	        // url: 'http://localhost/travelfair/sqtf_0220/api/agent/staff/save_data/_',
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded'
	        },
	        data: $('#form_staff').serialize()+"&agentID="+agentID ,
	        success: function(response) {
	        	if (response.code == 200) {
	        		alert('success');
					$('#modal_add').modal('hide');
		        	get_data();
	        	} else {
	        		alert('failed, try again later');
					$('#modal_add').modal('hide');
		        	get_data();
	        	}
	        },
	    });
	}
}