var agentID     = atob(localStorage.getItem(btoa("agent_agentID")));
var staffID     = atob(localStorage.getItem(btoa("agent_staffID")));
var agentName   = atob(localStorage.getItem(btoa("agent_agentName")));
var staffName   = atob(localStorage.getItem(btoa("agent_staffName")));
var password    = atob(localStorage.getItem(btoa("agent_password")));
var token    	= atob(localStorage.getItem(btoa("agent_token")));
var expired     = localStorage.getItem(btoa("agent_expired"));
var exp         = new Date(atob(expired));
var now         = new Date();

var bulans          = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
var days            = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

if(agentID == null || staffID == null || agentName == null || staffName == null || token == null || expired == null || exp.getDate() +`-`+ exp.getMonth() +`-`+ exp.getFullYear() != now.getDate() +`-`+ now.getMonth() +`-`+ now.getFullYear()){
    removeLocalstorage();
    window.location.href = '../pages/login';
}

if (staffName == "SUPER ID") {
    $('#nav_new_claim').css('display','none');
}

if (staffName != "SUPER ID") {
    $('#nav_staff').css('display','none');
    $('#alert_superid').css('display','none');
}

if (password == 0) {
    $('#modal_change_password').modal({backdrop: 'static', keyboard: false});
    $('#staffID').val(staffID);
}

$(document).ready(function() {
    $('#loginName').html(`&nbsp&nbsp`+staffName+` | `+agentName);
});

function dateFormat(date) {
    var format = new Date(date);
    var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
    return date + ' ' + bulans[format.getMonth()] + ' ' + format.getFullYear().toString().substr(2,2);
}

function changePassword(){
    if($('#password').val() == ''){
        alert('Password must be required');
    } else if ($('#password').val().length != 4) {
        alert('Password must be 4 digits');
    } else if ($('#password').val() == '1234'){
        alert('Password must not be 1234');
    } else {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'http://sqtfsurabaya.com/api/Agent/login/change_password/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/Agent/login/change_password/_',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                token           : '1b787b70ed2e0de697d731f14b5da57b',
                id              : staffID,
                password        : $('#password').val()
            },
            success: function(response) {
                alert('success');
                removeLocalstorage();
                window.location.href = '../pages/login';
            },
        });
    }
}

function logout() {
    if (confirm("Do you want to logout?")) {
        removeLocalstorage();
        window.location.href = '../pages/login';
    } 
}

function removeLocalstorage(){
    localStorage.removeItem(btoa('agent_agentID'));
    localStorage.removeItem(btoa('agent_staffID'));
    localStorage.removeItem(btoa('agent_agentName'));
    localStorage.removeItem(btoa('agent_staffName'));
    localStorage.removeItem(btoa('agent_password'));
    localStorage.removeItem(btoa('agent_token'));
    localStorage.removeItem(btoa('agent_expired'));
    localStorage.removeItem(btoa('agent_claimedID'));
}