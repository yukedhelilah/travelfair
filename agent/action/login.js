$('input[name="agentID"]').focus();

$(document).ready(function() { 
    if (localStorage.getItem(btoa('agent_expired')) != null && localStorage.getItem(btoa('agent_token')) != null && localStorage.getItem(btoa('agent_agentID')) != null && localStorage.getItem(btoa('agent_staffID')) != null && localStorage.getItem(btoa('agent_agentName')) != null && localStorage.getItem(btoa('agent_staffName')) != null) {
        window.location.href = '../pages/dashboard';
    }
    
    $('#password').keypress(function(e) {
        if(e.which == 13) {
            login($("#agentID").val(),$("#username").val(),$("#password").val());
        }
    });

    $('#login').click(function() {
        login($("#agentID").val(),$("#username").val(),$("#password").val());
    });
});

function login(agentID,username, password){ 
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Agent/login/login/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Agent/login/login/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            agentID     : agentID,
            username    : username,
            password    : password,
        },
        success: function(response) {
            if (response.code == '200') {
                localStorage.setItem(btoa('agent_agentID'), btoa(response.data.agentID));
                localStorage.setItem(btoa('agent_staffID'), btoa(response.data.staffID));
                localStorage.setItem(btoa('agent_agentName'), btoa(response.data.agentName));
                localStorage.setItem(btoa('agent_staffName'), btoa(response.data.staffName));
                localStorage.setItem(btoa('agent_password'), btoa(response.data.password));
                localStorage.setItem(btoa('agent_token'), btoa(response.data.token));
                localStorage.setItem(btoa('agent_expired'), btoa(response.data.expired));
                window.location.href = '../pages/dashboard';
            }else{
                alert("Your data is not verified");
            }
        },
    });
}