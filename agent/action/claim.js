localStorage.removeItem(btoa('agent_claimedID'));

$(document).ready(function() {
    master_class();
    $('#pnr').focus();

    choose_triptype();
    $('input[type=radio][name=tripType]').on('change', function () {
        choose_triptype();
    });

    $('#tripFrom_3').on('change keyup paste', function (e) {
        this.value.replace(/\s+/g, '');
        var input = $.trim(this.value);
        if ($('input[name="tripType"]:checked').val() == 0) {
            if ($('#tripFrom_3').val() == 'sin') {
                console.log('sin')
                $('#tripFrom_3').val('NON');
                check_tripform(input);
                check_tripto(input);    
            } else {
                $('#tripTo_1').val(input);
                check_tripform(input);
                check_tripto(input);
            }
        }else{
            if ($('#tripFrom_3').val() == 'sin') {
                $('#tripFrom_3').val('NON');
                check_tripform(input);  
            } else {
                check_tripform(input);
            }
        }
    });

    $('#tripTo_1').on('change keyup paste', function (e) {
        this.value.replace(/\s+/g, '');
        var input = $.trim(this.value);
        if ($('input[name="tripType"]:checked').val() == 0) {
            if ($('#tripTo_1').val() == 'sin') {
                $('#tripTo_1, #tripFrom_3').val('NON');
                check_tripform(input);
                check_tripto(input);    
            } else {
                $('#tripFrom_3').val(input);
                check_tripform(input);
                check_tripto(input);
            }
        }else{
            if ($('#tripTo_1').val() == 'sin') {
                $('#tripTo_1').val('NON');
                check_tripto(input);    
            } else {
                check_tripto(input);
            }
        }
    });
     
    looping_ticket($('#noofticket').val());
    $('#noofticket').on('change', function () {
        looping_ticket(this.value);
    });

    $('#btn_save').click( function() {
        $('#btn_save').attr('disabled','disabled');
        save();
    });

    $('#btn_close').click( function() {
        $('#btn_save').removeAttr("disabled");
    });
});

function master_class(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/class_airline/get_data/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/admin/class_airline/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
	        $('#classDep').empty();
	        $.each(response.data, function (i, item) {
	            $('#classDep').append($('<option>', { 
	                value: item.id,
	                text : item.className 
	            }));
	        });

	        $('#classArr').empty();
	        $.each(response.data, function (i, item) {
	            $('#classArr').append($('<option>', { 
	                value: item.id,
	                text : item.className 
	            }));
	        });
        },
    });
}

function testPnr(val){
    var value   = $.trim(val);
    var sukses  = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if (value.length < 6) {
        $('#pnrStatus').html(error);
        $('#isPnr').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: 'http://sqtfsurabaya.com/api/Agent/claim/check_pnr/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/agent/claim/check_pnr/_',
            dataType: 'json',
            cache: false,
            data: {
                'pnr' : value
            },
            success: function (responses) {
                if(responses.code == 200 && responses.total == 0){
                    $('#pnrStatus').html(sukses);
                    $('#isPnr').val(1);
                }else if(responses.total == 2){
                    $('#modal_add_ticket').modal('show');
                    $('#btn_add_ticket').click( function() {
                        localStorage.setItem(btoa('agent_claimedID'), btoa(responses.data.id));
                        window.location.href = "claimed_detail";
                    });
                    $('#pnrStatus').html(error);
                    $('#isPnr').val(0);
                }
            }
        });
    }
}

function choose_triptype(){
    $('#tripFrom_3, #tripTo_1').val('');
    setTimeout(function(){
        var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
        
        $('#tripFromInfo').css('display', 'none');
        $('#tripToInfo').css('display', 'none');
        $('#tripFromStatus').html(error);
        $('#tripToStatus').html(error);
    }, 100);
}

function check_tripform(input){
    var sukses  = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if(input.length < 3){
        $('#tripFromStatus').html(error);
        $('#tripFromInfo').css('display','none');
        $('#isFrom').val(0);
    }else if(input.length == 3){
        $.ajax({
            type: 'POST',
            url: 'http://sqtfsurabaya.com/api/Agent/claim/check_route/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/agent/claim/check_route/_',
            dataType: 'json',
            cache: false,
            data: {
                'airportCode' : input
            },
            success: function (responses) {
                if(responses.code == 200 && responses.total > 0){
                    $('#tripFromStatus').html(sukses);
                    $('#tripFromInfo').css('display','block');
                    $('#tripFromInfo').html('<b>'+responses.data.airportCode+'</b> ( '+responses.data.airportName+' - '+responses.data.airportCountry+' )');
                    $('#isFrom').val(1);
                }else{
                    $('#tripFromStatus').html(error);
                    $('#tripFromInfo').css('display','none');
                    $('#isFrom').val(0);
                }
            }
        })
    }
}

function check_tripto(input){
    var sukses  = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if(input.length < 3){
        $('#tripToStatus').html(error);
        $('#tripToInfo').css('display','none');
        $('#isTo').val(0);
    }else if(input.length == 3){
        $.ajax({
            type: 'POST',
            url: 'http://sqtfsurabaya.com/api/Agent/claim/check_route/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/agent/claim/check_route/_',
            dataType: 'json',
            cache: false,
            data: {
                'airportCode' : input
            },
            success: function (responses) {
                if(responses.code == 200 && responses.total > 0){
                    $('#tripToStatus').html(sukses);
                    $('#tripToInfo').css('display','block');
                    $('#tripToInfo').html('<b>'+responses.data.airportCode+'</b> ( '+responses.data.airportName+' - '+responses.data.airportCountry+' )');
                    $('#isTo').val(1);
                }else{
                    $('#tripToStatus').html(error);
                    $('#tripToInfo').css('display','none');
                    $('#isTo').val(0);
                }
            }
        })
    }
}

function looping_ticket(dt){
	$('#tb_ticket > tbody').empty();
    var txt = ``;
    for (var i = 1; i <= dt; i++) {
        txt += `
        <tr>
        	<td width="50%" style="padding:0px">
        		<div class="input-group">
				  <div class="input-group-prepend">
				    <span class="input-group-text" id="basic-addon1" style="border:none">618</span>
				  </div>
				  <input type="text" class="form-control" name="ticketNo[]" id="ticketNo`+i+`" onKeyUp="this.value = this.value.replace(/[^0-9\.]/g,'');testTicket(this.value, `+ i +`,`+dt+`)" onblur="testTicket(this.value, `+ i +`)" maxlength="10" style="border:none;border-right: 1px solid rgb(226, 229, 237);">
				  <input type="hidden" class="form-control" name="isTicket[]" id="isTicket`+ i +`" value="0">
				  <div class="input-group-append" id="ticketStatus`+i+`"></div>
				</div>
			</td>
        	<td width="50%" style="padding:0px">
        		<div class="input-group">
				  <input type="text" class="form-control" name="kfNo[]" id="kfNo`+i+`" onKeyUp="this.value = this.value.replace(/[^0-9\.]/g,'');testKf(this.value, `+ i +`,`+dt+`)" onblur="testKf(this.value, `+ i +`)" maxlength="10" style="border:none;border-right: 1px solid rgb(226, 229, 237);">
				  <input type="hidden" class="form-control" name="isKf[]" id="isKf`+ i +`" value="0">
				  <div class="input-group-append" id="kfStatus`+i+`"></div>
				</div>
			</td>
        </tr>
        `;
    }
    $('#tb_ticket > tbody').append(txt);
}

function testTicket(val, i, dt){
    var value 	= $.trim(val);
    var sukses 	= `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error 	= `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if (value.length < 10) {
        $('#ticketStatus' + i).html(error);
        $('#isTicket' + i).val(0);
    }else {
        $.ajax({
            type: 'POST',
            url: 'http://sqtfsurabaya.com/api/Agent/claim/check_ticket/_',
			// url: 'http://localhost/travelfair/sqtf_0220/api/agent/claim/check_ticket/_',
            dataType: 'json',
            cache: false,
            data: {
                'ticketNo' : value
            },
            success: function (responses) {
                if(responses.code == 200 && responses.total == 0){
                    if (dt == '1') {
                        $('#ticketStatus' + i).html(sukses);
                        $('#isTicket' + i).val(1); 
                    } else {
                        var exist = 0;
                        for (var j = 1; j <= dt; j++) {
                            if (i!=j) {
                                if($('#ticketNo'+i).val() == $('#ticketNo'+j).val()){
                                    exist += 1; 
                                }

                                if (exist > 0) {
                                    $('#ticketStatus' + i).html(error);
                                    $('#isTicket' + i).val(0);        
                                } else {
                                    $('#ticketStatus' + i).html(sukses);
                                    $('#isTicket' + i).val(1); 
                                }
                            }
                        }
                    }
                }else{
                    $('#ticketStatus' + i).html(error);
                    $('#isTicket' + i).val(0);
                }
            }
        });
    }
}

function testKf(val, i, dt){
    var value 	= $.trim(val);
    var sukses 	= `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error 	= `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if (value.length < 10) {
        $('#kfStatus' + i).html(error);
        $('#isKf' + i).val(0);
    }else {
        if (dt == '1') {
            $('#kfStatus' + i).html(sukses);
            $('#isKf' + i).val(1); 
        } else {
            var exist = 0;
            for (var j = 1; j <= dt; j++) {
                if (i!=j) {
                    if($('#kfNo'+i).val() == $('#kfNo'+j).val()){
                        exist += 1; 
                    }

                    if (exist > 0) {
                        $('#kfStatus' + i).html(error);
                        $('#isKf' + i).val(0);        
                    } else {
                        $('#kfStatus' + i).html(sukses);
                        $('#isKf' + i).val(1); 
                    }
                }
            }
        }
    }
}

function save(){
    $.ajax({
        type: 'POST',
        // url: 'http://sqtfsurabaya.com/api/Agent/claim/save/_',
        url: 'http://localhost/travelfair/sqtf_0220/api/agent/claim/save/_',
        dataType: 'json',
        cache: false,
        data: $('#form_claim').serialize()+"&agentID="+agentID+"&staffID="+staffID,
        success: function (responses) {
            if(responses.code==200){
                $('#tb_result').append(`
                    <tr>
                      <td>
                        <small><b>PNR</b></small><br>
                        `+responses.data[0].pnr+`
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <small><b>Trip Type</b></small><br>
                        `+responses.data[0].trip+`
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <small><b>Departure Date</b></small><br>
                        `+ dateFormat(responses.data[0].depDate)+`
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <small><b>Arrival Date</b></small><br>
                        `+ dateFormat(responses.data[0].arrDate)+`
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <small><b>Trip From</b></small><br>
                        `+responses.data[0].dep+` ( `+responses.data[0].depClass+` )
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <small><b>Trip To</b></small><br>
                       `+responses.data[0].arr+` ( `+responses.data[0].arrClass+` )
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <small><b>Total Ticket</b></small><br>
                        `+responses.data[0].noofticket+`
                      </td>
                    </tr>
                `);

                $.each(responses.data[0].ticket, function (i, item) {
                    var no = i+1;

                    $('#tb_result_ticket').append(`
                        <tr class="trTicket">
                            <td>`+no+`</td>
                            <td>`+item.ticketNo+`</td>
                            <td>`+item.kfNo+`</td>
                        </tr>
                    `);
                });

                $('#alert_result').css('display','none');
                $('#div_result').css('display','block');
                $('#modal_result').modal({backdrop: 'static', keyboard: false});
                $('#btn_claimed').css('display','inline-block');

                $('#btn_claimed').click( function() {
                    localStorage.setItem(btoa('agent_claimedID'), btoa(responses.data[0]._));
                    window.location.href = "claimed_detail";
                });

                $('#btn_close').click( function() {
                    window.location.href = "claim";
                });
            }else{
                $('#alert_result').empty();
                var txt = `<ul style="margin:0">`;
                $.each(responses.info, function (i, item) {
                    txt += `<li>`+ item +`</li>`;
                });
                $('#alert_result').html(txt);
                $('#alert_result').css('display','block');
                $('#div_result').css('display','none');
                $('#modal_result').modal({backdrop: 'static', keyboard: false});

                $('#btn_close').click( function() {
                    $('#modal_result').modal('hide');
                });
            }
        }
    });
}