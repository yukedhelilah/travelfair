$(document).ready(function() {
	dashboard();
	get_data();
});

function dashboard(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Agent/dashboard/dashboard/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/agent/dashboard/dashboard/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            agentID		: agentID,
        },
        success: function(response) {
        	$('#allClaimed').html(response.data.allClaimed);
        	$('#todayClaimed').html(response.data.todayClaimed);
        	$('#onQueue').html(response.data.onQueue);
        	$('#unclaimed').html(response.data.unclaimed);
        },
    });
}

function get_data(){
	$('#tb_claimed > tbody').empty();
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Agent/dashboard/get_data/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/agent/dashboard/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            agentID		: agentID
        },
        success: function(response) {
        	if (response.data == null) {
        		$('#tb_claimed > tbody').append(`
        			<tr><td colspan="11" style="text-align:center">No data available</td></tr>
    			`);
        	} else {
            	$.each(response.data, function( i, value ) {
            		var no = i+1;
                    var status = "";

                    if (value.isStatus == 0) {
                        status = `<span class="badge badge-warning pd-x-10 pd-y-5" style="color:white">OPEN</span>`
                    } else if(value.isStatus == 1){
                        status = `<span class="badge badge-info pd-x-10 pd-y-5" style="color:white">PARTLY</span>`
                    } else {
                        status = `<span class="badge badge-success pd-x-10 pd-y-5" style="color:white">COMPLETE</span>`
                    }

	           	 	$('#tb_claimed > tbody').append(`
	        			<tr>
	        				<td><div style="text-align:center">` + no + `</div></td>
	        				<td>`+value.pnr+`</td>
	        				<td>`+dateFormat(value.issuanceDate)+`</td>
	        				<td>`+value.trip+`</td>
	        				<td>`+value.tripFrom_1+` `+value.tripFrom_2+` `+value.tripFrom_3+` (`+value.depClass+`)</td>
	        				<td>`+value.tripTo_1+` `+value.tripTo_2+` `+value.tripTo_3+` (`+value.arrClass+`)</td>
	        				<td>`+dateFormat(value.depDate)+`</td>
	        				<td><div style="text-align:center">` + value.noofticket + `</div></td>
	        				<td><div style="text-align:center">`+status+`</div></td>
	        				<td>`+value.staffName+`</td>
	        				<td>
		        				<div style="text-align:center">
			                    	<div data-toggle="tooltip" title="Details" onclick="claimed_detail('`+value.id+`')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
			                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
			                		</div>
			            		</div>
		            		</td>
	        			</tr>
	        		`);
            	});
        	}
        },
    });
}

function claimed_detail(id){
    localStorage.setItem(btoa('agent_claimedID'), btoa(id));
    window.location.href = "claimed_detail";
}