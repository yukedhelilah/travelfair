<?php include "header.php"; ?>

  <div class="content content-fixed">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="margin: 0px;max-width: 100%">
        <div class="alert alert-info" role="alert" id="alert_superid">Create new staff to input data</div>
      <div class="d-sm-flex align-items-center justify-content-between mg-b-15">
        <div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-style1 mg-b-0">
              <li class="breadcrumb-item" style="font-size: 15px"><a href="dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page" style="font-size: 15px">Staff</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="card">
        <div class="card-header" style="padding: 7px 7px 7px 12px;">
          <span style="font-size: 17px;padding-top: 3px;position: absolute;">Staff</span>
          <a onclick="add_data()" class="btn btn-xs btn-primary" style="float: right;color: white"><i class="fa fa-plus"></i> &nbspNew</a>
        </div>
        <div class="card-body" style="padding: 7px 7px 7px 10px;">
          <table id="tb_agent" class="table">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Username</th>
                <th>Last Login</th>
                <th>Act</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content tx-14">
        <div class="modal-header pd-x-15 pd-y-10">
          <h6 class="modal-title" id="exampleModalLabel5"><span id="modal_tittle" style="padding-top: 2.5px;position: absolute;"></span></h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body pd-x-15 pd-y-15">
          <form id="form_staff">
            <input id="act" name="act" type="hidden" class="form-control" value="add">
            <input id="id" name="id" type="hidden" class="form-control" value="">
            <div class="form-group">
              <label for="formGroupExampleInput" class="d-block mg-b-0">Name</label>
              <input id="staffName" name="staffName" type="text" class="form-control" placeholder="Enter name">
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput2" class="d-block mg-b-0">Username</label>
              <input id="staffUsername" name="staffUsername" type="text" class="form-control" placeholder="Enter username">
            </div>
            <!-- <div id="add_password" class="form-group" style="">
              <label for="formGroupExampleInput2" class="d-block mg-b-0">Password</label>
              <input id="staffPassword" name="staffPassword" type="text" class="form-control" readonly="readonly" placeholder="1234">
            </div> -->

            <span id="default_password" style="display: none;">
              <div class="form-group">
                <label for="formGroupExampleInput" class="d-block mg-b-0">Password</label>
                <span class="tx-15 tx-bold">
                  1234
                </span>
              </div>
            </span>
            <div id="edit_password" class="form-group mg-b-0">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="resetPassword" name="resetPassword">
                <label class="custom-control-label" for="resetPassword">Reset Password</label>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer pd-x-10 pd-y-10">
          <a onclick="save()" class="btn btn-primary tx-13" style="color: white">Save</a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content tx-14">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLabel5">Do you want to delete this data?</h6>
        </div>
        <div class="modal-footer pd-x-10 pd-y-10">
          <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Cancel</button>
          <a id="delete_data" class="btn btn-primary tx-13" style="color: white">Yes</a>
        </div>
      </div>
    </div>
  </div>

<?php include "footer.php"; ?>
<script src="../action/staff.js"></script>

<style type="text/css">
    #staffName{
      text-transform: uppercase;
    }
    ::-webkit-input-placeholder { /* WebKit browsers */
      text-transform: none;
    }
    :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
      text-transform: none;
    }
    ::-moz-placeholder { /* Mozilla Firefox 19+ */
      text-transform: none;
    }
    :-ms-input-placeholder { /* Internet Explorer 10+ */
      text-transform: none;
    }
    ::placeholder { /* Recent browsers */
      text-transform: none;
    }
</style>