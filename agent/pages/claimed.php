<?php include "header.php"; ?>

  <div class="content content-fixed">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="margin: 0px;max-width: 100%">
      <div class="alert alert-info" role="alert" id="alert_superid">Create new staff to input data</div>
      <div class="d-sm-flex align-items-center justify-content-between mg-b-15">
        <div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-style1 mg-b-0">
              <li class="breadcrumb-item" style="font-size: 15px"><a href="dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page" style="font-size: 15px">Claim List</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row row-xs">
        <div class="col-sm-12 mg-t-10 mg-sm-t-0">
          <div class="card card-body">
            <table id="tb_claimed" class="table table-bordered" style="width: 100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>PNR</th>
                  <th>Issuance</th>
                  <th>Trip</th>
                  <th>Trip From</th>
                  <th>Trip To</th>
                  <th>Departure</th>
                  <th>Ticket</th>
                  <th>Status</th>
                  <th>Staff</th>
                  <th>Act</th>
                </tr>
              </thead>
            </table>
          </div>
        </div><!-- col -->
      </div><!-- row -->
    </div>
  </div>

<?php include "footer.php"; ?>
<script src="../action/claimed.js"></script>