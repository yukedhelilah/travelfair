<?php include "header.php"; ?>

  <div class="content content-fixed">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="margin: 0px;max-width: 100%">
      <div class="d-sm-flex align-items-center justify-content-between mg-b-15">
        <div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-style1 mg-b-0">
              <li class="breadcrumb-item" style="font-size: 15px"><a href="dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page" style="font-size: 15px">New Claim Data</li>
            </ol>
          </nav>
        </div>
      </div>
      <form id="form_claim">
        <div class="row row-xs">
          <div class="col-sm-12 col-md-6">
            <div class="card card-body">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Input Date</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="issuanceDate" name="issuanceDate" value="<?php echo date('Y-m-d');?>" disabled>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">PNR</label>
                <div class="col-sm-9 input-group">
                  <input type="text" class="form-control" name="pnr" id="pnr" maxlength="6" onKeyUp="testPnr(this.value)" onblur="testPnr(this.value)">
                  <input type="hidden" name="isPnr" id="isPnr" value="0">
                  <div class="input-group-append" id="pnrStatus">
                    <span class="input-group-text" id="basic-addon2" style="width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Departure Date</label>
                <div class="col-sm-3">
                  <select class="custom-select" name="depdate_dd">
                  <?php
                      for ($i=1; $i <= 31; $i++) { 
                          $j = $i<10 ? '0'.$i : $i;
                          $selected = $j==date('d') ? 'selected' : '';
                          echo '<option value="'.$j.'" '.$selected.'>'.$j.'</option>';
                      }
                  ?>                           
                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="custom-select" name="depdate_mm">
                  <?php
                      for ($i = 1; $i <= 12; $i++) {
                          $j = $i<10 ? '0'.$i : $i;
                          $date_str = date("F", strtotime("2019-" . $j . "-01"));
                          $selected = $j==date('m') ? 'selected' : '';
                          echo '<option value="'.$j.'" '.$selected.'>'.$date_str .'</option>';
                      }
                  ?>                           
                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="custom-select" name="depdate_yy">
                  <?php
                      for ($i=date('Y'); $i <= (date('Y')+1); $i++) { 
                          echo '<option value="'.$i.'">'.$i.'</option>';
                      }
                  ?>                           
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">Arrival Date</label>
                <div class="col-sm-3">
                  <select class="custom-select" name="arrdate_dd">
                  <?php
                      for ($i=1; $i <= 31; $i++) { 
                          $j = $i<10 ? '0'.$i : $i;
                          $selected = $j==date('d') ? 'selected' : '';
                          echo '<option value="'.$j.'" '.$selected.'>'.$j.'</option>';
                      }
                  ?>                           
                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="custom-select" name="arrdate_mm">
                  <?php
                      for ($i = 1; $i <= 12; $i++) {
                          $j = $i<10 ? '0'.$i : $i;
                          $date_str = date("F", strtotime("2019-" . $j . "-01"));
                          $selected = $j==date('m') ? 'selected' : '';
                          echo '<option value="'.$j.'" '.$selected.'>'.$date_str .'</option>';
                      }
                  ?>                           
                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="custom-select" name="arrdate_yy">
                  <?php
                      for ($i=date('Y'); $i <= (date('Y')+1); $i++) { 
                          echo '<option value="'.$i.'">'.$i.'</option>';
                      }
                  ?>                           
                  </select>
                </div>
              </div>
              <div class="form-group row mg-b-15">
                <label class="col-form-label col-sm-3 pt-0">Trip Type</label>
                <div class="col-sm-9">
                  <div class="custom-control custom-radio mg-r-20" style="float: left;">
                    <input type="radio" id="tripTypeA" name="tripType" value="0" class="custom-control-input" checked>
                    <label class="custom-control-label" for="tripTypeA">Return</label>
                  </div>

                  <div class="custom-control custom-radio" style="float: left;">
                    <input type="radio" id="tripTypeB" name="tripType" value="1" class="custom-control-input">
                    <label class="custom-control-label" for="tripTypeB">Open Jaw</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-9 offset-sm-3 mg-b-10">
                  <small style="color: #0168fa;font-weight: bold;">Please write "NON" for SUB-SIN return only.</small>
                </div>
                <label for="inputEmail3" class="col-sm-3 col-form-label">Destination From</label>
                <div class="col-sm-9" style="padding: 0 !important">
                  <div class="col-sm-2" style="float: left;">
                    <input type="text" class="form-control" name="tripFrom_1" id="tripFrom_1" maxlength="3" value="SUB" readonly>
                  </div>
                  <div class="col-sm-2" style="float: left;">
                    <input type="text" class="form-control" name="tripFrom_2" id="tripFrom_2" maxlength="3" value="SIN" readonly>
                  </div>
                  <div class="col-sm-3 input-group" style="float: left;">
                    <input type="text" class="form-control" name="tripFrom_3" id="tripFrom_3" maxlength="3">
                    <input type="hidden" name="isFrom" id="isFrom" value="0">
                    <div class="input-group-append" id="tripFromStatus">
                      <span class="input-group-text" id="basic-addon2" style="width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>
                    </div>
                  </div>
                  <div class="col-sm-5" style="float: left;">
                    <select class="custom-select" name="classDep" id="classDep"></select>
                  </div>
                </div>
                <div class="col-sm-9 offset-sm-3 mg-t-10">
                  <span id="tripFromInfo"></span>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 col-form-label">Destination To</label>
                <div class="col-sm-9" style="padding: 0 !important">
                  <div class="col-sm-3 input-group" style="float: left;">
                    <input type="text" class="form-control" name="tripTo_1" id="tripTo_1" maxlength="3">
                    <input type="hidden" name="isTo" id="isTo" value="0">
                    <div class="input-group-append" id="tripToStatus">
                      <span class="input-group-text" id="basic-addon2" style="width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>
                    </div>
                  </div>
                  <div class="col-sm-2" style="float: left;">
                    <input type="text" class="form-control" name="tripTo_2" id="tripTo_2" maxlength="3" value="SIN" readonly>
                  </div>
                  <div class="col-sm-2" style="float: left;">
                    <input type="text" class="form-control" name="tripTo_3" id="tripTo_3" maxlength="3" value="SUB" readonly>
                  </div>
                  <div class="col-sm-5" style="float: left;">
                    <select class="custom-select" name="classArr" id="classArr"></select>
                  </div>
                </div>
                <div class="col-sm-9 offset-sm-3 mg-t-10">
                  <span id="tripToInfo"></span>
                </div>
              </div>
            </div>
          </div><!-- col -->
          <div class="col-sm-12 col-md-6 mg-t-10 mg-sm-t-0">
            <div class="card card-body">
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-3 col-form-label">No Of Ticket</label>
                <div class="col-sm-9">
                  <select class="custom-select" id="noofticket" name="noofticket">
                  <?php
                      for ($i=1; $i <= 9; $i++) { 
                          echo '<option value="'.$i.'">'.$i.'</option>';
                      }
                  ?>                           
                  </select>
                </div>
              </div>
              <table class="table table-bordered" id="tb_ticket">
                <thead>
                  <th>Ticket No</th>
                  <th>Krisflyer No</th>
                </thead>
                <tbody></tbody>
              </table>
              <div class="col-sm-3 offset-sm-9 pd-0">
                <button id="btn_save" type="button" class="btn btn-primary" style="width: 100%;">Submit</button>
              </div>
            </div>
          </div><!-- col -->
        </div><!-- row -->
      </form>
    </div>
  </div>

  <div class="modal fade" id="modal_result" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content tx-14">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLabel4">Summary</h6>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger mg-0" role="alert" id="alert_result" style="display: none">EROR</div>
          <div class="row row-xs" id="div_result" style="display: none">
            <div class="col-sm-6" style="float: left;">
              <table class="table table-bordered" id="tb_result">
              </table>
            </div>
            <div class="col-sm-6" style="float: left;">
              <table class="table table-bordered mg-b-0" id="tb_result_ticket">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Ticket No</th>
                    <th>Krisflyer No</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button id="btn_claimed" class="btn btn-primary tx-13" style="display: none">Details</button>
          <button id="btn_close" class="btn btn-secondary tx-13">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_add_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content tx-14">
        <div class="modal-body pd-x-15 pd-y-15" style="text-align: center;">
          <p class="mg-0">This PNR already exist, do you want to add the ticket for this PNR?</p>
        </div>
        <div class="modal-footer pd-x-10 pd-y-10">
          <a id="btn_add_ticket" class="btn btn-primary tx-13" style="color: white">Yes</a>
          <a class="btn btn-secondary tx-13" data-dismiss="modal" style="color: white">No</a>
        </div>
      </div>
    </div>
  </div>

<?php include "footer.php"; ?>
<script src="../action/claim.js"></script>

<style type="text/css">
    #pnr, #tripFrom_3, #tripTo_1{
      text-transform: uppercase;
    }
    ::-webkit-input-placeholder { /* WebKit browsers */
      text-transform: none;
    }
    :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
      text-transform: none;
    }
    ::-moz-placeholder { /* Mozilla Firefox 19+ */
      text-transform: none;
    }
    :-ms-input-placeholder { /* Internet Explorer 10+ */
      text-transform: none;
    }
    ::placeholder { /* Recent browsers */
      text-transform: none;
    }

    .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
        padding-right: 10px !important;
        padding-left: 10px !important;
    }
</style>