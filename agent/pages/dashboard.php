<?php include "header.php"; ?>

    <div class="content content-fixed pd-b-20">
      <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="margin: 0px;max-width: 100%">
        <div class="alert alert-info" role="alert" id="alert_superid">Create new staff to input data</div>
        <div class="d-sm-flex align-items-center justify-content-between mg-b-15">
          <div>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb breadcrumb-style1 mg-b-0">
                <li class="breadcrumb-item" style="font-size: 15px"><a href="dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page" style="font-size: 15px">Claimed Monitoring</li>
              </ol>
            </nav>
          </div>
        </div>
        <div class="row row-xs">
          <div class="col-sm-6 col-lg-3">
            <div class="card card-body">
              <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">All Claimed</h6>
              <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-primary tx-rubik mg-b-0 mg-r-5 lh-1" id="allClaimed"></h3>
              </div>
            </div>
          </div><!-- col -->
          <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
            <div class="card card-body">
              <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Claimed Today</h6>
              <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-success tx-rubik mg-b-0 mg-r-5 lh-1" id="todayClaimed"></h3>
              </div>
            </div>
          </div><!-- col -->
          <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
            <div class="card card-body">
              <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Queue</h6>
              <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-info tx-rubik mg-b-0 mg-r-5 lh-1" id="onQueue"></h3>
              </div>
            </div>
          </div><!-- col -->
          <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
            <div class="card card-body">
              <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Unclaimed</h6>
              <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-warning tx-rubik mg-b-0 mg-r-5 lh-1" id="unclaimed"></h3>
              </div>
            </div>
          </div><!-- col -->
        </div><!-- row -->
        <div class="row row-xs mg-t-20">
          <div class="col-sm-12 col-lg-12">
          <div class="card">
            <div class="card-header" style="padding: 7px 7px 7px 12px;">
              <span>Recent Claimed</span>
            </div>
            <table id="tb_claimed" class="table table-bordered mg-0" style="width: 100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>PNR</th>
                  <th>Issuance</th>
                  <th>Trip</th>
                  <th>Trip From</th>
                  <th>Trip To</th>
                  <th>Departure</th>
                  <th>Ticket</th>
                  <th>Status</th>
                  <th>Staff</th>
                  <th>Act</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          </div><!-- col -->
        </div><!-- row -->
      </div>
    </div>

<?php include "footer.php"; ?>
<script src="../action/dashboard.js"></script>