<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="SQTF">
    <meta name="twitter:description" content="Travelfair SQTF Surabaya 13-16 February 2020">
    <meta name="twitter:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/dashforge">
    <meta property="og:title" content="SQTF">
    <meta property="og:description" content="Travelfair SQTF Surabaya 13-16 February 2020">

    <meta property="og:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Travelfair SQTF Surabaya 13-16 February 2020">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png">

    <title>AGENT | SQTF SURABAYA</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="../lib/jqvmap/jqvmap.min.css" rel="stylesheet">
    <link href="../lib/prismjs/themes/prism-vs.css" rel="stylesheet">
    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="../assets/css/dashforge.css">
    <link rel="stylesheet" href="../assets/css/dashforge.dashboard.css">
    <link rel="stylesheet" href="../assets/css/additional_css.css">
  </head>
  <body class="page-profile">

    <div class="modal fade" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content tx-14">
          <div class="modal-header pd-x-15 pd-y-10">
            <h6 class="modal-title" id="exampleModalLabel5">Change Password</h6>
          </div>
          <div class="modal-body pd-x-15 pd-y-15">
            <form>
              <input id="staffID" name="staffID" type="hidden" class="form-control" value="">
              <div id="add_password" class="form-group mg-b-0">
                <label for="formGroupExampleInput2" class="d-block mg-b-0">Password</label>
                <input id="password" name="password" type="text" class="form-control" maxlength="4" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');">
              </div>
            </form>
          </div>
          <div class="modal-footer pd-x-10 pd-y-10">
            <a onclick="changePassword()" class="btn btn-primary tx-13" style="color: white">Save</a>
          </div>
        </div>
      </div>
    </div>

    <header class="navbar navbar-header navbar-header-fixed">
      <a href="" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
      <div class="navbar-brand" style="padding-top: 10px !important;width: fit-content">
        <a href="../index.html" class="df-logo pd-r-20" style="height: 110%;border-right: 1px solid #e2e5ed;">SQTF&nbsp<span> | Agent</span></a>
      </div><!-- navbar-brand -->
      <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header" style="padding-top: 10px !important">
          <a href="../index.html" class="df-logo">SQTF<span> | AGENT</span></a>
          <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
        <ul class="nav navbar-menu">
          <li class="nav-item"><a href="dashboard" class="nav-link"><i data-feather="box"></i> DASHBOARD</a></li>
          <li class="nav-item"><a href="claimed" class="nav-link"><i data-feather="archive"></i> CLAIM LIST</a></li>
          <li class="nav-item" id="nav_new_claim"><a href="claim" class="nav-link"><i data-feather="archive"></i> NEW CLAIM</a></li>
          <li class="nav-item" id="nav_staff"><a href="staff" class="nav-link"><i data-feather="archive"></i> STAFF</a></li>
          <li class="nav-item" style="cursor: pointer;"><a onclick="logout()" class="nav-link"><i data-feather="archive"></i> LOGOUT</a></li>
        </ul>
      </div><!-- navbar-menu-wrapper -->
      <div class="navbar-right" style="width: fit-content !important">
        <div><span id="loginName"></span></div>
      </div><!-- navbar-right -->
    </header><!-- navbar -->
