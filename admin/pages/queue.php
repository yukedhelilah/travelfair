<?php include "header.php"; ?>

  <div class="content content-fixed pd-15">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="margin: 0px;max-width: 100%">
      <div class="row row-xs">
        <div class="col-sm-12 mg-t-10 mg-sm-t-0">
          <div class="card card-body pd-10">
            <table id="tb_queue" class="table table-bordered mg-0" style="width: 100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Date</th>
                  <th>Queue Code</th>
                  <th>PNR | Ticket</th>
                  <th>Staff</th>
                  <th>Guest</th>
                  <th>Status</th>
                  <th>Act</th>
                </tr>
              </thead>
            </table>
          </div>
        </div><!-- col -->
      </div><!-- row -->
    </div>
  </div>

<?php include "footer.php"; ?>
<script src="../action/queue.js"></script>