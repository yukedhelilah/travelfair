<?php include "header.php"; ?>

<div class="content content-fixed">
  <div class="container-fluid pd-x-0 pd-lg-x-10 pd-xl-x-0">
    <div class="d-sm-flex align-items-center justify-content-between mg-b-0">
      <div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item" style="font-size: 15px"><a href="dashboard">Dashboard</a></li>
            <li class="breadcrumb-item" style="font-size: 15px"><a href="queue">Queue List</a></li>
            <li class="breadcrumb-item active" aria-current="page" style="font-size: 15px">Queue Detail</li>
          </ol>
        </nav>
      </div>
      <div class="d-none d-md-block">
        <a href="queue" class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i> Back</a>
        <a href="javascript:;" onclick="save()" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="save" class="wd-10 mg-r-5"></i> Save</a>
      </div>
    </div>

    <div class="row row-xs">
      <div class="col-lg-12 col-xl-12">
        <div class="row row-sm">
          <div class="col-sm-12">
            <div class="divider-text">Tickets</div>
            <form id="newForm">
              <input type="hidden" name="no_pnr" id="no_pnr">
              <input type="hidden" name="no_ticket" id="no_ticket">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th rowspan="2" class="tx-12 tx-bold tx-uppercase" width="10%">TICKET NO (618)</th>
                      <th rowspan="2" class="tx-12 tx-bold tx-uppercase" width="10%">KRISSFLYER</th>
                      <th colspan="8" class="tx-12 tx-bold tx-uppercase">GIFT</th>
                    </tr>
                    <tr>
                      <th width="10%"><span class="tx-12 tx-bold tx-uppercase">Flazz Card</span><!-- <br><span class="tx-small tx-10">*) Every 2 ticket PEY and above</span> --></th>
                      <th width="10%"><span class="tx-12 tx-bold tx-uppercase">Foldable Bags</span><!-- <br><span class="tx-small tx-10">*) Every ticket PEY and above</span> --></th>
                      <th width="10%"><span class="tx-12 tx-bold tx-uppercase">Shopping Bags</span><!-- <br><span class="tx-small tx-10">*) Every ticket</span> --></th>
                      <th width="10%"><span class="tx-12 tx-bold tx-uppercase">CDV</span><!-- <br><span class="tx-small tx-10">*) 1X (SEA, NA, SWP, WAA)<br>**) 2X (EUR, AME)</span> --></th>
                      <th width="10%"><span class="tx-12 tx-bold tx-uppercase">CDV EXTRA</span><!-- <br><span class="tx-small tx-10">*) Travel in Feb-Mar 2020</span> --></th>
                      <th width="10%"><span class="tx-12 tx-bold tx-uppercase">Ez-link Card</span><!-- <br><span class="tx-small tx-10">*) GV4 or 3 night stay</span> --></th>
                      <th width="10%"><span class="tx-12 tx-bold tx-uppercase">MAP PER</span><!-- <br><span class="tx-small tx-10">*) Every PER</span> --></th>
                      <th width="10%"><span class="tx-12 tx-bold tx-uppercase">MAP EUR & US</span><!-- <br><span class="tx-small tx-10">*) Every EUR, AME</span> --></th>
                    </tr>
                  </thead>
                  <tbody id="tb_summary"></tbody>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include "footer.php"; ?>
<script src="../action/queue_detail.js"></script>