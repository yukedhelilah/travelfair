<?php include "header.php"; ?>

  <div class="content content-fixed pd-15">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="margin: 0px;max-width: 100%">
      <div class="row row-xs" id="alert_dataclaim">
        <div class="col-sm-12 col-md-8 mg-t-10 mg-sm-t-0">
          <div class="card card-body">
            <table class="table mg-b-0" id="tb_detail">
              <tbody></tbody>
            </table>
          </div>
        </div><!-- col -->
        <div class="col-sm-12 col-md-4 mg-t-10 mg-sm-t-0">
          <div class="card card-body">
            <table class="table mg-b-0" id="tb_update">
              <tbody></tbody>
            </table>
          </div>
        </div><!-- col -->
        <div class="col-sm-12 mg-t-10 mg-sm-t-10">
          <div class="card">
            <div class="card-header" style="padding: 7px 7px 7px 12px;">
              <span>Tickets</span>
            </div>
            <!-- <div class="card-body"> -->
              <table class="table table-bordered mg-b-0" id="tb_ticket" style="width: 100%">
                <thead>
                  <th style="width: 1%;font-size: 10px;text-align: center;">No</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">Ticket / KF</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">Flazz</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">Foldable</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">Shopping</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">CDV</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">CDV Extra</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">Ez-Link</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">MAP-PER</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">MAP-EUR/US</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">Claimed</th>
                  <th style="width: 2%;font-size: 10px;text-align: center;">Status</th>
                  <th style="width: 1%;font-size: 10px;text-align: center;">Action</th>
                </thead>
                <tbody></tbody>
              </table>
            <!-- </div> -->
          </div>
        </div><!-- col -->
      </div><!-- row -->
    </div>
  </div>

  <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document" style="max-width: 750px;width: 750px">
      <div class="modal-content tx-14">
        <div class="modal-header pd-x-15 pd-y-10">
          <h6 class="modal-title">Edit Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body pd-x-15 pd-y-15">
          <form id="form_claim">
            <div class="row row-xs">
              <div class="col-sm-12">
                <div class="card card-body">
                  <div class="form-group row">
                    <input type="hidden" name="pnrID" id="pnrID">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">PNR</label>
                    <div class="col-sm-9 input-group">
                      <input type="hidden" name="thisPnr" id="thisPnr">
                      <input type="text" class="form-control" name="pnr" id="pnr" maxlength="6" onKeyUp="testPnr(this.value)" onblur="testPnr(this.value)">
                      <input type="hidden" name="isPnr" id="isPnr" value="0">
                      <div class="input-group-append" id="pnrStatus">
                        <span class="input-group-text" id="basic-addon2" style="width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Departure Date</label>
                    <div class="col-sm-3">
                      <select class="custom-select" name="depdate_dd" id="depdate_dd">
                      <?php
                          for ($i=1; $i <= 31; $i++) { 
                              $j = $i<10 ? '0'.$i : $i;
                              $selected = $j==date('d') ? 'selected' : '';
                              echo '<option value="'.$j.'" '.$selected.'>'.$j.'</option>';
                          }
                      ?>                           
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <select class="custom-select" name="depdate_mm" id="depdate_mm">
                      <?php
                          for ($i = 1; $i <= 12; $i++) {
                              $j = $i<10 ? '0'.$i : $i;
                              $date_str = date("F", strtotime("2019-" . $j . "-01"));
                              $selected = $j==date('m') ? 'selected' : '';
                              echo '<option value="'.$j.'" '.$selected.'>'.$date_str .'</option>';
                          }
                      ?>                           
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <select class="custom-select" name="depdate_yy" id="depdate_yy">
                      <?php
                          for ($i=date('Y'); $i <= (date('Y')+1); $i++) { 
                              echo '<option value="'.$i.'">'.$i.'</option>';
                          }
                      ?>                           
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Arrival Date</label>
                    <div class="col-sm-3">
                      <select class="custom-select" name="arrdate_dd" id="arrdate_dd">
                      <?php
                          for ($i=1; $i <= 31; $i++) { 
                              $j = $i<10 ? '0'.$i : $i;
                              $selected = $j==date('d') ? 'selected' : '';
                              echo '<option value="'.$j.'" '.$selected.'>'.$j.'</option>';
                          }
                      ?>                           
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <select class="custom-select" name="arrdate_mm" id="arrdate_mm">
                      <?php
                          for ($i = 1; $i <= 12; $i++) {
                              $j = $i<10 ? '0'.$i : $i;
                              $date_str = date("F", strtotime("2019-" . $j . "-01"));
                              $selected = $j==date('m') ? 'selected' : '';
                              echo '<option value="'.$j.'" '.$selected.'>'.$date_str .'</option>';
                          }
                      ?>                           
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <select class="custom-select" name="arrdate_yy" id="arrdate_yy">
                      <?php
                          for ($i=date('Y'); $i <= (date('Y')+1); $i++) { 
                              echo '<option value="'.$i.'">'.$i.'</option>';
                          }
                      ?>                           
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mg-b-15">
                    <label class="col-form-label col-sm-3 pt-0">Trip Type</label>
                    <div class="col-sm-9">
                      <div class="custom-control custom-radio mg-r-20" style="float: left;">
                        <input type="radio" id="tripTypeA" name="tripType" value="0" class="custom-control-input" checked>
                        <label class="custom-control-label" for="tripTypeA">Return</label>
                      </div>

                      <div class="custom-control custom-radio" style="float: left;">
                        <input type="radio" id="tripTypeB" name="tripType" value="1" class="custom-control-input">
                        <label class="custom-control-label" for="tripTypeB">Open Jaw</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-9 offset-sm-3 mg-b-10">
                      <small style="color: #0168fa;font-weight: bold;">Please write "NON" for SUB-SIN return only.</small>
                    </div>
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Destination From</label>
                    <div class="col-sm-9" style="padding: 0 !important">
                      <div class="col-sm-2" style="float: left;">
                        <input type="text" class="form-control" name="tripFrom_1" id="tripFrom_1" maxlength="3" value="SUB" readonly>
                      </div>
                      <div class="col-sm-2" style="float: left;">
                        <input type="text" class="form-control" name="tripFrom_2" id="tripFrom_2" maxlength="3" value="SIN" readonly>
                      </div>
                      <div class="col-sm-3 input-group" style="float: left;">
                        <input type="text" class="form-control" name="tripFrom_3" id="tripFrom_3" maxlength="3">
                        <input type="hidden" name="isFrom" id="isFrom" value="0">
                        <div class="input-group-append" id="tripFromStatus">
                          <span class="input-group-text" id="basic-addon2" style="width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>
                        </div>
                      </div>
                      <div class="col-sm-5" style="float: left;">
                        <select class="custom-select" name="classDep" id="classDep"></select>
                      </div>
                    </div>
                    <div class="col-sm-9 offset-sm-3 mg-t-10">
                      <span id="tripFromInfo"></span>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Destination To</label>
                    <div class="col-sm-9" style="padding: 0 !important">
                      <div class="col-sm-3 input-group" style="float: left;">
                        <input type="text" class="form-control" name="tripTo_1" id="tripTo_1" maxlength="3">
                        <input type="hidden" name="isTo" id="isTo" value="0">
                        <div class="input-group-append" id="tripToStatus">
                          <span class="input-group-text" id="basic-addon2" style="width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>
                        </div>
                      </div>
                      <div class="col-sm-2" style="float: left;">
                        <input type="text" class="form-control" name="tripTo_2" id="tripTo_2" maxlength="3" value="SIN" readonly>
                      </div>
                      <div class="col-sm-2" style="float: left;">
                        <input type="text" class="form-control" name="tripTo_3" id="tripTo_3" maxlength="3" value="SUB" readonly>
                      </div>
                      <div class="col-sm-5" style="float: left;">
                        <select class="custom-select" name="classArr" id="classArr"></select>
                      </div>
                    </div>
                    <div class="col-sm-9 offset-sm-3 mg-t-10">
                      <span id="tripToInfo"></span>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="remarks" class="col-sm-3 col-form-label">Remarks</label>
                    <div class="col-sm-9 input-group" style="float: left;">
                      <input type="text" class="form-control" name="remarks" id="remarks">
                    </div>
                  </div>
                </div>
              </div><!-- col -->
            </div><!-- row -->
          </form>
        </div>
        <div class="modal-footer pd-x-10 pd-y-10">
          <button type="button" id="btn_save_data" class="btn btn-primary tx-13" style="color: white">Save</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_add_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content tx-14">
        <div class="modal-header pd-x-15 pd-y-10">
          <h6 class="modal-title" id="modal_tittle"></h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body pd-x-15 pd-y-15">
          <form id="form_ticket">
            <input id="act" name="act" type="hidden" class="form-control" value="">
            <input id="id" name="id" type="hidden" class="form-control" value="">
            <div class="form-group mg-b-0">
              <label for="formGroupExampleInput2" class="d-block mg-b-0">Ticket No</label>
              <div class="input-group mg-b-10">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">618</span>
                </div>
                <input type="hidden" class="form-control" name="thisTicket" id="thisTicket">
                <input type="text" class="form-control" name="ticketNo" id="ticketNo" onKeyUp="this.value = this.value.replace(/[^0-9\.]/g,'');testTicket(this.value)" onblur="testTicket(this.value)" maxlength="10">
                <input type="hidden" class="form-control" name="isTicket" id="isTicket" value="0">
                <div class="input-group-append" id="ticketStatus"></div>
              </div>
              <label for="formGroupExampleInput2" class="d-block mg-b-0">Krisflyer No</label>
              <div class="input-group">
                <input type="text" class="form-control" name="kfNo" id="kfNo" onKeyUp="this.value = this.value.replace(/[^0-9\.]/g,'');testKf(this.value)" onblur="testKf(this.value)" maxlength="10">
                <input type="hidden" class="form-control" name="isKf" id="isKf" value="0">
                <div class="input-group-append" id="kfStatus"></div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer pd-x-10 pd-y-10">
          <button type="button" id="btn_save_details" class="btn btn-primary tx-13" style="color: white">Save</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content tx-14">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLabel5">Do you want to delete this data?</h6>
        </div>
        <div class="modal-footer pd-x-10 pd-y-10">
          <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Cancel</button>
          <a id="delete_data" class="btn btn-primary tx-13" style="color: white">Yes</a>
        </div>
      </div>
    </div>
  </div>

<?php include "footer.php"; ?>
<script src="../action/claimed_detail.js"></script>

<style type="text/css">
    #pnr, #tripFrom_3, #tripTo_1{
      text-transform: uppercase;
    }
    ::-webkit-input-placeholder { /* WebKit browsers */
      text-transform: none;
    }
    :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
      text-transform: none;
    }
    ::-moz-placeholder { /* Mozilla Firefox 19+ */
      text-transform: none;
    }
    :-ms-input-placeholder { /* Internet Explorer 10+ */
      text-transform: none;
    }
    ::placeholder { /* Recent browsers */
      text-transform: none;
    }

    .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
        padding-right: 10px !important;
        padding-left: 10px !important;
    }

    .table td {
      padding: 5px 7px;
      line-height: 1.5;
  }
</style>