<?php include "header.php"; ?>

<div class="content content-fixed bd-b">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
    <div class="d-sm-flex align-items-center justify-content-between">
      <div>
        <h4 class="mg-b-5">Queue #<span id="queueCode_duplicate"></span></h4>
        <p class="mg-b-0 tx-color-03" id="queueDate_duplicate"><?php echo date('d F Y'); ?></p>
      </div>
      <div class="mg-t-20 mg-sm-t-0">
        <button class="btn btn-white" onclick="print()"><i data-feather="printer" class="mg-r-5"></i> PRINT</button>
      </div>
    </div>
  </div>
</div>

<div class="content tx-13">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
    <div class="row">
      <div class="col-sm-4">
      </div>
      <div class="col-sm-3">
        <label class="tx-medium tx-12 tx-color-05 mg-b-10 tx-center">BCA - SINGAPORE AIRLINE TRAVEL FAIR 2020<br>13-16 FEBRUARI 2020</label>
        <h1 class="tx-normal tx-color-04 tx-spacing--2 tx-center" id="queueCode"></h1>
        <center><span class="mg-b-30 tx-center" id="barcode"></span></center>

        <span id="details"></span>
      </div>
      <div class="col-sm-5">
      </div>
    </div>
  </div>
</div>

<style type="text/css">
@media print {  
  @page {
    size: auto;
    /*58mm 100mm*/
  }
}
</style>

<span id="DivIdToPrint2" style="display: none;">
  <table border="0">
    <tr>
      <th colspan="2" style="font-size: 0.7em; text-align:center;">SQ - BCA TRAVEL FAIR 2020</th>
    </tr>
    <tr>
      <th colspan="2" style="font-size: 0.7em; text-align:center;">13-16 FEBRUARI 2020</th>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="font-size: 1em; text-align:center;"><span id="queueCode_print"></span></td>
    </tr>
    <tr>
      <td colspan="2" style="font-size: 0.7em; text-align:center;"><span id="barcode_print"></span></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tbody id="details_print"></tbody>
  </table>  
</span>

<?php include "footer.php"; ?>
<script src="../action/queue_print.js"></script>