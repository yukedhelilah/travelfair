<meta http-equiv="refresh" content="30"/>
<?php include "header.php"; ?>

  <div class="content content-fixed pd-15">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="margin: 0px;max-width: 100%">
      <div class="row row-xs">
        <div class="col-sm-6 col-lg-3">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">All Claimed</h6>
            <div>
              <h1 class="tx-primary tx-rubik mg-b-0 mg-r-5 lh-1" id="allClaimed" style="text-align: center;font-weight: bold;font-size: 50px"></h1>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">Claimed Today</h6>
            <div>
              <h1 class="tx-success tx-rubik mg-b-0 mg-r-5 lh-1" id="todayClaimed" style="text-align: center;font-weight: bold;font-size: 50px"></h1>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">Queue</h6>
            <div>
              <h1 class="tx-info tx-rubik mg-b-0 mg-r-5 lh-1" id="onQueue" style="text-align: center;font-weight: bold;font-size: 50px"></h1>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">Unclaimed</h6>
            <div>
              <h1 class="tx-warning tx-rubik mg-b-0 mg-r-5 lh-1" id="unclaimed" style="text-align: center;font-weight: bold;font-size: 50px"></h1>
            </div>
          </div>
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0 mg-b-10">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">FLAZZ CARD</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
              <table class="table table-bordered mg-b-0 tabelGift" id="tb_gift1"></table>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0 mg-b-10">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">FOLDABLE BAGS</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
              <table class="table table-bordered mg-b-0 tabelGift" id="tb_gift2"></table>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0 mg-b-10">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">SHOPPING BAG</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
              <table class="table table-bordered mg-b-0 tabelGift" id="tb_gift3"></table>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0 mg-b-10">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">CDV</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
              <table class="table table-bordered mg-b-0 tabelGift" id="tb_gift4"></table>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0 mg-b-10">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">CDV EXTRA</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
              <table class="table table-bordered mg-b-0 tabelGift" id="tb_gift5"></table>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0 mg-b-10">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">EZ-LINK CARD</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
              <table class="table table-bordered mg-b-0 tabelGift" id="tb_gift6"></table>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0 mg-b-10">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">MAP PER</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
              <table class="table table-bordered mg-b-0 tabelGift" id="tb_gift7"></table>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0 mg-b-10">
          <div class="card card-body">
            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">MAP EUR & US</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
              <table class="table table-bordered mg-b-0 tabelGift" id="tb_gift8"></table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php include "footer.php"; ?>
<script src="../action/dashboard.js"></script>

<style type="text/css">
  .tabelGift tr td{
    padding: 5px !important;
    line-height: normal !important;
  }
  .card-body {
    padding: 10px;
  }
</style>