<?php include "header.php"; ?>

  <div class="content content-fixed pd-15">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="margin: 0px;max-width: 100%">
      <div class="card">
        <div class="card-header" style="padding: 7px 7px 7px 12px;">
          <span style="font-size: 17px;padding-top: 3px;position: absolute;">Master Stock</span>
          <a onclick="add_data()" class="btn btn-xs btn-primary" style="float: right;color: white"><i class="fa fa-plus"></i> &nbspNew</a>
          <a id="btn_reset_queue" onclick="reset_queue()" class="btn btn-xs btn-danger mg-r-10" style="float: right;color: white"><i class="fa fa-redo-alt"></i> &nbspReset Queue</a>
        </div>
        <div class="card-body" style="padding: 7px 7px 7px 10px;">
          <table id="tb_admin" class="table">
            <thead>
              <tr>
                <th>No</th>
                <th>Item</th>
                <th>Stock Date</th>
                <th>Stock Total</th>
                <th>Act</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div><!-- container -->
  </div>

  <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content tx-14">
        <div class="modal-header pd-x-15 pd-y-10">
          <h6 class="modal-title" id="exampleModalLabel5"><span id="modal_tittle" style="padding-top: 2.5px;position: absolute;"></span></h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body pd-x-15 pd-y-15">
          <form id="form_stock">
            <input id="act" name="act" type="hidden" class="form-control" value="add">
            <input id="id" name="id" type="hidden" class="form-control" value="">
            <div class="form-group">
              <label for="formGroupExampleInput2" class="d-block mg-b-0">Item</label>
              <select class="custom-select" id="selectItem" name="selectItem">
              </select>
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput2" class="d-block mg-b-0">Stock Date</label>
              <input id="stockDate" name="stockDate" type="text" class="form-control" placeholder="Enter Stock Date">
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput" class="d-block mg-b-0">Stock Total</label>
              <input id="stockTotal" name="stockTotal" type="text" class="form-control" placeholder="Enter Stock Total" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');">
            </div>
          </form>
        </div>
        <div class="modal-footer pd-x-10 pd-y-10">
          <a onclick="save()" class="btn btn-primary tx-13" style="color: white">Save</a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content tx-14">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLabel5">Do you want to delete this data?</h6>
        </div>
        <div class="modal-footer pd-x-10 pd-y-10">
          <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Cancel</button>
          <a id="delete_data" class="btn btn-primary tx-13" style="color: white">Yes</a>
        </div>
      </div>
    </div>
  </div>

<?php include "footer.php"; ?>
<script src="../lib/jqueryui/jquery-ui.min.js"></script>
<script src="../action/stock.js"></script>

<style type="text/css">
  #ui-datepicker-div{
    z-index: 1051 !important;
  }

  .tabelGift tr td{
    padding: 5px !important;
    line-height: normal !important;
  }
</style>