<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
<?php include "header.php"; ?>

  <div class="content content-fixed pd-15">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="margin: 0px;max-width: 100%">
      <div class="row row-xs">
        <ul class="nav nav-tabs nav-justified" id="myTab3" role="tablist" style="width: 100%">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#cdv" role="tab" aria-controls="home" aria-selected="true">CDV</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#cdv_extra" role="tab" aria-controls="profile" aria-selected="false">CDV Extra</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#map_per" role="tab" aria-controls="contact" aria-selected="false">MAP PER</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#map_us" role="tab" aria-controls="contact" aria-selected="false">MAP EUR & US</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#flazz" role="tab" aria-controls="contact" aria-selected="false">Flazz Card</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#ezlink" role="tab" aria-controls="contact" aria-selected="false">Ez-Link</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#daily_report" role="tab" aria-controls="contact" aria-selected="false">Daily Report</a>
          </li>
        </ul>
        <div class="tab-content bd bd-gray-300 bd-t-0 pd-y-10" id="myTabContent3" style="width: 100%">
          <div class="tab-pane fade show active" id="cdv" role="tabpanel" aria-labelledby="home-tab3">
            <table id="tb_cdv" class="table table-bordered mg-0" style="width: 100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Agent</th>
                  <th>Issuance</th>
                  <th>PNR</th>
                  <th>Ticket</th>
                  <th>Krisflyer</th>
                  <th>Itinerary</th>
                  <th>CDV1</th>
                  <th>CDV2</th>
                  <th>Claimed</th>
                </tr>
              </thead>
            </table>
          </div>
          <div class="tab-pane fade" id="cdv_extra" role="tabpanel" aria-labelledby="profile-tab3">
            <table id="tb_cdv_extra" class="table table-bordered mg-0" style="width: 100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Agent</th>
                  <th>Issuance</th>
                  <th>PNR</th>
                  <th>Ticket</th>
                  <th>Krisflyer</th>
                  <th>Itinerary</th>
                  <th>CDV</th>
                  <th>Claimed</th>
                </tr>
              </thead>
            </table>
          </div>
          <div class="tab-pane fade" id="map_per" role="tabpanel" aria-labelledby="contact-tab3">
            <table id="tb_map_per" class="table table-bordered mg-0" style="width: 100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Agent</th>
                  <th>Issuance</th>
                  <th>PNR</th>
                  <th>Ticket</th>
                  <th>Krisflyer</th>
                  <th>Itinerary</th>
                  <th>MAP1</th>
                  <th>MAP2</th>
                  <th>Claimed</th>
                </tr>
              </thead>
            </table>
          </div>
          <div class="tab-pane fade" id="map_us" role="tabpanel" aria-labelledby="contact-tab3">
            <table id="tb_map_us" class="table table-bordered mg-0" style="width: 100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Agent</th>
                  <th>Issuance</th>
                  <th>PNR</th>
                  <th>Ticket</th>
                  <th>Krisflyer</th>
                  <th>Itinerary</th>
                  <th>MAP</th>
                  <th>Claimed</th>
                </tr>
              </thead>
            </table>
          </div>
          <div class="tab-pane fade" id="flazz" role="tabpanel" aria-labelledby="contact-tab3">
            <table id="tb_flazz" class="table table-bordered mg-0" style="width: 100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Agent</th>
                  <th>Issuance</th>
                  <th>PNR</th>
                  <th>Ticket</th>
                  <th>Krisflyer</th>
                  <th>Itinerary</th>
                  <th>Flazz</th>
                  <th>Claimed</th>
                </tr>
              </thead>
            </table>
          </div>
          <div class="tab-pane fade" id="ezlink" role="tabpanel" aria-labelledby="contact-tab3">
            <table id="tb_ezlink" class="table table-bordered mg-0" style="width: 100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Agent</th>
                  <th>Issuance</th>
                  <th>PNR</th>
                  <th>Ticket</th>
                  <th>Krisflyer</th>
                  <th>Itinerary</th>
                  <th>Ez-Link</th>
                  <th>Claimed</th>
                </tr>
              </thead>
            </table>
          </div>
          <div class="tab-pane fade" id="daily_report" role="tabpanel" aria-labelledby="contact-tab3">
            <table id="tb_daily_report" class="table table-bordered mg-0" style="width: 100%;font-size:9px">
              <thead>
                <tr>
                  <th>Agent</th>
                  <th>Queue Code</th>
                  <th>PNR</th>
                  <th>Trip1</th>
                  <th>Class</th>
                  <th>Trip2</th>
                  <th>Class</th>
                  <th>Depdate</th>
                  <th>Arrdate</th>
                  <th>Ticket</th>
                  <th>Krisflyer</th>
                  <th>Flazz Card</th>
                  <th>Barcode</th>
                  <th>Foldable Bag</th>
                  <th>Shopping Bag</th>
                  <th>CDV</th>
                  <th>Barcode</th>
                  <th>Barcode</th>
                  <th>CDV Extra</th>
                  <th>Barcode</th>
                  <th>EZ-Link</th>
                  <th>Barcode</th>
                  <th>MAP PER</th>
                  <th>Barcode</th>
                  <th>Barcode</th>
                  <th>MAP US</th>
                  <th>Barcode</th>
                  <th>Claimed By</th>
                  <th>Claimed Date</th>
                  <th>Claimed Time</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>  
      </div>
    </div>
  </div>

<?php include "footer.php"; ?>
<script src="../action/report.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>