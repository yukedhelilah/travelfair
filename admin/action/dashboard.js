$(document).ready(function() {
	dashboard();
    gift();
});

function dashboard(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/dashboard/dashboard/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/dashboard/dashboard/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
        	$('#allClaimed').html(response.data.allClaimed);
        	$('#todayClaimed').html(response.data.todayClaimed);
        	$('#onQueue').html(response.data.onQueue);
        	$('#unclaimed').html(response.data.unclaimed);
        },
    });
}

function gift(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/dashboard/gift/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/dashboard/gift/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            $('#tb_gift1').append(`
                <tr>
                  <td colspan="3" style="text-align: center;height: 45px">
                    <small style="font-size: 12px">BALANCE</small>
                    <p class="mg-0" style="font-size: 40px;font-weight: bold;color: #dc3545">`+response.data.gift1.balance+`</p>
                  </td>
                </tr>
                <tr>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">STOCK</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift1.stock+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">CLAIMED</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift1.claimed+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">QUEUE</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift1.queue+`</p>
                  </td>
                </tr>
            `);
            $('#tb_gift2').append(`
                <tr>
                  <td colspan="3" style="text-align: center;height: 45px">
                    <small style="font-size: 12px">BALANCE</small>
                    <p class="mg-0" style="font-size: 40px;font-weight: bold;color: #dc3545">`+response.data.gift2.balance+`</p>
                  </td>
                </tr>
                <tr>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">STOCK</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift2.stock+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">CLAIMED</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift2.claimed+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">QUEUE</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift2.queue+`</p>
                  </td>
                </tr>
            `);
            $('#tb_gift3').append(`
                <tr>
                  <td colspan="3" style="text-align: center;height: 45px">
                    <small style="font-size: 12px">BALANCE</small>
                    <p class="mg-0" style="font-size: 40px;font-weight: bold;color: #dc3545">`+response.data.gift3.balance+`</p>
                  </td>
                </tr>
                <tr>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">STOCK</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift3.stock+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">CLAIMED</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift3.claimed+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">QUEUE</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift3.queue+`</p>
                  </td>
                </tr>
            `);
            $('#tb_gift4').append(`
                <tr>
                  <td colspan="3" style="text-align: center;height: 45px">
                    <small style="font-size: 12px">BALANCE</small>
                    <p class="mg-0" style="font-size: 40px;font-weight: bold;color: #dc3545">`+response.data.gift4.balance+`</p>
                  </td>
                </tr>
                <tr>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">STOCK</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift4.stock+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">CLAIMED</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift4.claimed+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">QUEUE</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift4.queue+`</p>
                  </td>
                </tr>
            `);
            $('#tb_gift5').append(`
                <tr>
                  <td colspan="3" style="text-align: center;height: 45px">
                    <small style="font-size: 12px">BALANCE</small>
                    <p class="mg-0" style="font-size: 40px;font-weight: bold;color: #dc3545">`+response.data.gift5.balance+`</p>
                  </td>
                </tr>
                <tr>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">STOCK</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift5.stock+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">CLAIMED</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift5.claimed+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">QUEUE</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift5.queue+`</p>
                  </td>
                </tr>
            `);
            $('#tb_gift6').append(`
                <tr>
                  <td colspan="3" style="text-align: center;height: 45px">
                    <small style="font-size: 12px">BALANCE</small>
                    <p class="mg-0" style="font-size: 40px;font-weight: bold;color: #dc3545">`+response.data.gift6.balance+`</p>
                  </td>
                </tr>
                <tr>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">STOCK</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift6.stock+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">CLAIMED</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift6.claimed+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">QUEUE</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift6.queue+`</p>
                  </td>
                </tr>
            `);
            $('#tb_gift7').append(`
                <tr>
                  <td colspan="3" style="text-align: center;height: 45px">
                    <small style="font-size: 12px">BALANCE</small>
                    <p class="mg-0" style="font-size: 40px;font-weight: bold;color: #dc3545">`+response.data.gift7.balance+`</p>
                  </td>
                </tr>
                <tr>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">STOCK</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift7.stock+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">CLAIMED</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift7.claimed+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">QUEUE</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift7.queue+`</p>
                  </td>
                </tr>
            `);
            $('#tb_gift8').append(`
                <tr>
                  <td colspan="3" style="text-align: center;height: 45px">
                    <small style="font-size: 12px">BALANCE</small>
                    <p class="mg-0" style="font-size: 40px;font-weight: bold;color: #dc3545">`+response.data.gift8.balance+`</p>
                  </td>
                </tr>
                <tr>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">STOCK</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift8.stock+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">CLAIMED</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift8.claimed+`</p>
                  </td>
                  <td style="text-align: center;width: 33%">
                    <small style="font-size: 12px">QUEUE</small>
                    <p class="mg-0" style="font-size: 25px;font-weight: bold;color: #535353">`+response.data.gift8.queue+`</p>
                  </td>
                </tr>
            `);

            // $('#tb_gift').append(`
            //   <tr>
            //     <th>Flazz Cards</th>
            //     <th style="color: #535353">`+response.data.gift1.stock+`</th>
            //     <th style="color: #535353">`+response.data.gift1.claimed+`</th>
            //     <th style="color: #00b8d4">`+response.data.gift1.queue+`</th>
            //     <th style="color: #dc3545">`+response.data.gift1.balance+`</th>
            //   </tr>
            //   <tr>
            //     <th>Foldable Bags</th>
            //     <th style="color: #535353">`+response.data.gift2.stock+`</th>
            //     <th style="color: #535353">`+response.data.gift2.claimed+`</th>
            //     <th style="color: #00b8d4">`+response.data.gift2.queue+`</th>
            //     <th style="color: #dc3545">`+response.data.gift2.balance+`</th>
            //   </tr>
            //   <tr>
            //     <th>Shopping Bags</th>
            //     <th style="color: #535353">`+response.data.gift3.stock+`</th>
            //     <th style="color: #535353">`+response.data.gift3.claimed+`</th>
            //     <th style="color: #00b8d4">`+response.data.gift3.queue+`</th>
            //     <th style="color: #dc3545">`+response.data.gift3.balance+`</th>
            //   </tr>
            //   <tr>
            //     <th>CDV</th>
            //     <th style="color: #535353">`+response.data.gift4.stock+`</th>
            //     <th style="color: #535353">`+response.data.gift4.claimed+`</th>
            //     <th style="color: #00b8d4">`+response.data.gift4.queue+`</th>
            //     <th style="color: #dc3545">`+response.data.gift4.balance+`</th>
            //   </tr>
            //   <tr>
            //     <th>CDV Extra</th>
            //     <th style="color: #535353">`+response.data.gift5.stock+`</th>
            //     <th style="color: #535353">`+response.data.gift5.claimed+`</th>
            //     <th style="color: #00b8d4">`+response.data.gift5.queue+`</th>
            //     <th style="color: #dc3545">`+response.data.gift5.balance+`</th>
            //   </tr>
            //   <tr>
            //     <th>Ex-link Card</th>
            //     <th style="color: #535353">`+response.data.gift1.stock+`</th>
            //     <th style="color: #535353">`+response.data.gift1.claimed+`</th>
            //     <th style="color: #00b8d4">`+response.data.gift1.queue+`</th>
            //     <th style="color: #dc3545">`+response.data.gift1.balance+`</th>
            //   </tr>
            //   <tr>
            //     <th>MAP PER</th>
            //     <th style="color: #535353">`+response.data.gift6.stock+`</th>
            //     <th style="color: #535353">`+response.data.gift6.claimed+`</th>
            //     <th style="color: #00b8d4">`+response.data.gift6.queue+`</th>
            //     <th style="color: #dc3545">`+response.data.gift6.balance+`</th>
            //   </tr>
            //   <tr>
            //     <th>MAP EUR & US</th>
            //     <th style="color: #535353">`+response.data.gift7.stock+`</th>
            //     <th style="color: #535353">`+response.data.gift7.claimed+`</th>
            //     <th style="color: #00b8d4">`+response.data.gift7.queue+`</th>
            //     <th style="color: #dc3545">`+response.data.gift7.balance+`</th>
            //   </tr>
            // `);
        },
    });
}