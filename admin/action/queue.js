localStorage.removeItem(btoa('admin_queueID'));

$(document).ready(function() {
	get_data();
});

function get_data(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/queue/get_data/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/queue/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var status 		= "";
                var guest       = "";
                var pnr         = ` 
                                    <table style="Width:100%">
                                        <tr>
                                            <td rowspan="`+value.noofticket+`" style="Width:1%">`+value.noofpnr+` PNR<br>`+value.noofticket+` Ticket</td>
                                `;

                $.each(value.detail, function( j, val ) {
                    pnr += `<td rowspan="`+val.ticket.length+`" style="width:1%">`+val.pnr+`</td>`;
                    $.each(val.ticket, function( k, values ) {
                        if (j.id == k.claimID) {
                            if (k == 0) {
                                pnr += `<td style="width:1%">618`+values.ticketNo+`</td></tr>`;
                            } else {
                                pnr += `<tr><td style="width:1%">618`+values.ticketNo+`</td></tr>`;
                            }
                        }
                    });
                });

                pnr += `</table>`;

                if(value.isStatus == 1){
                	status = `<span class="badge badge-warning pd-x-10 pd-y-5" style="color:white">OPEN</span>`
                } else {
                	status = `<span class="badge badge-success pd-x-10 pd-y-5" style="color:white">DONE</span>`
                }

                if (value.guestName == "") {
                    guest = "-";
                } else {
                    guest = value.guestName+`<br>`+value.guestPhone;
                }
                
                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    dateFormat(value.createdDate)+` [`+ value.createdTime +`]`,
                    value.queueCode, 
                    pnr, 
                    value.createdBy, 
                    guest, 
                    `<div style="text-align:center">` + status + `</div>`, 
                    `<div style="text-align:center">
                    	<div data-toggle="tooltip" title="Edit" onclick="queue_detail('`+value.code+`')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
                		</div>
                        <div data-toggle="tooltip" title="Reprint" onclick="queue_print('`+value.queueCode+`')" class="btn btn-xs btn-info" style="padding: 1px 5px;">
                            <i style="vertical-align: middle;font-size: 10px;" class="fa fa-print"></i>
                        </div>
            		</div>`,
                ]);
            });
            $('#tb_queue').DataTable().destroy();
        	$('#tb_queue').DataTable({
				// 'responsive': true,
				'language'	: {
					'searchPlaceholder'		: 'Search...',
					'sSearch'				: '',
					'lengthMenu'			: '_MENU_ items/page',
			  	},
				'aaData'	: aaData,
				"bJQueryUI"	: true,
				"aoColumns"	: [
					{ "sWidth"	: "1%" },  
					{ "sWidth"	: "2%" }, 
					{ "sWidth"	: "2%" }, 
                    { "sWidth"  : "5%" }, 
					{ "sWidth"	: "1%" },
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
					{ "sWidth"	: "2%" },  
				],
				'aLengthMenu'	: [
					[10, 20, 50, -1],
					[10, 20, 50, "All"]
				],
			});
        },
    });
}

function queue_detail(code){
    localStorage.setItem(btoa('admin_queueCode'), btoa(code));
    window.open("queue_detail");
}

function queue_print(code){
    localStorage.setItem(btoa('admin_queuePrint'), btoa(code));
    window.open("queue_print", "_blank");
}