var adminID     = atob(localStorage.getItem(btoa("admin_id")));
var name     	= atob(localStorage.getItem(btoa("admin_name")));
var username    = atob(localStorage.getItem(btoa("admin_username")));
var password    = atob(localStorage.getItem(btoa("admin_password")));
var token    	= atob(localStorage.getItem(btoa("admin_token")));
var expired     = localStorage.getItem(btoa("admin_expired"));
var exp         = new Date(atob(expired));
var now         = new Date();

var bulans          = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
var days            = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

if(name == null || username == null || token == null || expired == null || exp.getDate() +`-`+ exp.getMonth() +`-`+ exp.getFullYear() != now.getDate() +`-`+ now.getMonth() +`-`+ now.getFullYear()){
    removeLocalstorage();
    window.location.href = '../pages/login';
}

if (password == 0) {
    $('#modal_change_password').modal({backdrop: 'static', keyboard: false});
    $('#adminID').val(adminID);
}

if (username != 'superid') {
    $('#btn_reset_queue').css('display','none');
}

$(document).ready(function() {
    $('#loginName').html(name);
});

function dateFormat(date) {
    var format = new Date(date);
    var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
    return date + ' ' + bulans[format.getMonth()] + ' ' + format.getFullYear().toString().substr(2,2);
}

function changePassword(){
    if($('#password').val() == ''){
        alert('Password must be required');
    } else if ($('#password').val().length != 4) {
        alert('Password must be 4 digits');
    } else if ($('#password').val() == '1234'){
        alert('Password must not be 1234');
    } else {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'http://sqtfsurabaya.com/api/Admin/login/change_password/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/login/change_password/_',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                token           : '1b787b70ed2e0de697d731f14b5da57b',
                id              : $('#adminID').val(),
                password        : $('#password').val()
            },
            success: function(response) {
                alert('success');
                removeLocalstorage();
                window.location.href = '../pages/login';
            },
        });
    }
}

function logout() {
    if (confirm("Do you want to logout?")) {
        removeLocalstorage();
        window.location.href = '../pages/login';
    } 
}

function removeLocalstorage(){
    localStorage.removeItem(btoa('admin_name'));
    localStorage.removeItem(btoa('admin_username'));
    localStorage.removeItem(btoa('admin_token'));
    localStorage.removeItem(btoa('admin_expired'));
}