var queuePrint   = atob(localStorage.getItem(btoa("admin_queuePrint")));

$(function(){
  'use strict'
  loadData();
});

function loadData(){
  $.ajax({
    type: 'GET',
    url: 'http://sqtfsurabaya.com/api/checker/data/print_preview',
    dataType: 'json',
    data: {
      'code'   : queuePrint,
    },
    beforeSend: function() {
      $('#tb_summary').empty();
    },
    success: function(responses) {
      if(responses.status == true){
        $('#queueCode').html(responses.data.queueCode);
        $('#queueCode_duplicate').html(responses.data.queueCode);
        $('#queueDate_duplicate').html(responses.data.queueDate);
        $('#barcode').html('<img src="http://sqtfsurabaya.com/api/checker/barcode/set_barcode?code='+ responses.data.queueCode +'">');

        $('#queueCode_print').html(responses.data.queueCode);
        $('#barcode_print').html('<img src="http://sqtfsurabaya.com/api/checker/barcode/set_barcode?code='+ responses.data.queueCode +'">');

        $.each(responses.data.details, function(i, e){
          $('#details').append(`
            <div class="divider-text">PNR `+ e.pnr +` / `+ e.noofticket +` TICKET</div>
            <ul class="list-unstyled lh-7">
              <li class="d-flex justify-content-between">
                <span>Flazz Card</span>
                <span>`+ e.gift_1 +`</span>
              </li>
              <li class="d-flex justify-content-between">
                <span>Foldable Bags</span>
                <span>`+ e.gift_2 +`</span>
              </li>
              <li class="d-flex justify-content-between">
                <span>KF / Shoe Bags</span>
                <span>`+ e.gift_3 +`</span>
              </li>
              <li class="d-flex justify-content-between">
                <span>CDV</span>
                <span>`+ e.gift_4 +`</span>
              </li>
              <li class="d-flex justify-content-between">
                <span>CDV Extra</span>
                <span>`+ e.gift_5 +`</span>
              </li>
              <li class="d-flex justify-content-between">
                <span>Ez-link Card SGD 15</span>
                <span>`+ e.gift_6 +`</span>
              </li>
              <li class="d-flex justify-content-between">
                <span>MAP 200K - Perth</span>
                <span>`+ e.gift_7 +`</span>
              </li>
              <li class="d-flex justify-content-between">
                <span>MAP 100K - EUR & USA</span>
                <span>`+ e.gift_8 +`</span>
              </li>
            </ul>
          `);

          $('#details_print').append(`
            <tr>
              <td colspan="2" style="font-size: 0.7em;"><b>PNR `+ e.pnr +` / `+ e.noofticket +` TICKET</b></td>
            <tr>
            <tr>
              <td style="font-size: 0.7em;">Flazz Card</td>
              <td style="font-size: 0.7em;">`+ e.gift_1 +`</td>
            </tr>
            <tr>
              <td style="font-size: 0.7em;">Foldable Bags</td>
              <td style="font-size: 0.7em;">`+ e.gift_2 +`</td>
            </tr>
            <tr>
              <td style="font-size: 0.7em;">KF / Shoe Bags</td>
              <td style="font-size: 0.7em;">`+ e.gift_3 +`</td>
            </tr>
            <tr>
              <td style="font-size: 0.7em;">CDV</td>
              <td style="font-size: 0.7em;">`+ e.gift_4 +`</td>
            </tr>
            <tr>
              <td style="font-size: 0.7em;">CDV Extra</td>
              <td style="font-size: 0.7em;">`+ e.gift_5 +`</td>
            </tr>
            <tr>
              <td style="font-size: 0.7em;">Ez-link Card SGD 15</td>
              <td style="font-size: 0.7em;">`+ e.gift_6 +`</td>
            </tr>
            <tr>
              <td style="font-size: 0.7em;">MAP 200K - Perth</td>
              <td style="font-size: 0.7em;">`+ e.gift_7 +`</td>
            </tr>
            <tr>
              <td style="font-size: 0.7em;">MAP 100K - EUR & USA</td>
              <td style="font-size: 0.7em;">`+ e.gift_8 +`</td>
            </tr>
          `);
        });
      }
    }
  });
}

function print(){
  var divToPrint = document.getElementById('DivIdToPrint2');
  var newWin = window.open('','Print-Window');
  newWin.document.open();
  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
  newWin.document.close();
  setTimeout(function(){newWin.close();},10); 
}