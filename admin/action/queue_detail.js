var queueCode   = atob(localStorage.getItem(btoa("admin_queueCode")));

$(function(){
  'use strict'
  loadData();
});

function loadData(){
  $.ajax({
    type: 'POST',
    url: 'http://sqtfsurabaya.com/api/checker/data/edit_queue',
    dataType: 'json',
    data: {
      'search'   : queueCode,
    },
    beforeSend: function() {
      $('#tb_summary').empty();
    },
    success: function(responses) {
      var no = 0; var no_pnr = 0;
      $.each(responses.data, function(i, e){
        if(i == 0){
          no = e.claimID;
          no_pnr++;
          $('#tb_summary').append(`<tr class="tx-12 tx-bold tx-uppercase tx-black"><td colspan="11">
            <i class="fa fa-file"></i> PNR : <b>`+ e.pnr +`</b> &nbsp&nbsp&nbsp&nbsp&nbsp 
            <i class="fa fa-map-marker-alt"></i> DESTINATION : <b>`+ e.route_departure +` (`+ e.classDepName +`) - `+ e.route_return +` (`+ e.classArrName +`)</b> &nbsp&nbsp&nbsp&nbsp&nbsp 
            <i class="fa fa-calendar-alt"></i> DATE : <b>`+ e.depdate +` - `+ e.arrdate +`</b></td></tr>`);
        }else{
          if(no != e.claimID){
            no = e.claimID;
            no_pnr++;
            $('#tb_summary').append(`<tr class="tx-12 tx-bold tx-uppercase tx-black"><td colspan="11">
              <i class="fa fa-file"></i> PNR : <b>`+ e.pnr +`</b> &nbsp&nbsp&nbsp&nbsp&nbsp 
              <i class="fa fa-map-marker-alt"></i> DESTINATION : <b>`+ e.route_departure +` (`+ e.classDepName +`) - `+ e.route_return +` (`+ e.classArrName +`)</b> &nbsp&nbsp&nbsp&nbsp&nbsp 
              <i class="fa fa-calendar-alt"></i> DEPARTURE : <b>`+ e.depdate +`</b></td></tr>`);
          }
        }

        $('#tb_summary').append(`
          <tr class="tx-12 tx-bold tx-uppercase tx-`+ e.color +`">
            <td>
              <input type="text" class="form-control" aria-label="Ticket number" name="ticketNo[`+ e.id +`]" value="`+ e.ticketNo +`">
            </td>
            <td>
              <input type="text" class="form-control" aria-label="Krissflyer number" name="kfNo[`+ e.id +`]" value="`+ e.kfNo +`">
            </td>
            <td>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-light" type="button" id="button-min_1_`+ e.id +`" onclick="min(1,`+ e.id +`)">-</button>
                </div>
                <input type="text" class="form-control" readonly id="gift_1_`+ e.id +`" name='gift_1[`+ e.id +`]' value="`+ e.gift_1 +`">
                <div class="input-group-append">
                  <button class="btn btn-outline-light" type="button" id="button-plus_1_`+ e.id +`" onclick="plus(1,`+ e.id +`)">+</button>
                </div>
              </div>
            </td>
            <td>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-light" type="button" id="button-min_2_`+ e.id +`" onclick="min(2,`+ e.id +`)">-</button>
                </div>
                <input type="text" class="form-control" readonly id="gift_2_`+ e.id +`" name='gift_2[`+ e.id +`]' value="`+ e.gift_2 +`">
                <div class="input-group-append">
                  <button class="btn btn-outline-light" type="button" id="button-plus_2_`+ e.id +`" onclick="plus(2,`+ e.id +`)">+</button>
                </div>
              </div>
            </td>
            <td>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-light" type="button" id="button-min_3_`+ e.id +`" onclick="min(3,`+ e.id +`)">-</button>
                </div>
                <input type="text" class="form-control" readonly id="gift_3_`+ e.id +`" name='gift_3[`+ e.id +`]' value="`+ e.gift_3 +`">
                <div class="input-group-append">
                  <button class="btn btn-outline-light" type="button" id="button-plus_3_`+ e.id +`" onclick="plus(3,`+ e.id +`)">+</button>
                </div>
              </div>
            </td>
            <td>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-light" type="button" id="button-min_4_`+ e.id +`" onclick="min(4,`+ e.id +`)">-</button>
                </div>
                <input type="text" class="form-control" readonly id="gift_4_`+ e.id +`" name='gift_4[`+ e.id +`]' value="`+ e.gift_4 +`">
                <div class="input-group-append">
                  <button class="btn btn-outline-light" type="button" id="button-plus_4_`+ e.id +`" onclick="plus(4,`+ e.id +`)">+</button>
                </div>
              </div>
            </td>
            <td>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-light" type="button" id="button-min_5_`+ e.id +`" onclick="min(5,`+ e.id +`)">-</button>
                </div>
                <input type="text" class="form-control" readonly id="gift_5_`+ e.id +`" name='gift_5[`+ e.id +`]' value="`+ e.gift_5 +`">
                <div class="input-group-append">
                  <button class="btn btn-outline-light" type="button" id="button-plus_5_`+ e.id +`" onclick="plus(5,`+ e.id +`)">+</button>
                </div>
              </div>
            </td>
            <td>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-light" type="button" id="button-min_6_`+ e.id +`" onclick="min(6,`+ e.id +`)">-</button>
                </div>
                <input type="text" class="form-control" readonly id="gift_6_`+ e.id +`" name='gift_6[`+ e.id +`]' value="`+ e.gift_6 +`">
                <div class="input-group-append">
                  <button class="btn btn-outline-light" type="button" id="button-plus_6_`+ e.id +`" onclick="plus(6,`+ e.id +`)">+</button>
                </div>
              </div>
            </td>
            <td>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-light" type="button" id="button-min_7_`+ e.id +`" onclick="min(7,`+ e.id +`)">-</button>
                </div>
                <input type="text" class="form-control" readonly id="gift_7_`+ e.id +`" name='gift_7[`+ e.id +`]' value="`+ e.gift_7 +`">
                <div class="input-group-append">
                  <button class="btn btn-outline-light" type="button" id="button-plus_7_`+ e.id +`" onclick="plus(7,`+ e.id +`)">+</button>
                </div>
              </div>
            </td>
            <td>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-light" type="button" id="button-min_8_`+ e.id +`" onclick="min(8,`+ e.id +`)">-</button>
                </div>
                <input type="text" class="form-control" readonly id="gift_8_`+ e.id +`" name='gift_8[`+ e.id +`]' value="`+ e.gift_8 +`">
                <div class="input-group-append">
                  <button class="btn btn-outline-light" type="button" id="button-plus_8_`+ e.id +`" onclick="plus(8,`+ e.id +`)">+</button>
                </div>
              </div>
            </td>
          </tr>          
        `);
      });
      
      $('#no_pnr').val(no_pnr);
      $('#no_ticket').val(responses.data.length);
    }
  });
}

function min(type,id){
  var current = parseInt($('#gift_'+type+'_'+id).val())-1;
  if(current < 0){
    $('#gift_'+type+'_'+id).val(0);
  }else{
    $('#gift_'+type+'_'+id).val(current);
  }
}

function plus(type,id){
  var current = parseInt($('#gift_'+type+'_'+id).val())+1;
  $('#gift_'+type+'_'+id).val(current);
}

function save(){
  $.ajax({
    type: 'POST',
    url: 'http://sqtfsurabaya.com/api/checker/data/update_data_queue/_',
    dataType: 'json',
    cache: false,
    data: $('#newForm').serialize(),
    success: function (responses) {
      if(responses.status==true){
        alert('Successfully update.');
        loadData();
      }else{
        alert(responses.error);
        loadData();
      }
    }
  });
}