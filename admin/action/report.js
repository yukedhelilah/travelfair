$(document).ready(function() {
	get_cdv();
    get_cdv_extra();
    get_map_per();
    get_map_us();
    get_flazz();
    get_ezlink();
    get_daily();
});

function get_cdv(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/report/get_cdv/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/report/get_cdv/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var agent       = value.agentName.split(" ");
                var agentName   = "";

                if (agent.length > 1) {
                    agentName   = agent[0] + ' ' + agent[1];
                } else {
                    agentName   = agent[0];
                }
                
                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    value.agentName,
                    dateFormat(value.createdDate),
                    value.pnr, 
                    `618`+value.ticketNo,
                    value.kfNo,  
                    value.tripFrom_1+`-`+value.tripFrom_2+`-`+value.tripFrom_3+`(`+value.depClass+`) || `+value.tripTo_1+`-`+value.tripTo_2+`-`+value.tripTo_3+`(`+value.arrClass+`)`, 
                    value.cdv1,
                    value.cdv2,
                    value.adminName+`, `+dateFormat(value.claimedDate)+` [`+value.claimedTime+`]`,
                ]);
            });
            $('#tb_cdv').DataTable().destroy();
        	$('#tb_cdv').DataTable({
				// 'responsive': true,
				'language'	: {
					'searchPlaceholder'		: 'Search...',
					'sSearch'				: '',
					'lengthMenu'			: '_MENU_ items/page',
			  	},
				'aaData'	: aaData,
				"bJQueryUI"	: true,
				"aoColumns"	: [
					{ "sWidth"	: "1%" },  
					{ "sWidth"	: "2%" }, 
					{ "sWidth"	: "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
					{ "sWidth"	: "1%" }, 
					{ "sWidth"	: "6%" }, 
					{ "sWidth"	: "4%" }, 
					{ "sWidth"	: "4%" }, 
					{ "sWidth"	: "3%" },  
				],
				'aLengthMenu'	: [
					[-1],
					["All"]
				],
                dom: 'Bfrtip',
                buttons: [ {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    sheetName: 'Exported data'
                }, 'copy', 'print' ]
			});
        },
    });
}

function get_cdv_extra(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/report/get_cdv_extra/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/report/get_cdv_extra/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var agent       = value.agentName.split(" ");
                var agentName   = "";

                if (agent.length > 1) {
                    agentName   = agent[0] + ' ' + agent[1];
                } else {
                    agentName   = agent[0];
                }
                
                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    value.agentName,
                    dateFormat(value.createdDate),
                    value.pnr, 
                    `618`+value.ticketNo,
                    value.kfNo,  
                    value.tripFrom_1+`-`+value.tripFrom_2+`-`+value.tripFrom_3+`(`+value.depClass+`) || `+value.tripTo_1+`-`+value.tripTo_2+`-`+value.tripTo_3+`(`+value.arrClass+`)`, 
                    value.cdv,
                    value.adminName+`, `+dateFormat(value.claimedDate)+` [`+value.claimedTime+`]`,
                ]);
            });
            $('#tb_cdv_extra').DataTable().destroy();
            $('#tb_cdv_extra').DataTable({
                // 'responsive': true,
                'language'  : {
                    'searchPlaceholder'     : 'Search...',
                    'sSearch'               : '',
                    'lengthMenu'            : '_MENU_ items/page',
                },
                'aaData'    : aaData,
                "bJQueryUI" : true,
                "aoColumns" : [
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "2%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "6%" }, 
                    { "sWidth"  : "4%" }, 
                    { "sWidth"  : "3%" },  
                ],
                'aLengthMenu'   : [
                    [-1],
                    ["All"]
                ],
                dom: 'Bfrtip',
                buttons: [ {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    sheetName: 'Exported data'
                }, 'copy', 'print' ]
            });
        },
    });
}

function get_map_per(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/report/get_map_per/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/report/get_map_per/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var agent       = value.agentName.split(" ");
                var agentName   = "";

                if (agent.length > 1) {
                    agentName   = agent[0] + ' ' + agent[1];
                } else {
                    agentName   = agent[0];
                }
                
                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    value.agentName,
                    dateFormat(value.createdDate),
                    value.pnr, 
                    `618`+value.ticketNo,
                    value.kfNo,  
                    value.tripFrom_1+`-`+value.tripFrom_2+`-`+value.tripFrom_3+`(`+value.depClass+`) || `+value.tripTo_1+`-`+value.tripTo_2+`-`+value.tripTo_3+`(`+value.arrClass+`)`, 
                    value.map1,
                    value.map2,
                    value.adminName+`, `+dateFormat(value.claimedDate)+` [`+value.claimedTime+`]`,
                ]);
            });
            $('#tb_map_per').DataTable().destroy();
            $('#tb_map_per').DataTable({
                // 'responsive': true,
                'language'  : {
                    'searchPlaceholder'     : 'Search...',
                    'sSearch'               : '',
                    'lengthMenu'            : '_MENU_ items/page',
                },
                'aaData'    : aaData,
                "bJQueryUI" : true,
                "aoColumns" : [
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "2%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "6%" }, 
                    { "sWidth"  : "4%" }, 
                    { "sWidth"  : "4%" }, 
                    { "sWidth"  : "3%" },  
                ],
                'aLengthMenu'   : [
                    [-1],
                    ["All"]
                ],
                dom: 'Bfrtip',
                buttons: [ {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    sheetName: 'Exported data'
                }, 'copy', 'print' ]
            });
        },
    });
}

function get_map_us(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/report/get_map_us/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/report/get_map_us/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var agent       = value.agentName.split(" ");
                var agentName   = "";

                if (agent.length > 1) {
                    agentName   = agent[0] + ' ' + agent[1];
                } else {
                    agentName   = agent[0];
                }
                
                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    value.agentName,
                    dateFormat(value.createdDate),
                    value.pnr, 
                    `618`+value.ticketNo,
                    value.kfNo,  
                    value.tripFrom_1+`-`+value.tripFrom_2+`-`+value.tripFrom_3+`(`+value.depClass+`) || `+value.tripTo_1+`-`+value.tripTo_2+`-`+value.tripTo_3+`(`+value.arrClass+`)`, 
                    value.map,
                    value.adminName+`, `+dateFormat(value.claimedDate)+` [`+value.claimedTime+`]`,
                ]);
            });
            $('#tb_map_us').DataTable().destroy();
            $('#tb_map_us').DataTable({
                // 'responsive': true,
                'language'  : {
                    'searchPlaceholder'     : 'Search...',
                    'sSearch'               : '',
                    'lengthMenu'            : '_MENU_ items/page',
                },
                'aaData'    : aaData,
                "bJQueryUI" : true,
                "aoColumns" : [
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "2%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "6%" }, 
                    { "sWidth"  : "4%" }, 
                    { "sWidth"  : "3%" },  
                ],
                'aLengthMenu'   : [
                    [-1],
                    ["All"]
                ],
                dom: 'Bfrtip',
                buttons: [ {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    sheetName: 'Exported data'
                }, 'copy', 'print' ]
            });
        },
    });
}

function get_flazz(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/report/get_flazz/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/report/get_flazz/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var agent       = value.agentName.split(" ");
                var agentName   = "";

                if (agent.length > 1) {
                    agentName   = agent[0] + ' ' + agent[1];
                } else {
                    agentName   = agent[0];
                }
                
                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    value.agentName,
                    dateFormat(value.createdDate),
                    value.pnr, 
                    `618`+value.ticketNo,
                    value.kfNo,  
                    value.tripFrom_1+`-`+value.tripFrom_2+`-`+value.tripFrom_3+`(`+value.depClass+`) || `+value.tripTo_1+`-`+value.tripTo_2+`-`+value.tripTo_3+`(`+value.arrClass+`)`, 
                    value.flazz,
                    value.adminName+`, `+dateFormat(value.claimedDate)+` [`+value.claimedTime+`]`,
                ]);
            });
            $('#tb_flazz').DataTable().destroy();
            $('#tb_flazz').DataTable({
                // 'responsive': true,
                'language'  : {
                    'searchPlaceholder'     : 'Search...',
                    'sSearch'               : '',
                    'lengthMenu'            : '_MENU_ items/page',
                },
                'aaData'    : aaData,
                "bJQueryUI" : true,
                "aoColumns" : [
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "2%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "6%" }, 
                    { "sWidth"  : "4%" }, 
                    { "sWidth"  : "3%" },  
                ],
                'aLengthMenu'   : [
                    [-1],
                    ["All"]
                ],
                dom: 'Bfrtip',
                buttons: [ {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    sheetName: 'Exported data'
                }, 'copy', 'print' ]
            });
        },
    });
}

function get_ezlink(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/report/get_ezlink/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/report/get_ezlink/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var agent       = value.agentName.split(" ");
                var agentName   = "";

                if (agent.length > 1) {
                    agentName   = agent[0] + ' ' + agent[1];
                } else {
                    agentName   = agent[0];
                }
                
                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    value.agentName,
                    dateFormat(value.createdDate),
                    value.pnr, 
                    `618`+value.ticketNo,
                    value.kfNo,  
                    value.tripFrom_1+`-`+value.tripFrom_2+`-`+value.tripFrom_3+`(`+value.depClass+`) || `+value.tripTo_1+`-`+value.tripTo_2+`-`+value.tripTo_3+`(`+value.arrClass+`)`, 
                    value.ezlink,
                    value.adminName+`, `+dateFormat(value.claimedDate)+` [`+value.claimedTime+`]`,
                ]);
            });
            $('#tb_ezlink').DataTable().destroy();
            $('#tb_ezlink').DataTable({
                // 'responsive': true,
                'language'  : {
                    'searchPlaceholder'     : 'Search...',
                    'sSearch'               : '',
                    'lengthMenu'            : '_MENU_ items/page',
                },
                'aaData'    : aaData,
                "bJQueryUI" : true,
                "aoColumns" : [
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "2%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "1%" }, 
                    { "sWidth"  : "6%" }, 
                    { "sWidth"  : "4%" }, 
                    { "sWidth"  : "3%" },  
                ],
                'aLengthMenu'   : [
                    [-1],
                    ["All"]
                ],
                dom: 'Bfrtip',
                buttons: [ {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    sheetName: 'Exported data'
                }, 'copy', 'print' ]
            });
        },
    });
}

function get_daily(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        // url: 'http://sqtfsurabaya.com/api/Admin/report/get_daily/_',
        url: 'http://localhost/travelfair/sqtf_0220/api/Admin/report/get_daily/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var agent       = value.agentName.split(" ");
                var agentName   = "";

                if (agent.length > 1) {
                    agentName   = agent[0] + ' ' + agent[1];
                } else {
                    agentName   = agent[0];
                }
                
                aaData.push([
                    value.agentName,
                    value.queueCode, 
                    value.pnr, 
                    value.tripFrom_3, 
                    value.depClass, 
                    value.tripTo_1, 
                    value.arrClass,
                    value.depDate, 
                    value.arrDate,  
                    `618`+value.ticketNo,
                    value.kfNo,  
                    value.flazz, 
                    value.barcodeFlazz,
                    value.foldable,
                    value.shopping,   
                    dateFormat(value.depDate),
                    value.trip,
                    value.itin, 
                    value.Flazz,
                    value.Foldable,
                    value.Shopping,
                    value.CDV,
                    value.CDV_Extra,
                    value.EZ_Link,
                    value.MAP_PER,
                    value.MAP_US, 
                    value.adminName,
                    dateFormat(value.claimedDate),
                ]);
            });
            $('#tb_daily_report').DataTable().destroy();
            $('#tb_daily_report').DataTable({
                // 'responsive': true,
                'language'  : {
                    'searchPlaceholder'     : 'Search...',
                    'sSearch'               : '',
                    'lengthMenu'            : '_MENU_ items/page',
                },
                'aaData'    : aaData,
                "bJQueryUI" : true,
                "aoColumns" : [
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                    { "sWidth"  : "1%" },  
                ],
                'aLengthMenu'   : [
                    [-1],
                    ["All"]
                ],
                dom: 'Bfrtip',
                buttons: [ {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    sheetName: 'Exported data'
                }, 'copy', 'print' ]
            });
        },
    });
}