var claimedID   = atob(localStorage.getItem(btoa("admin_claimedID")));

$(document).ready(function() {
	get_data();
    master_class();
    $('#pnr').focus();

    choose_triptype();
    $('input[type=radio][name=tripType]').on('change', function () {
        choose_triptype();
    });

    $('#tripFrom_3').on('change keyup paste', function (e) {
        this.value.replace(/\s+/g, '');
        var input = $.trim(this.value);
        if ($('input[name="tripType"]:checked').val() == 0) {
            if ($('#tripFrom_3').val() == 'sin') {
                console.log('sin')
                $('#tripFrom_3').val('NON');
                check_tripform(input);
                check_tripto(input);    
            } else {
                $('#tripTo_1').val(input);
                check_tripform(input);
                check_tripto(input);
            }
        }else{
            if ($('#tripFrom_3').val() == 'sin') {
                $('#tripFrom_3').val('NON');
                check_tripform(input);  
            } else {
                check_tripform(input);
            }
        }
    });

    $('#tripTo_1').on('change keyup paste', function (e) {
        this.value.replace(/\s+/g, '');
        var input = $.trim(this.value);
        if ($('input[name="tripType"]:checked').val() == 0) {
            if ($('#tripTo_1').val() == 'sin') {
                $('#tripTo_1, #tripFrom_3').val('NON');
                check_tripform(input);
                check_tripto(input);    
            } else {
                $('#tripFrom_3').val(input);
                check_tripform(input);
                check_tripto(input);
            }
        }else{
            if ($('#tripTo_1').val() == 'sin') {
                $('#tripTo_1').val('NON');
                check_tripto(input);    
            } else {
                check_tripto(input);
            }
        }
    });

    $('#btn_save_data').click( function() {
        $('#btn_save_data').attr('disabled','disabled');
        save_data();
    });

    $('#btn_save_details').click( function() {
        $('#btn_save_details').attr('disabled','disabled');
        save_details();
    });
});

function get_data(){
    $('#tb_detail > tbody').empty();
    $('#tb_update > tbody').empty();
    $('#tb_ticket > tbody').empty();
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/claimed/get_details/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/claimed/get_details/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            claimedID   : claimedID
        },
        success: function(response) {
            var isStatus    = "";
            var lastBy      = "";
            var lastDate    = "";
            var data        = `'`+response.data._+`','`+response.data.pnr+`','`+response.data.tripType+`','`+response.data.depDate+`','`+response.data.arrDate+`','`+response.data.classDep+`','`+response.data.classArr+`','`+response.data.tripFrom_3+`','`+response.data.tripTo_1+`','`+response.data.remarks+`'`;
            var pnr         = '';

            var wl = 0; var queue = 0; var done = 0;
            $.each(response.data.ticket, function( i, val ) {
                var no      = i + 1;
                var status  = "";
                var button  = "";
                var claimed = "";
                var total1; var total2; var total3;
                var total4; var total5; var total6;
                var total7; var total8; var total9;

                if (val.isStatus == 0) {
                    wl      += 1;
                } else if(val.isStatus == 1){
                    queue   += 1;
                } else {
                    done    += 1;
                }

                if (done == response.data.ticket.length) {
                    pnr         = 'PNR';
                } else {
                    pnr         = `PNR <span onclick="edit_data(`+data+`)" class="badge badge-info" style="cursor:pointer"><i class="fa fa-edit"></i> Edit</span>`;
                }

                if (val.isStatus == 0) {
                    status  = `<span class="badge badge-warning pd-x-10 pd-y-5" style="color:white;text-align: center;width: max-content;top: 50%;left: 50%;transform: translate(-50%,50%);position: relative;">WAITING LIST</span>`;
                    button  = `<div style="text-align: center;width: max-content;top: 50%;left: 50%;transform: translate(-50%,40%);position: relative;">
                                <div data-toggle="tooltip" title="Details" onclick="edit_details('`+val.id+`','`+val.ticketNo+`','`+val.kfNo+`','0')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
                                    <i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
                                </div>
                                <div data-toggle="tooltip" title="Delete" onclick="delete_details(`+val.id+`)" class="btn btn-xs btn-danger" style="padding: 1px 5px;">
                                    <i style="vertical-align: middle;font-size: 10px;" class="fa fa-trash"></i>
                                </div>
                            </div>`;
                            
                    total1  = '-';
                    total2  = '-';
                    total3  = '-';
                    total4  = '-';
                    total5  = '-';
                    total6  = '-';
                    total7  = '-';
                    total8  = '-';
                    claimed = '-';
                } else if(val.isStatus == 1){
                    status  = `<span class="badge badge-info pd-x-10 pd-y-5" style="color:white;text-align: center;width: max-content;top: 50%;left: 50%;transform: translate(-50%,50%);position: relative;">QUEUE</span>`;
                    button  = `<div style="text-align: center;width: max-content;top: 50%;left: 50%;transform: translate(-50%,40%);position: relative;">
                                <div data-toggle="tooltip" title="Details" onclick="edit_details('`+val.id+`','`+val.ticketNo+`','`+val.kfNo+`','1')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
                                    <i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
                                </div>
                                <div data-toggle="tooltip" title="Delete" onclick="delete_details(`+val.id+`)" class="btn btn-xs btn-danger" style="padding: 1px 5px;">
                                    <i style="vertical-align: middle;font-size: 10px;" class="fa fa-trash"></i>
                                </div>
                            </div>`;
                            
                    total1  = val.queue_gift_1;
                    total2  = val.queue_gift_2;
                    total3  = val.queue_gift_3;
                    total4  = val.queue_gift_4;
                    total5  = val.queue_gift_5;
                    total6  = val.queue_gift_6;
                    total7  = val.queue_gift_7;
                    total8  = val.queue_gift_8;
                    claimed = '-';
                } else {
                    status  = `<span class="badge badge-success pd-x-10 pd-y-5" style="color:white;text-align: center;width: max-content;top: 50%;left: 50%;transform: translate(-50%,50%);position: relative;">COMPLETE</span>`;
                    button  = `<div style="text-align: center;width: max-content;top: 50%;left: 50%;transform: translate(-50%,40%);position: relative;">
                                <div data-toggle="tooltip" title="Details" onclick="edit_details('`+val.id+`','`+val.ticketNo+`','`+val.kfNo+`','2')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
                                    <i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
                                </div>
                            </div>`;
                            
                    var gift4; var gift5; var gift7; var gift8;
                    if (val.total_4 == "2") {
                        gift4 = `<span data-toggle="tooltip" title="[`+val.barcode_4_1+`] [`+val.barcode_4_2+`]" style="cursor:default">`+val.total_4+`</span>`;
                    } else if (val.total_4 == "1") {
                        gift4 = `<span data-toggle="tooltip" title="[`+val.barcode_4_1+`]" style="cursor:default">`+val.total_4+`</span>`;
                    } else {
                        gift4 = val.total_4;
                    }

                    if (val.gift5 == "1") {
                        gift5 = `<span data-toggle="tooltip" title="[`+val.barcode_5+`]" style="cursor:default">`+val.total_5+`</span>`;
                    } else {
                        gift5 = val.total_5;
                    }

                    if (val.total_7 == "2") {
                        gift7 = `<span data-toggle="tooltip" title="[`+val.barcode_7_1+`] [`+val.barcode_7_2+`]" style="cursor:default">`+val.total_7+`</span>`;
                    } else if (val.total_7 == "1") {
                        gift7 = `<span data-toggle="tooltip" title="[`+val.barcode_7_1+`]" style="cursor:default">`+val.total_7+`</span>`;
                    } else {
                        gift7 = val.total_7;
                    }

                    if (val.gift8 == "1") {
                        gift8 = `<span data-toggle="tooltip" title="[`+val.barcode_8+`]" style="cursor:default">`+val.total_8+`</span>`;
                    } else {
                        gift8 = val.total_8;
                    }

                    total1  = val.total_1;
                    total2  = val.total_2;
                    total3  = val.total_3;
                    total4  = gift4;
                    total5  = gift5;
                    total6  = val.total_6;
                    total7  = gift7;
                    total8  = gift8;
                    claimed = val.claimBy+`<br>`+dateFormat(val.claimedDate)+`<br>`+val.claimedTime;
                }

                $('#tb_ticket > tbody').append(`
                    <tr>
                        <td style="text-align:center">`+no+`</td>
                        <td>618`+val.ticketNo+`<br>`+val.kfNo+`</td>
                        <td style="text-align:center;font-size:180%">`+total1+`</td>
                        <td style="text-align:center;font-size:180%">`+total2+`</td>
                        <td style="text-align:center;font-size:180%">`+total3+`</td>
                        <td style="text-align:center;font-size:180%">`+total4+`</td>
                        <td style="text-align:center;font-size:180%">`+total5+`</td>
                        <td style="text-align:center;font-size:180%">`+total6+`</td>
                        <td style="text-align:center;font-size:180%">`+total7+`</td>
                        <td style="text-align:center;font-size:180%">`+total8+`</td>
                        <td style="text-align:center;font-size:9px;font-weight:bold">`+claimed+`</td>
                        <td>`+status+`</td>
                        <td>`+button+`</td>
                    </tr>
                `);
            });
            
            console.log()

            if (response.data.isStatus == 0) {
                isStatus    = `<span class="badge badge-warning pd-x-10 pd-y-5" style="color:white">OPEN</span>`;
            } else if(response.data.isStatus == 1){
                isStatus    = `<span class="badge badge-info pd-x-10 pd-y-5" style="color:white">PARTLY</span>`;
            } else {
                isStatus    = `<span class="badge badge-success pd-x-10 pd-y-5" style="color:white">COMPLETE</span>`;
            }

            if (response.data.lastBy == null) {
                lastBy = `-`;
            } else {
                lastBy = response.data.lastBy;
            }

            if (response.data.lastDate == null) {
                lastDate = `-`;
            } else {
                lastDate = dateFormat(response.data.lastDate);
            }

            $('#tb_detail > tbody').append(`
                <tr>
                  <td>
                    <small><b>`+pnr+`</b></small><br>
                    `+response.data.pnr+`
                  </td>
                  <td>
                    <small><b>Trip Type</b></small><br>
                    `+response.data.trip+`
                  </td>
                </tr>
                <tr>
                  <td>
                    <small><b>Departure Date</b></small><br>
                    `+dateFormat(response.data.depDate)+`
                  </td>
                  <td>
                    <small><b>Arrival Date</b></small><br>
                    `+dateFormat(response.data.arrDate)+`
                  </td>
                </tr>
                <tr>
                  <td>
                    <small><b>Trip From</b></small><br>
                    `+response.data.dep+` ( `+response.data.depClass+` )
                  </td>
                  <td>
                    <small><b>Trip To</b></small><br>
                    `+response.data.arr+` ( `+response.data.arrClass+` )
                  </td>
                </tr>
            `);

            var remarks = "";
            if (response.data.remarks == "" || response.data.remarks == null) {
                remarks = "-";
            } else {
                remarks = response.data.remarks;
            }

            $('#tb_update > tbody').append(`
                <tr>
                    <td colspan="2">
                      <small><b>Remarks</b></small><br>
                      `+remarks+`
                    </td>
                </tr>
                <tr>
                    <td>
                      <small><b>Ticket</b></small><br>
                      `+response.data.noofticket+`
                    </td>
                    <td>
                      <small><b>Status</b></small><br>
                      `+isStatus+`
                    </td>
                </tr>
                <tr>
                    <td>
                      <small><b>Created By</b></small><br>
                      `+response.data.createdBy+`
                    </td>
                    <td>
                      <small><b>Created Date</b></small><br>
                      `+dateFormat(response.data.createdDate)+`
                    </td>
                </tr>
            `);
        },
    });
}

function edit_data(id,pnr,tripType,depDate,arrDate,classDep,classArr,tripFrom,tripTo,remarks){
    $('#btn_save_data').removeAttr("disabled");
    $('#pnrID').val(id);
    $('#pnr').val(pnr);
    $('#thisPnr').val(pnr)
    $('#isPnr').val('1');
    $('#pnrStatus').html(`<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`);
    $('#tripType').val(tripType);
    $("input[name=tripType][value='"+tripType+"']").prop("checked",true);
    var dep = depDate.split('-');
    $('#depdate_dd').val(dep[2]);
    $('#depdate_mm').val(dep[1]);
    $('#depdate_yy').val(dep);
    var arr = arrDate.split('-');
    $('#arrdate_dd').val(arr[2]);
    $('#arrdate_mm').val(arr[1]);
    $('#arrdate_yy').val(arr);
    $('#tripFrom_3').val(tripFrom);
    $('#tripTo_1').val(tripTo);
    $('#classDep').val(classDep);
    $('#classArr').val(classArr);
    $('#remarks').val(remarks);
    $('#isFrom').val('1');
    $('#isTo').val('1');
    $('#tripFromStatus').html(`<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`);
    $('#tripToStatus').html(`<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`);
    $('#modal_edit').modal('show');
}

function add_details(){
    $('#btn_save_details').removeAttr("disabled");
    $('#modal_add_ticket').modal('show');
    $('#modal_tittle').html('Add Ticket');
    $('#act').val('add');
    $('#id').val('');
    $('#thisTicket').val('');
    $('#ticketNo').val('');
    $('#kfNo').val('');
}

function edit_details(id,ticketNo, kfNo, status){
    $('#btn_save_details').removeAttr("disabled");
    $('#modal_add_ticket').modal('show');
    $('#modal_tittle').html('Edit Ticket');
    $('#act').val('edit');
    $('#id').val(id);
    $('#isTicket').val(1);
    $('#thisTicket').val(ticketNo);
    $('#ticketNo').val(ticketNo);
    $('#kfNo').val(kfNo);

    if (status == 2) {
        $('#ticketNo').attr('readonly',true);
    } else {
        $('#ticketNo').attr('readonly',false);
    }
}

function master_class(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/class_airline/get_data/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/admin/class_airline/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            $('#classDep').empty();
            $.each(response.data, function (i, item) {
                if (item.id != 1) {
                    $('#classDep').append($('<option>', { 
                        value: item.id,
                        text : item.className 
                    }));
                }
            });

            $('#classArr').empty();
            $.each(response.data, function (i, item) {
                if (item.id != 1) {
                    $('#classArr').append($('<option>', { 
                        value: item.id,
                        text : item.className 
                    }));
                }
            });
        },
    });
}

function testPnr(val){
    var value   = $.trim(val);
    var sukses  = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if (value.length < 6) {
        $('#pnrStatus').html(error);
        $('#isPnr').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: 'http://sqtfsurabaya.com/api/Agent/claim/check_pnr/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/agent/claim/check_pnr/_',
            dataType: 'json',
            cache: false,
            data: {
                'pnr' : value
            },
            success: function (responses) {
                if(responses.code == 200 && responses.total == 0){
                    $('#pnrStatus').html(sukses);
                    $('#isPnr').val(1);
                }else if(responses.total == 2){
                    if ($('#thisPnr').val() == val) {
                        $('#pnrStatus').html(sukses);
                        $('#isPnr').val(1);
                    } else {
                        $('#pnrStatus').html(error);
                        $('#isPnr').val(0);
                    }
                }
            }
        });
    }
}

function choose_triptype(){
    $('#tripFrom_3, #tripTo_1').val('');
    setTimeout(function(){
        var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
        
        $('#tripFromInfo').css('display', 'none');
        $('#tripToInfo').css('display', 'none');
        $('#tripFromStatus').html(error);
        $('#tripToStatus').html(error);
    }, 100);
}

function check_tripform(input){
    var sukses  = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if(input.length < 3){
        $('#tripFromStatus').html(error);
        $('#tripFromInfo').css('display','none');
        $('#isFrom').val(0);
    }else if(input.length == 3){
        $.ajax({
            type: 'POST',
            url: 'http://sqtfsurabaya.com/api/Agent/claim/check_route/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/agent/claim/check_route/_',
            dataType: 'json',
            cache: false,
            data: {
                'airportCode' : input
            },
            success: function (responses) {
                if(responses.code == 200 && responses.total > 0){
                    $('#tripFromStatus').html(sukses);
                    $('#tripFromInfo').css('display','block');
                    $('#tripFromInfo').html('<b>'+responses.data.airportCode+'</b> ( '+responses.data.airportName+' - '+responses.data.airportCountry+' )');
                    $('#isFrom').val(1);
                }else{
                    $('#tripFromStatus').html(error);
                    $('#tripFromInfo').css('display','none');
                    $('#isFrom').val(0);
                }
            }
        })
    }
}

function check_tripto(input){
    var sukses  = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if(input.length < 3){
        $('#tripToStatus').html(error);
        $('#tripToInfo').css('display','none');
        $('#isTo').val(0);
    }else if(input.length == 3){
        $.ajax({
            type: 'POST',
            url: 'http://sqtfsurabaya.com/api/Agent/claim/check_route/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/agent/claim/check_route/_',
            dataType: 'json',
            cache: false,
            data: {
                'airportCode' : input
            },
            success: function (responses) {
                if(responses.code == 200 && responses.total > 0){
                    $('#tripToStatus').html(sukses);
                    $('#tripToInfo').css('display','block');
                    $('#tripToInfo').html('<b>'+responses.data.airportCode+'</b> ( '+responses.data.airportName+' - '+responses.data.airportCountry+' )');
                    $('#isTo').val(1);
                }else{
                    $('#tripToStatus').html(error);
                    $('#tripToInfo').css('display','none');
                    $('#isTo').val(0);
                }
            }
        })
    }
}

function save_data(){
    if ($('#isPnr').val() == 0){
        alert('PNR is already in our system and/or your PNR is not valid.');
    } else if ($('#isFrom').val() == 0){
        alert('We can not find this city () in our system.');
    } else if ($('#isTo').val() == 0){
        alert('We can not find this city () in our system.');
    } else {
        $.ajax({
            type: 'POST',
            url: 'http://sqtfsurabaya.com/api/Admin/claimed/save_data/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/claimed/save_data/_',
            dataType: 'json',
            cache: false,
            data: $('#form_claim').serialize(),
            success: function (responses) {
                if(responses.code==200){
                    alert('success');
                    $('#modal_edit').modal('hide');
                    get_data();
                }else{
                    alert('failed, try again later');
                    $('#modal_edit').modal('hide');
                    get_data();
                }
            }
        });
    }
}

function testTicket(val){
    var value   = $.trim(val);
    var sukses  = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if (value.length < 10) {
        $('#ticketStatus').html(error);
        $('#isTicket').val(0);
    }else {
        $.ajax({
            type: 'POST',
            url: 'http://sqtfsurabaya.com/api/Agent/claim/check_ticket/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/agent/claim/check_ticket/_',
            dataType: 'json',
            cache: false,
            data: {
                'ticketNo'      : value,
            },
            success: function (responses) {
                if(responses.code == 200 && responses.total == 0){
                    $('#ticketStatus').html(sukses);
                    $('#isTicket').val(1);
                }else{
                    if ($('#thisTicket').val() == val) {
                        $('#ticketStatus').html(sukses);
                        $('#isTicket').val(1);
                    } else {
                        $('#ticketStatus').html(error);
                        $('#isTicket').val(0);
                    }
                }
            }
        });
    }
}

function testKf(val){
    var value   = $.trim(val);
    var sukses  = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-check rataTengah" style="color:green;"></i></span>`;
    var error   = `<span class="input-group-text" id="basic-addon2" style="border:none;width:40px"><i class="fa fa-times rataTengah" style="color:red;"></i></span>`;
   
    if (value.length < 10) {
        $('#kfStatus').html(error);
        $('#isKf').val(0);
    }else {
        $('#kfStatus').html(sukses);
        $('#isKf').val(1);
    }
}

function save_details(){
    if ($('#ticketNo').val() == '' || $('#kfNo').val() == '') {
        alert('Data must be required!');
    } else if ($('#isTicket').val() == 0){
        alert('Ticket number (618-) is already in our system and/or your ticket number is not valid.');
    } else if ($('#isKf').val() == 0){
        alert('Krisflyer is not valid.');
    } else {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'http://sqtfsurabaya.com/api/Admin/claimed/save_details/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/claimed/save_details/_',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $('#form_ticket').serialize()+"&claimedID="+claimedID,
            success: function(response) {
                if (response.code == 200) {
                    alert('success');
                    $('#modal_add_ticket').modal('hide');
                    get_data();
                } else {
                    alert('failed, try again later');
                    $('#modal_add_ticket').modal('hide');
                    get_data();
                }
            },
        });
    }
}

function delete_details(id){
    $('#modal_delete').modal('show');
    $('#delete_data').click(function() {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'http://sqtfsurabaya.com/api/Admin/claimed/delete_details/_',
            // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/claimed/delete_details/_',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                token       : '1b787b70ed2e0de697d731f14b5da57b',
                ticketID    : id
            },
            success: function(response) {
                if (response.code == 200) {
                    alert('success');
                    $('#modal_delete').modal('hide');
                    get_data();
                } else {
                    alert('failed, try again later');
                    $('#modal_delete').modal('hide');
                    get_data();
                }
            },
        });
    });
}