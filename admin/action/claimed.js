localStorage.removeItem(btoa('admin_claimedID'));

$(document).ready(function() {
	get_data();
});

function get_data(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://sqtfsurabaya.com/api/Admin/claimed/get_data/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/claimed/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var status 		= "";
                var ticket      = "";

                ticket = `<table style="width:100%">
                            <tr>
                                <td rowspan="`+value.noofticket+`" style="Width:1%">`+value.noofticket+`</td>`;
                $.each(value.ticket, function( j, val ) {
                    var isStatus = "";
                    if (val.isStatus == '0') {
                        isStatus = `<span class="badge badge-warning pd-x-10 pd-y-5" style="color:white"><i class="fa fa-clock"></i></span>`
                    } else if(val.isStatus == '1'){
                        isStatus = `<span class="badge badge-info pd-x-10 pd-y-5" style="color:white"><i class="fa fa-circle"></i></span>`
                    } else {
                        isStatus = `<span class="badge badge-success pd-x-10 pd-y-5" style="color:white"><i class="fa fa-check"></i></span>`
                    }

                    if (j == 0) {
                        ticket += `<td>618`+val.ticketNo+`</td><td>`+isStatus+`</td></tr>`;
                    } else {
                        ticket += `<tr><td>618`+val.ticketNo+`</td><td>`+isStatus+`</td></tr>`;
                    }
                });
                ticket += `</table>`;

                if (value.isStatus == 0) {
                	status = `<span class="badge badge-warning pd-x-10 pd-y-5" style="color:white">OPEN</span>`
                } else if(value.isStatus == 1){
                	status = `<span class="badge badge-info pd-x-10 pd-y-5" style="color:white">PARTLY</span>`
                } else {
                	status = `<span class="badge badge-success pd-x-10 pd-y-5" style="color:white">COMPLETE</span>`
                }

                var agent       = value.agentName.split(" ");
                var agentName   = "";

                if (agent.length > 1) {
                    agentName   = agent[0] + ' ' + agent[1];
                } else {
                    agentName   = agent[0];
                }
                
                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    value.pnr, 
                    dateFormat(value.issuanceDate),
                    value.trip,  
                    value.tripFrom_1+` `+value.tripFrom_2+` `+value.tripFrom_3+` (`+value.depClass+`)`,
                    value.tripTo_1+` `+value.tripTo_2+` `+value.tripTo_3+` (`+value.arrClass+`)`, 
                    dateFormat(value.depDate),
                    `<div style="text-align:center">` + ticket + `</div>`, 
                    agentName, 
                    `<div style="text-align:center">` + status + `</div>`, 
                    `<div style="text-align:center">
                    	<div data-toggle="tooltip" title="Details" onclick="claimed_detail('`+value.id+`')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
                		</div>
            		</div>`,
                ]);
            });
            $('#tb_claimed').DataTable().destroy();
        	$('#tb_claimed').DataTable({
				// 'responsive': true,
				'language'	: {
					'searchPlaceholder'		: 'Search...',
					'sSearch'				: '',
					'lengthMenu'			: '_MENU_ items/page',
			  	},
				'aaData'	: aaData,
				"bJQueryUI"	: true,
				"aoColumns"	: [
					{ "sWidth"	: "1%" },  
					{ "sWidth"	: "1%" }, 
					{ "sWidth"	: "1%" }, 
                    { "sWidth"  : "2%" }, 
					{ "sWidth"	: "6%" }, 
					{ "sWidth"	: "6%" }, 
					{ "sWidth"	: "1%" }, 
					{ "sWidth"	: "1%" }, 
					{ "sWidth"	: "3%" },
					{ "sWidth"	: "1%" },
					{ "sWidth"	: "1%" },  
				],
				'aLengthMenu'	: [
					[10, 20, 50, -1],
					[10, 20, 50, "All"]
				],
			});
        },
    });
}

function claimed_detail(id){
    localStorage.setItem(btoa('admin_claimedID'), btoa(id));
    window.location.href = "claimed_detail";
}