$(document).ready(function() {
	get_data();
	get_region();
});

function get_region(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
    	url: 'http://sqtfsurabaya.com/api/Admin/region/get_data/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/region/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            $.each(response.data, function( i, value ) {
            	$('#selectRegion').append(`
	            	<option value="`+value.id+`">`+value.region+`</option>
    			`);
            });
        },
    });
}

function get_data(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
    	url: 'http://sqtfsurabaya.com/api/Admin/airport/get_data/_',
        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/airport/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;

                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    `<span style="text-align:center">` + value.region + `</span>`, 
                    value.airportCode, 
                    value.airportName, 
                    value.airportCountry,
                    `<div style="text-align:center">
                    	<div data-toggle="tooltip" title="Edit" onclick="edit_data('`+value.id+`','`+value.regionID+`','`+value.airportCode+`','`+value.airportName+`','`+value.airportCountry+`')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
                		</div>
                    	<div data-toggle="tooltip" title="Delete" onclick="delete_data(`+value.id+`)" class="btn btn-xs btn-danger" style="padding: 1px 5px;">
                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-trash"></i>
                		</div>
            		</div>`,
                ]);
            });
            $('#tb_admin').DataTable().destroy();
        	$('#tb_admin').DataTable({
				'responsive': true,
				'language'	: {
					'searchPlaceholder'		: 'Search...',
					'sSearch'				: '',
					'lengthMenu'			: '_MENU_ items/page',
			  	},
				'aaData'	: aaData,
				"bJQueryUI"	: true,
				"aoColumns"	: [
					{ "sWidth"	: "5%" },  
					{ "sWidth"	: "25%" }, 
					{ "sWidth"	: "13%" }, 
					{ "sWidth"	: "25%" }, 
					{ "sWidth"	: "25%" }, 
					{ "sWidth"	: "7%" },
				],
				'aLengthMenu'	: [
					[10, 20, 50, -1],
					[10, 20, 50, "All"]
				],
			});
        },
    });
}

function add_data(){
	$('#modal_add').modal('show');
	$('#modal_tittle').html('Add New Data');
	$('#act').val('add');
	$('#id').val('');
	$('#airportName').val('');
	$('#airportCode').val('');
	$('#airportCountry').val('');
}

function edit_data(id,regionID,code,name,country){
	$('#modal_add').modal('show');
	$('#modal_tittle').html('Edit Data');
	$('#act').val('edit');
	$('#id').val(id);
	$('#selectRegion').val(regionID);
	$('#airportCode').val(code);
	$('#airportName').val(name);
	$('#airportCountry').val(country);
}

function delete_data(id){
	$('#modal_delete').modal('show');
    $('#delete_data').click(function() {
	    $.ajax({
	        type: 'POST',
	        dataType: 'JSON',
    		url: 'http://sqtfsurabaya.com/api/Admin/airport/delete_data/_',
	        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/airport/delete_data/_',
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded'
	        },
	        data: {
	            token	: '1b787b70ed2e0de697d731f14b5da57b',
	            id		: id
	        },
	        success: function(response) {
        		if (response.code == 200) {
        			alert('success');
					$('#modal_delete').modal('hide');
		        	get_data();
		        } else {
        			alert('failed, try again later');
					$('#modal_delete').modal('hide');
		        	get_data();
		        }
	        },
	    });
    });
}

function save(){
	if ($('#airportName').val() == '' || $('#airportCode').val() == '' || $('#airportCountry').val() == '') {
		alert('Data must be required!');
	} else {
	    $.ajax({
	        type: 'POST',
	        dataType: 'JSON',
    		url: 'http://sqtfsurabaya.com/api/Admin/airport/save_data/_',
	        // url: 'http://localhost/travelfair/sqtf_0220/api/Admin/airport/save_data/_',
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded'
	        },
	        data: $('#form_airport').serialize() ,
	        success: function(response) {
	        	if (response.code == 200) {
	        		alert('success');
					$('#modal_add').modal('hide');
		        	get_data();
	        	} else {
	        		alert('failed, try again later');
					$('#modal_add').modal('hide');
		        	get_data();
	        	}
	        },
	    });
	}
}